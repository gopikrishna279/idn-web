'use strict';

angular.module('mod.app', []);


angular.module('mod.app')

    .constant('MOD_APP', {
        API_URL: '',
        API_URL_DEV: ''
    })

    .config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state('app', {
                url: '/app',
                parent:'layout',
                template: '<div ui-view></div>',
                data: {
                    type: "home"
                }
            })
            //FOR NAVIGATION
           .state('app.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'mod_app/views/menu/app.header.html',
                   position: 1,
                   moduleName: "App",
                   type: 'header',
                   displayLink : true,
                   disableLink : false,
                   authenticate : true,
                   displayName : "Dashboard",
                   displayIcon: "",
                   authorities:""
               }
           })
           //FOR ROUTES
           .state('app.dashboard', {
               url: '/dash',
               template: "<h1>Hello From dashboard</h1>",
               controller : "AppDashboardCtrl",
               data: {
                   moduleName: "App",
                   groupName : 'App',
                   type: 'nav',
                   displayLink : true,
                   disableLink : false,
                   authenticate : true,
                   displayName : "Dashboard",
                   displayIcon: "",
                   authorities:""
               }
           })

           .state('app.newpage', {
               url : '/newpage',
               templateUrl : 'mod_app/views/newpage/app.newpage.html',
               data: {
                   moduleName: "App",
                   groupName : 'App',
                   type: 'nav',
                   displayLink : true,
                   disableLink : false,
                   authenticate : false,
                   displayName : "My New Page",
                   displayIcon: "",
                   authorities:""
               }
           })

    }])