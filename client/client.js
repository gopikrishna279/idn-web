angular
    .module('clientApp', [
        'ngAnimate',
        'ngResource',
        'ui.router',
        'ngMaterial',
        'LocalStorageModule',
        'angular-loading-bar',
        'ngMessages',
        'md.data.table',
        'mdColorPicker',
        'froala',
        'underscore',
        'ngSanitize',

        // Coding Labs
        'app.config',
        'app.nav',
        'app.layout',
        'app.theme',
        'mod.idn',
        'mod.nbos',
        'mod.console',
        'mod.dev',
        'mod.admin'
    ])

    .constant('CLIENT_CONFIG',{
            // CLIENT_ID: 'f94ee2cb-6e8a-4e81-b506-37a02922ac9e',
            CLIENT_ID: 'appConsole-app-client',
            CLIENT_SECRET: 'appConsole-app-secret',
            CLIENT_DOMAIN : 'http://localhost:9001'
    })

    .config(['localStorageServiceProvider', function (localStorageServiceProvider) {

        //localstorage configuration
        localStorageServiceProvider
            .setPrefix('CODINGLABS');

    }]);