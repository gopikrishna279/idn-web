angular.module('mod.admin')
    .controller('AdminModuleCtrl', ['$scope', '$stateParams', 'ModuleService_Admin', '$state', function($scope, $stateParams, ModuleService_Admin, $state) {

        var moduleId = $stateParams.id;
        $scope.showApi = false;

        var getModuleInfo = function() {
            ModuleService_Admin.getModule(moduleId).then(function(response) {
                $scope.module = response;
            }, function(response) {
                console.log(response);
            });
        };

        $scope.generate = function(){
            $scope.showApi = true;
        }

        $scope.approve = function(moduleId) {
            ModuleService_Admin.activateModule(moduleId, $scope.module).then(function(response) {
                if (response) {
                    $state.go('admin.modules');
                }
            }, function(response) {
                console.log(response);
            })
        }

        var init = function() {
            if (moduleId) {
                getModuleInfo();
            } else {
                $state.go('admin.modules');
            }
        };
        init();
    }]);
