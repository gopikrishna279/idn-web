'use strict';

angular.module('mod.nbos')

.factory('ModuleService_Admin', ['ModuleFactory_Admin', '$q', function(ModuleFactory_Admin, $q) {
    var factory = {};

    factory.allModules;
    factory.pendingModules;
    factory.module;

    factory.getAllModules = function(tenantId) {
        var deferred = $q.defer();
        ModuleFactory_Admin.allModules().get({ tenantId: tenantId }, function(success) {
            factory.allModules = success;
            console.log(success);
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;

    };

    factory.getAllPendingModules = function() {
        var deferred = $q.defer();
        ModuleFactory_Admin.allModules().get({ status: 'pending' }, function(success) {
            factory.pendingModules = success;
            console.log(success);
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }

    factory.getModule = function(moduleId) {
        var deferred = $q.defer();
        ModuleFactory_Admin.getModule().get({ moduleId: moduleId }, function(success) {
            factory.module = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    /*
        TODO : THIS SHOULD BE AN ADMIN TASK ONLY
     */
    factory.activateModule = function(moduleId, moduleObj) {
        var deferred = $q.defer();
        ModuleFactory_Admin.activateModule().activate({ moduleUuid: moduleId }, moduleObj, function(success) {

            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    //Get Configuration for a Module
    factory.getModuleConfig = function(moduleId) {
        var deferred = $q.defer();
        ModuleFactory_Admin.moduleConfig().get({ 'moduleId': moduleId }, function(success) {
            deferred.resolve(success);
            factory.config = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    factory.updateModuleConfig = function(config) {
        var deferred = $q.defer();
        ModuleFactory_Admin.moduleConfig().update(config, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    //get all the supported attribute types for a configuration
    factory.getAttributeTypes = function() {
        var deferred = $q.defer();
        ModuleFactory_Admin.configAttr().get(function(success) {
            deferred.resolve(success);
            factory.configAttrs = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }


    return factory;

}])

.factory('ModuleFactory_Admin', ['MOD_NBOS', 'SessionService', '$resource', function(MOD_NBOS, SessionService, $resource) {
    var factory = {};


    factory.allModules = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/list/:status', {
            status: '@status'
        }, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                },
                isArray: true
            }
        })
    };


    factory.getModule = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleId/show', {}, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    factory.activateModule = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleUuid/activate', { moduleUuid: '@moduleUuid' }, {
            'activate': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    factory.moduleConfig = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.API_URL + 'core/v0/config/module/:moduleId', { moduleId: '@moduleId' }, {
            'update': {
                method: 'PUT',
                headers: {
                    Authorization: bearer
                }
            },
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    factory.configAttr = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.API_URL + 'core/v0/config/attribute/types', {}, {
            'get': {
                method: 'GET',
                isArray: false,
                headers: {
                    Authorization: bearer
                }
            }
        })
    }

    return factory;
}]);
