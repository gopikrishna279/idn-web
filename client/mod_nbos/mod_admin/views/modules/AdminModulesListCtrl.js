angular.module('mod.admin')
    .controller('AdminModulesListCtrl', ['$scope', '$state', 'ModuleService_Admin', function($scope, $state, ModuleService_Admin) {

    	$scope.modulesList = [];

    	//Gel all modules 
    	var getModulesList = function(){
    		ModuleService_Admin.getAllPendingModules().then(function(response){
    			$scope.modulesList = response;
    		}, function(response){
    			console.log(response);
    			console.log("error getting Modules list");
    		})
    	};

        $scope.showModule = function(moduleId){
            $state.go('admin.module', {
                'id': moduleId
            })
        }

    	var init = function(){
    		getModulesList();
    	};
    	init();
    }]);
