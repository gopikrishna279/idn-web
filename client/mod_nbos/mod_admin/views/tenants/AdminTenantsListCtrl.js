angular.module('mod.admin')
    .controller('AdminTenantsListCtrl', ['$scope', '$state', 'AdminTenantService', function($scope, $state, AdminTenantService) {

        $scope.search = {};

        var getList = function() {
            AdminTenantService.getAllTenants().then(function(response) {
                $scope.tenants = response;
            }, function(error) {
                console.log("error retreiving tenants list");
            });
        };

        $scope.showTenant = function(tenant) {
            if (tenant && tenant.tenantId) {
                $state.go('admin.tenant', {
                    'id': tenant.tenantId
                });
            } else {
                console.log("no tenant id");
            }
        };

        var init = function() {

            getList();

        };

        init();
    }]);
