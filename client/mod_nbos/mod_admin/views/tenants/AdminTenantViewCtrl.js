angular.module('mod.admin')
    .controller('AdminTenantViewCtrl', ['$scope', '$state', 'AdminTenantService', '_', '$stateParams', '$rootScope', 'SessionService', 'AlertService', function($scope, $state, AdminTenantService, _, $stateParams, $rootScope, SessionService, AlertService) {

        var tenantId = $stateParams.id;
        $scope.showApi = false;
        $scope.user = {};


        var tenantView = function() {
            AdminTenantService.getAllTenants().then(function(response) {
                _.each(AdminTenantService.allTenants, function(item) {
                    if (item.tenantId == tenantId) {
                        $scope.tenant = item;
                    }
                });
                $scope.swaggerUrl = "https://idn.au-syd.mybluemix.net/api/ids/v0/definitions/tenants/" + tenantId + "/api-json";
            }, function(error) {
                console.log("error retreiving tenants list");
            });
        };

        $scope.generate = function() {
            AdminTenantService.getTokens(tenantId, $scope.user.token).then(function(response) {
                $rootScope.admin_user_token = $scope.user.token;
                $scope.showApi = true;
            }, function(error) {
                console.log(error);
                if(error.status == '401'){
                    AlertService.alert("invalid access token", 'md-warn');
                    $scope.showApi = false;
                }
            });

        };

        $scope.useToken = function() {
            $scope.user.token = SessionService.getStoredUserToken();
        };

        var init = function() {
            if (AdminTenantService.allTenants && tenantId) {
                _.each(AdminTenantService.allTenants, function(item) {
                    if (item.tenantId == tenantId) {
                        $scope.tenant = item;
                    }
                });
                $scope.swaggerUrl = "https://idn.au-syd.mybluemix.net/api/ids/v0/definitions/tenants/" + tenantId + "/api-json";
            } else if (tenantId) {
                tenantView();
            } else {
                $state.go('admin.tenants');
            }
        };

        init();
    }]);
