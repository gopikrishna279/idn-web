'use strict';

angular.module('mod.nbos')

.factory('AdminTenantService', ['AdminTenantFactory', '$q', function(AdminTenantFactory, $q) {
    var factory = {};

    factory.allTenants;



    factory.getAllTenants = function() {
        var deferred = $q.defer();
        AdminTenantFactory.allTenants().get(function(success) {
            factory.allTenants = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;

    };

    //Random API call to validate user token;

    factory.getTokens = function(tenantId, userToken) {
        var deferred = $q.defer();
        AdminTenantFactory.tokens(userToken).get({ "tenantId": tenantId }, function(success) {
            factory.allTenants = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    /*factory.viewTenant = function(tenantId) {
        var deferred = $q.defer();
        AdminTenantFactory.tenant().get({ 'tenantId': tenantId }, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;

    }*/

    return factory;

}])

.factory('AdminTenantFactory', ['MOD_NBOS', 'SessionService', '$resource', function(MOD_NBOS, SessionService, $resource) {
    var factory = {};


    factory.allTenants = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'ids/v0/tenants', {}, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                },
                isArray: true
            }
        })
    };

    factory.tenant = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'ids/v0/definitions/tenants/:tenantId', {
            tenantId: '@tenantId'
        }, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    factory.tokens = function(bearerToken) {

        var bearer = "Bearer " + bearerToken;

        return $resource(MOD_NBOS.API_URL + 'oauth/v0/tenants/:tenantId/tokens', {
            tenantId: '@tenantId'
        }, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                },
                isArray: true
            }
        })
    };

    return factory;
}]);
