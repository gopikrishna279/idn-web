angular.module('mod.admin')
    .controller('AdminAppConfigurationCtrl', ['$scope', 'ModuleService_Admin', 'AlertService', '$q', function($scope, ModuleService_Admin, AlertService, $q) {

        var moduleId = "MOD:app";
        $scope.configArr = [];

        $scope.addConfig = function() {
            $scope.configArr.push({});
        };

        $scope.deleteConfig = function(config) {
            var index = $scope.configArr.indexOf(config);
            $scope.configArr.splice(index, 1);
        }

        $scope.updateMulti = function(config) {
            if (config.multiValued) {
                config.type = "String";
                config.possibleValues = [{}];
            } else {
                delete config['possibleValues'];
            }
        };

        $scope.addPossible = function(arr) {
            arr.possibleValues.push({});
        };

        $scope.postConfig = function() {
            var configObj = {
                "moduleUuid": moduleId,
                "configuration": $scope.configArr
            };

            ModuleService_Admin.updateModuleConfig(configObj).then(function(response) {
                AlertService.alert("Config added successfully", "md-primary");
            }, function(response) {

            })

        };

        var init = function() {
            var config = ModuleService_Admin.getModuleConfig(moduleId),
                types = ModuleService_Admin.getAttributeTypes();

            $q.all([config, types])
                .then(function(success) {
                    $scope.configTypes = ModuleService_Admin.configAttrs.configTypes;
                    if (ModuleService_Admin.config) {
                        $scope.configArr = ModuleService_Admin.config;
                    } else {
                        $scope.configArr = [{

                        }];
                    }
                }, function(error) {
                    console.error(error);
                });
        };
        init();
    }]);
