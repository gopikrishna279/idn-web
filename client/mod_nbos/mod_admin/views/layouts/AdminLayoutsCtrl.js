angular.module('mod.admin')
    .controller('AdminLayoutsCtrl', ['$scope', 'LayoutService_Admin', '$mdDialog', 'AlertService', '$mdMedia', "_", function($scope, LayoutService_Admin, $mdDialog, AlertService, $mdMedia, _) {

        $scope.layouts = [];
        $scope.imagePath = "mod_nbos/assets/images/layout.png";

        var getAllLayouts = function() {
            LayoutService_Admin.getLayouts().then(function(response) {
                $scope.layouts = response;
            }, function(response) {
                console.log("error getting layouts");
            })
        };

        $scope.deleteLayout = function(item, ev){
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete this layout?')
                .textContent(item.name +" @ " +item.location)
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Yes')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                $scope.status = 'You decided to get rid of your debt.';
                // LayoutService_Admin.deleteLayout(item).then(function(response) {
                //     if (response) {
                //         AlertService.alert("Layout Deleted Successfully!", "md-primary");
                //     };
                // }, function(error) {
                //     console.log("error saving layout");
                // });
            }, function() {
                //do nothing
            });




        };

        $scope.showPreview = function(ev, index){

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                controller: function($scope, $mdDialog, LayoutService_Admin, layout) {
                    $scope.imagePath = "mod_nbos/assets/images/layout.png";

                    // $scope.layoutObj = layout;
                    $scope.layoutObj = _.clone(layout);
                    console.log($scope.layoutObj);
                    $scope.imageUpdated = false;

                    $scope.create = function() {
                        LayoutService_Admin.saveLayout($scope.layoutObj).then(function(response) {
                            if (response) {
                                $mdDialog.hide(response);
                            };
                        }, function(error) {
                            console.log("error saving layout");
                        });
                    };

                    $scope.closeDialog = function() {
                        if($scope.imageUpdated){
                            // $mdDialog.hide(layout);
                        } else {
                            $mdDialog.cancel();
                        }
                    };

                    $scope.saveImage = function(layout){

                        var file = $scope.logoImage;;

                        if(file){
                            LayoutService_Admin.saveLayoutImage(layout.id, file).then(function(success){
                                AlertService.alert("Hurray! Image saved successfully", "md-primary");
                                $scope.layoutObj.previewImage = success;
                                $scope.imageUpdated = true;
                            }, function(error){
                                AlertService.alert("Oops! Something went wrong. Please try again with another image.", "md-warn");
                            });
                        }

                    };
                },
                templateUrl: 'mod_nbos/mod_admin/views/layouts/LayoutImage.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen,
                locals : {
                    layout : $scope.layouts[index]
                }
            })
                .then(function(answer) {
                    $scope.layouts[index] = answer;
                }, function() {
                    // do nothing
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        $scope.createLayout = function(ev, layout) {
            $mdDialog.show({
                controller: function($scope, $mdDialog, LayoutService_Admin, layout) {
                    console.log(layout);
                    if (layout) {
                        $scope.layoutObj = layout;
                    } else{
                        $scope.layoutObj = {};
                    }
                    $scope.create = function() {
                        LayoutService_Admin.saveLayout($scope.layoutObj).then(function(response) {
                            if (response) {
                                $mdDialog.hide(response);
                            };
                        }, function(error) {
                            console.log("error saving layout");
                        });
                    };

                    $scope.closeDialog = function() {
                        $mdDialog.cancel();
                    }
                },
                locals: {
                    'layout': layout
                },
                templateUrl: 'mod_nbos/mod_admin/views/layouts/LayoutCreate.html',
                parent: angular.element(document.body),
                targetEvent: ev
            })
                .then(function(layout) {
                    if (layout) {
                        $scope.layouts.push(layout);
                        AlertService.alert("Layout Added successfully", "md-primary");
                    }
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        var init = function() {
            getAllLayouts();
        };
        init();
    }]);
