angular.module('mod.admin')
    .factory('LayoutService_Admin', ['$q', 'LayoutFactory_Admin', function($q, LayoutFactory_Admin) {

        var layoutServ = {};
        layoutServ.layouts;

        layoutServ.getLayouts = function() {
            var deferred = $q.defer();

            LayoutFactory_Admin.layouts().get(function(success) {
                deferred.resolve(success);
                layoutServ.layouts = success;
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        layoutServ.saveLayout = function(layout) {
            var deferred = $q.defer();
            LayoutFactory_Admin.layouts().save(layout, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            })

            return deferred.promise;
        };

        layoutServ.deleteLayout = function(layout) {
            var deferred = $q.defer();
            LayoutFactory_Admin.deleteLayout(layout).then(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };


        layoutServ.saveLayoutImage = function(layoutId, file) {
            var deferred = $q.defer();

            LayoutFactory_Admin.saveImage(layoutId, file).then(function(success) {

                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return layoutServ;
    }])
    .factory('LayoutFactory_Admin', ['$resource', '$http', 'MOD_NBOS', 'SessionService', '$q', 'Upload', function($resource, $http, MOD_NBOS, SessionService, $q, Upload) {

        var layoutFact = {};

        // POST "/api/core/$namespace/layouts/$layoutId/previewImage
        layoutFact.saveImage = function(layoutId, file) {
            var deferred = $q.defer();

            var bearer = "Bearer " + SessionService.getStoredUserToken();
            Upload.upload({
                url: MOD_NBOS.API_URL + 'core/v0/layouts/' + layoutId + '/previewImage',
                headers: {
                    'Authorization': bearer
                },
                file: file,
            }).success(function(data, status, headers, config) {
                deferred.resolve(data);

            }).error(function(error) {
                deferred.reject(error);

            });

            return deferred.promise;
        };


        layoutFact.deleteLayout = function(obj){
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            var deleteUrl = MOD_NBOS.API_URL + 'core/v0/layouts/';

            //NOTE: this has to be a $http request since 'DELETE' in $resource does not accept a payload
            // POST "/api/core/$namespace/layouts/$layoutId/previewImage
            return $http({
                method : 'DELETE',
                url : deleteUrl,
                headers :{
                    Authorization: bearer
                },
                data : obj
            });
        };

        layoutFact.layouts = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'core/v0/layouts', {

            }, {
                'get': {
                    method: 'GET',
                    isArray: true,
                    headers: {
                        "Authorization": bearer
                    }

                },
                'save': {
                    method: 'POST',
                    headers: {
                        'Authorization': bearer
                    }
                },
                'delete': {
                    method: 'DELETE',
                    headers: {
                        'Authorization': bearer
                    }
                }
            })
        };
        return layoutFact;
    }])
