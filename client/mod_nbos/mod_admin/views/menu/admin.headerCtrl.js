'use strict';

angular.module('mod.idn')
    .controller('AdminHeaderCtrl', ['$scope', 'UserService', '$location', function($scope, UserService, $location) {
        $scope.isAdmin = false;
        var hostName = $location.host().split('admin.')[1];
        var hostPort = $location.port();

        $scope.devUrl = 'http://dev.' + hostName + ":" + hostPort;
        $scope.consoleUrl = 'http://console.' + hostName + ":" + hostPort;

        var init = function() {
            if (UserService.roles && UserService.roles.isAdmin) {
                $scope.isAdmin = true;
            };
        };

        init();
    }]);
