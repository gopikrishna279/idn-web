/**
 *
 */

'use strict';

angular.module('mod.admin', ['ngSanitize', 'swaggerUi'])
    .constant('APP_ADMIN', {

    })
    .service('swaggerModule', ['$rootScope', '$q', function($rootScope, $q) {

        this.execute = function(req) {
            var bearer = "Bearer " + $rootScope.admin_user_token;
            req.headers.Authorization = bearer;
            var deferred = $q.defer();
            deferred.resolve(true);
            return deferred.promise;
        }

    }])
    .run(function(swaggerModules, swaggerModule) {
        swaggerModules.add(swaggerModules.BEFORE_EXPLORER_LOAD, swaggerModule);
    })

.config(['$stateProvider', function($stateProvider) {
    $stateProvider
        .state('admin', {
            url: '/admin',
            parent: 'layout',
            template: "<div ui-view></div>",
            data: {
                type: 'home',
                menu: false,
                name: "Primary",
                module: "Admin"
            }
        })
        //FOR NAVIGATION
        .state('admin.header', {
            url: '',
            template: "<div></div>",
            data: {
                templateUrl: 'mod_nbos/mod_admin/views/menu/admin.header.html',
                position: 1,
                moduleName: "Admin",
                type: 'header',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Profile",
                displayIcon: ""
            }
        })
        .state('admin.dashboard', {
            url: '/dashboard',
            templateUrl: "mod_nbos/mod_admin/views/dashboard/admin.dashboard.html",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Dashboard",
                displayIcon: ""
            }
        })
        .state('admin.analytics', {
            url: '/analytics',
            template: "mod_nbos/mod_admin/views/analytics/analytics.html",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Analytics",
                displayIcon: ""
            }
        })
        .state('admin.user', {
            url: '/userManagement',
            template: "mod_nbos/mod_admin/views/users/users.html",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "User Management",
                displayIcon: ""
            }
        })
        .state('admin.modules', {
            url: '/modules',
            templateUrl: "mod_nbos/mod_admin/views/modules/AdminModulesList.html",
            controller: "AdminModulesListCtrl",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Module Management",
                displayIcon: ""
            }
        })
        .state('admin.module', {
            url: '/module/:id',
            templateUrl: "mod_nbos/mod_admin/views/modules/AdminModule.html",
            controller: "AdminModuleCtrl",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module View",
                displayIcon: ""
            }
        })
        .state('admin.tenants', {
            url: '/tenants',
            templateUrl: "mod_nbos/mod_admin/views/tenants/admin.tenantsList.html",
            controller: "AdminTenantsListCtrl",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Tenant Management",
                displayIcon: ""
            }
        })

    .state('admin.tenant', {
        url: '/tenant/:id',
        templateUrl: "mod_nbos/mod_admin/views/tenants/admin.tenantView.html",
        controller: "AdminTenantViewCtrl",
        data: {
            moduleName: "Admin",
            type: 'nav',
            displayLink: false,
            disableLink: false,
            authenticate: true,
            displayName: "Tenant View",
            displayIcon: ""
        }
    })

    .state('admin.layouts', {
            url: '/layouts',
            templateUrl: "mod_nbos/mod_admin/views/layouts/admin.layouts.html",
            controller: "AdminLayoutsCtrl",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Layouts",
                displayIcon: ""
            }

        })
        .state('admin.idnConfiguration', {
            url: '/idnConfiguration',
            templateUrl: "mod_nbos/mod_admin/views/config/admin.idnConfiguration.html",
            controller: "IdnAdminConfigurationCtrl",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "IDN Configuration",
                displayIcon: ""
            }
        })
        .state('admin.appConfiguration', {
            url: '/appConfiguration',
            templateUrl: "mod_nbos/mod_admin/views/config/admin.appConfiguration.html",
            controller: "AdminAppConfigurationCtrl",
            data: {
                moduleName: "Admin",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "App Configuration",
                displayIcon: ""
            }
        })
}])
