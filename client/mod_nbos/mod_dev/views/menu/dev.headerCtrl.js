'use strict';

angular.module('mod.idn')
    .controller('DevHeaderCtrl', ['$scope', 'UserService', '$location', function($scope, UserService, $location) {
        $scope.isAdmin = false;
        var hostName = $location.host().split('dev.')[1];
        var hostPort = $location.port();

        $scope.consoleUrl = 'http://console.' + hostName + ":" + hostPort;
        $scope.adminUrl = 'http://admin.' + hostName + ":" + hostPort;

        var init = function() {
            if (UserService.roles && UserService.roles.isAdmin) {
                $scope.isAdmin = true;
            };
        };

        init();
    }]);
