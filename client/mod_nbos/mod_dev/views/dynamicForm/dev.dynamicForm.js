'use strict';

angular.module('mod.dev')
    .directive('devDynamicForm', function() {
        return {
            restrict: 'E',
            scope: {
                config: '=moduleConfig',
                updateConfig: '&onSubmit'
            },
            templateUrl: 'mod_nbos/mod_dev/views/dynamicForm/dev.dynamicForm-template.html',
            link: function(scope, element, attr) {
                scope.noConfig = false;

            },
            controller: ['$scope', 'AlertService', function($scope, AlertService) {
                $scope.config = [];
                $scope.$watchCollection('config', function() {
                    $scope.moduleConfig = [];
                    $scope.moduleConfig = clone($scope.config);
                    console.log($scope.config)

                    if ($scope.moduleConfig && $scope.moduleConfig.length == 0) {
                        $scope.noConfig = true;
                    } else {
                        $scope.noConfig = false;

                        if ($scope.moduleConfig) {
                            for (var i = 0; i < $scope.moduleConfig.length; i++) {
                                if ($scope.moduleConfig[i].values && $scope.moduleConfig[i].values.length > 0) {
                                    if ($scope.moduleConfig[i].possibleValues && $scope.moduleConfig[i].possibleValues.length > 0) {
                                        var values = [];
                                        var obj = {};

                                        $scope.module[i].values.forEach(function(element, index, array) {
                                            var found = false;
                                            for (var j = 0; j < $scope.moduleConfig[i].possibleValues.length; j++) {
                                                if (element == $scope.moduleConfig[i].possibleValues[j].value) {
                                                    found = true;
                                                    obj = {};
                                                    obj.name = $scope.moduleConfig[i].possibleValues[j].name;
                                                    obj.value = $scope.moduleConfig[i].possibleValues[j].value;
                                                    values.push(obj);
                                                };
                                            };
                                            if (!found) {
                                                //the value present is not in any of the possible values, so remove it.

                                            }
                                        });
                                        $scope.moduleConfig[i].values = values;
                                    };
                                }

                            };
                            console.log("END MODULE IF");
                        }; // END MODULE IF
                    }
                });

                $scope.transformChip = function(chip) {
                    // If it is an object, it's already a known chip
                    if (angular.isObject(chip)) {
                        return chip;
                    }
                    // Otherwise, create a new one
                    return { name: chip, type: 'new' }
                };

                $scope.editConfig = function(config) {
                    var configClone = clone(config);
                    $scope.updateConfig({ "config": configClone, 'task': 'edit' });
                };

                $scope.deleteConfig = function(config) {
                    var configClone = clone(config);
                    $scope.updateConfig({ "config": configClone, 'task': 'delete' });
                }

                var clone = function(obj) {
                    var copy;

                    // Handle the 3 simple types, and null or undefined
                    if (null == obj || "object" != typeof obj) return obj;

                    // Handle Date
                    if (obj instanceof Date) {
                        copy = new Date();
                        copy.setTime(obj.getTime());
                        return copy;
                    }

                    // Handle Array
                    if (obj instanceof Array) {
                        copy = [];
                        for (var i = 0, len = obj.length; i < len; i++) {
                            copy[i] = clone(obj[i]);
                        }
                        return copy;
                    }

                    // Handle Object
                    if (obj instanceof Object) {
                        copy = {};
                        for (var attr in obj) {
                            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
                        }
                        return copy;
                    }

                    throw new Error("Unable to copy obj! Its type isn't supported.");
                };

                $scope.showModule = function() {
                    console.log($scope.module.length);
                };
            }]
        }
    });
