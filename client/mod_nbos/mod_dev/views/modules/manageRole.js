angular.module('mod.dev')
.directive('manageRole', function(Module_RoleAuthService){
    return {
        restrict : 'E',
        transclude: true,
        scope : {
            uRoleName : '@',
            roleObj : '=',
            module : '=',
            moduleUpdate : '&'
        },
        link : function(scope, element, attrs){

            var createAuthoritiesList = function(){
                //TODO - should we load authorities of ALL modules?

                scope.moduleObj = _.clone(scope.module);

                _.each(scope.moduleObj.authorities, function(authority){

                    var found = false;
                    _.each(scope.roleObj.authorities, function(roleAuthority){
                        if(roleAuthority.uAuthorityName == authority.uAuthorityName){
                            found = true;
                        };
                    });
                    if(found){
                        authority.selected = true;
                    } else {
                        authority.selected = false;
                    };

                });

            };

            scope.$watch("uRoleName", function(value){
                console.log("ROLE CHANGED");
                if(scope.uRoleName != ""){
                    scope.isEdit = true;
                    scope.roleObj = _.clone(Module_RoleAuthService.role_edit);
                    var uRoleNameArr = scope.roleObj.uRoleName.split('.');
                    scope.roleObj.roleName = uRoleNameArr[uRoleNameArr.length - 1]; //The last array value
                    scope.roleObj.roleCategory = uRoleNameArr[uRoleNameArr.length - 2]; //The last but 1 array value

                    Module_RoleAuthService.role_edit = null;
                } else {
                    scope.isEdit = false;
                    scope.roleObj = {};
                };

                console.log(scope.roleObj);
                console.log(scope.isEdit);

                if(scope.module){
                    createAuthoritiesList();
                };
            });


        },
        controller : function($scope, $stateParams, AlertService){

            $scope.dummy = function(){
                console.log($scope.roleObj);
            };

            $scope.roleObj = {};
            $scope.roleObj.roleName = '';
            $scope.roleObj.roleCategory = '';
            $scope.isEdit = false;
            $scope.isDirty = false;

            var tenantId = $stateParams.id;
            var moduleId = $stateParams.moduleId;
            console.log(tenantId);

            $scope.coreAuthorities = [
                "authority:core.module.admin",
                "authority:identity.user.admin",
                "authority:oauth.client.admin",
                "authority:about.page.admin",
                "authority:oauth.token.admin"
            ];

            $scope.project = {
                description: 'Nuclear Missile Defense System',
                rate: 500
            };

            $scope.selectedAuthorities = [];

            // $scope.$watch('roleObj.roleName', function() {
            //     $scope.roleObj.roleName = $scope.roleObj.roleName.replace(/\s+/g,'');
            // });

            $scope.createRole = function(){

                $scope.roleObj.moduleId = $scope.module.uuid;
                $scope.roleObj.uRoleName = "role:" + $scope.module.name +"." + $scope.roleObj.roleCategory + "." + $scope.roleObj.roleName;
                console.log($scope.roleObj);
                Module_RoleAuthService.createRoleForModule($scope.roleObj).then(function(success){
                    console.log(success);
                    // Module_RoleAuthService.getRoles(tenantId);
                    $scope.moduleUpdate();
                    $scope.isEdit = true;
                }, function(error){
                    AlertService.alert("There was an error creating role.", "md-warn");
                });

            };

            $scope.editRole = function(){

                $scope.roleObj.moduleId = $scope.module.uuid;
                console.log($scope.roleObj);
                Module_RoleAuthService.updateRoleForModule($scope.roleObj).then(function(success){
                    console.log(success);
                    $scope.moduleUpdate();
                    // Module_RoleAuthService.getRoles(tenantId);
                    $scope.isEdit = true;
                }, function(error){
                    AlertService.alert("There was an error editing role.", "md-warn");
                });

            };



            $scope.addRemoveAuthority = function(ev, authority){

                var authorityObj = {
                    moduleId : $scope.module.uuid,
                    roleId : $scope.roleObj.uRoleName,
                    authorities : [
                        {
                            uAuthorityName : authority.uAuthorityName
                        }
                    ]
                };

                console.log(authorityObj);

                if(authority.selected){

                    Module_RoleAuthService.addAuthorityForRoleInModule(authorityObj).then(function(success){
                        console.log(success);
                        $scope.moduleUpdate();
                    }, function(error){
                        AlertService.alert("Error!", "md-warn")
                    });
                } else {
                    Module_RoleAuthService.deleteAuthorityForRoleInModule(authorityObj).then(function(success){
                        console.log(success);
                        $scope.moduleUpdate();
                    }, function(error){
                        AlertService.alert("Error!", "md-warn")
                    });
                };


            };


        },
        templateUrl : 'mod_nbos/mod_dev/views/modules/manage.role.template.html',
    }
})