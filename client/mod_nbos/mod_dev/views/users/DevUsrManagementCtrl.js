/**
 *  Module
 *
 * Description
 */
angular.module('mod.dev').
controller('DevUsrManagementCtrl', ['$scope', 'UserService', function($scope, UserService) {
	var tenantId = 'TNT:appConsole';
	var getUsers = function(){
		UserService.getTenantUsers(tenantId).then(function(response){
			$scope.users = response;
		}, function(response){
			console.log("error");
		})
	};

	var init = function(){
		getUsers();
	};
	init();
}]);
