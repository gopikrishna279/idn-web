angular.module('mod.dev')
    .controller('ModuleBeCtrl', ['$scope', 'StarterCodeService', 'ModuleService', 'AlertService', '$state', '$window', '$mdDialog', function($scope, StarterCodeService, ModuleService, AlertService, $state, $window, $mdDialog) {

        $scope.moduleClients = [];
        $scope.devTenants = {};
        var moduleKey;
        $scope.getStarterCode = function(ev, lang) {
            var obj = {};
            obj.moduleId = ModuleService.module.uuid;
            obj.lang = lang;
            obj.tenantId = $scope.devTenants.tenantId;
            StarterCodeService.get_starter_api_code(obj).then(function(success) {

                if (success.downloadUrl) {
                    $window.location.assign("http://" + success.downloadUrl);
                } else {
                    AlertService.alert("Something Went Wrong in downloading" + lang + "starter code", "md-warn");
                }
            }, function(error) {
                AlertService.alert("Something Went Wrong", "md-warn");
            });
        };

        $scope.updateSecret = function(ev) {

            var confirm = $mdDialog.prompt()
                .title('Please enter a new password.')
                .textContent('Use this password in your API\'s')
                .placeholder('password')
                .ariaLabel('password')
                .targetEvent(ev)
                .ok('Change')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function(result) {
                if (result != "") {
                    var obj = {
                        "clientSecret": result
                    }

                    ModuleService.updateSecret('TNT:ROOT', $scope.beClients.clientId, obj).then(function(response) {
                        AlertService.alert("The new secret was set successfully.", 'md-primary');
                    }, function(error) {
                        AlertService.alert("Error Updating Secret. Please try again", 'md-warn');
                    })
                }
            }, function() {
                //do nothing
            });
        };


        if (!ModuleService.module && !ModuleService.tokens) {
            $scope.$parent.manageNext = $state.current.name;
            $state.go('dev.module');
        } else {
            for (var i = 0, len = ModuleService.devTenants.length; i < len; i++) {
                if (ModuleService.module.name == ModuleService.devTenants[i].name) {
                    $scope.devTenants = ModuleService.devTenants[i];
                    break;
                };
            };
            $scope.beClients = ModuleService.apiClients[0];
            $scope.beClients.moduleKey = ModuleService.module.moduleKey;
        };
    }]);
