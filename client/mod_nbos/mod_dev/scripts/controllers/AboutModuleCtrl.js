angular.module('mod.dev')
    .controller('AboutModuleCtrl', ['$scope', 'ModuleService', 'AlertService', '$state', function($scope, ModuleService, AlertService, $state) {
        $scope.isLogoUpdated = false;
        $scope.updateModule = function() {
            $scope.isInfoEdit = false;
            $scope.moduleObj.tags = $scope.tags.toString();
            $scope.moduleObj.moduleUuid = $scope.moduleObj.uuid;
            ModuleService.updateModule($scope.moduleObj).then(function(response) {
                if ($scope.isLogoUpdated) {
                    updateLogo();
                }
                if ($scope.modImages && $scope.modImages.length) {
                    for (var i = 0, len = $scope.modImages.length; i < len; i++) {
                        updateModuleImages($scope.modImages[i]);
                    }
                }
                AlertService.alert("Module Updated Successfully", "md-primary");
            })

        };

        $scope.logoUpdated = function() {
            $scope.isLogoUpdated = true;
        };

        var updateLogo = function() {
            ModuleService.updateModuleLogo($scope.moduleId, $scope.moduleLogo).then(function(response) {
                if (response && response.mediaFileDetailsList) {
                    ModuleService.module.logoImage = response;
                    $scope.moduleLogo = response.mediaFileDetailsList[1].mediapath;
                }
            })
        };

        var updateModuleImages = function(file) {
            ModuleService.updateModuleImages($scope.moduleId, file).then(function(response) {
                if (response && response.mediaFileDetailsList) {
                    console.log(response.mediaFileDetailsList);
                }
            })
        }

        var init = function() {
            if (!ModuleService.module) {
                $scope.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {

                $scope.moduleObj = ModuleService.module;
                if (ModuleService.module.logoImage)
                    $scope.moduleLogo = ModuleService.module.logoImage.mediaFileDetailsList[1].mediapath;
                if (ModuleService.module.moduleImages && ModuleService.module.moduleImages.length) {
                    for (var i = 0, len = ModuleService.module.moduleImages.length; i < len; i++) {
                        $scope.moduleImages.push(ModuleService.module.moduleImages[i].mediaFileDetailsList[1].mediapath);
                    };
                }
                if (ModuleService.module.tags && ModuleService.module.tags != "") {
                    $scope.tags = ModuleService.module.tags.split(",");
                } else {
                    $scope.tags = [];
                };
            }
        }

        init();

    }]);
