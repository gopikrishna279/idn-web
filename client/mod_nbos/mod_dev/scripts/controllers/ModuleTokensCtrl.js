angular.module('mod.dev')
    .controller('ModuleTokensCtrl', ['$scope', 'ModuleService', '$state', 'AlertService', function($scope, ModuleService, $state, AlertService) {

        $scope.tokensObj = {
            "authorizedGrantTypes": [],
            "scopes": []
        };

        var getTokens = function() {
            $scope.isCreated = false;
            for (var i = 0, len = ModuleService.tokens.length; i < len; i++) {
                if (ModuleService.tokens[i].clientName.split('-')[0] === ModuleService.module.name) {
                    $scope.tokensObj = ModuleService.tokens[i];
                    $scope.isCreated = true;
                    break;
                };
            };
            if ($scope.isCreated) {
                //console.log($scope.tokensObj);
            } else {
                $scope.tokensObj = {
                    "clientName": ModuleService.module.name + "-module-client",
                    "tenantId": "TNT:ROOT",
                    "authorizedGrantTypes": ["client_credentials"],
                    "scopes": ["scope:oauth.token.verify"]
                };
            }

        };

        $scope.createTokens = function() {
            ModuleService.createModuleTokens($scope.tokensObj).then(function(response) {
                $scope.tokensObj = response;
                if (!ModuleService.tokens) {
                    ModuleService.tokens = [];
                };
                ModuleService.tokens.push(response);
                createDevToknes();
            }, function(response) {
                console.log(response);
            })
        };

        var createDevToknes = function() {
            var devTokens = {
                "name": ModuleService.module.name,
                "tenantId": ModuleService.module.name
            }
            ModuleService.createDevToknes(devTokens).then(function(response) {
                $scope.devTokens = response;
                if (!ModuleService.devTokens){
                    ModuleService.devTokens = [];
                }
                ModuleService.devTokens.push(response);
                $scope.isCreated = true;
                AlertService.alert("Token Created Successfully", "md-primary");
            }, function(response) {
                console.log(response);
            })
        };

        var init = function() {
            if (!ModuleService.module && !ModuleService.tokens && !ModuleService.devTokens) {
                $scope.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {
                for (var i = 0, len = ModuleService.devTokens.length; i < len; i++) {
                    if (ModuleService.module.name == ModuleService.devTokens[i].name) {
                        $scope.devTokens = ModuleService.devTokens[i];
                        break;
                    };
                };
                getTokens();
            }
        };
        init();
    }]);
