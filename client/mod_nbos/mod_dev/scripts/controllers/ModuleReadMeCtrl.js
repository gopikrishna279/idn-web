angular.module('mod.dev')
    .controller('ModuleReadMeCtrl', ['$scope', '$state', 'ModuleService', function($scope, $state, ModuleService) {

        $scope.guideObj = {};
        $scope.froalaOptions = {
            height: 400,

        };

        var getReadMe = function() {
            ModuleService.getGuide($scope.moduleId).then(function(response) {
                $scope.guideObj = response;
            }, function(response) {

            });
        };

        $scope.updateReadMe = function() {
            var obj = {
                "moduleId": $scope.moduleId,
                "description": $scope.guideObj.description
            };
            ModuleService.updateGuide($scope.moduleId, obj).then(function(response) {
                console.log(response);
            }, function(response) {
                console.log(response);
            })
        };

        var init = function() {
            if (!ModuleService.module) {
                $scope.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {
                getReadMe();
            };
        };

        init();

    }]);
