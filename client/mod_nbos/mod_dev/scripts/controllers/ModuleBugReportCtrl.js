angular.module('mod.dev')
    .controller('ModuleBugReportCtrl', ['$scope', '$state', 'ModuleService', function($scope, $state, ModuleService) {
        //$scope.bugs = $scope.moduleObj.bugReport;
        $scope.filter = {
            "status": "open"
        };

        var getAllBugs = function() {
            ModuleService.getAllBugs($scope.moduleId).then(function(response) {
                $scope.bugs = response;
            }, function(error) {
                console.log("error retrieving bugs");
            });
        }

        $scope.backToInfo = function() {
            $state.go('dev.module.info', {
                'id': $scope.moduleId
            });
        };

        var init = function() {
            if (!ModuleService.module) {
                console.log("here")
                $scope.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {
                getAllBugs();
            };
        };

        init();

    }]);
