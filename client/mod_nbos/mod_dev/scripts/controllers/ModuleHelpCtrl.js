angular.module('mod.dev')
    .controller('ModuleHelpCtrl', ['$scope', '$state', 'ModuleService', function($scope, $state, ModuleService) {

        $scope.guideObj = {};

        var init = function() {
            if (!ModuleService.guide) {
                $scope.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {
                if (ModuleService.guide)
                    $scope.guideObj = ModuleService.guide;
            };
        };

        init();

    }]);
