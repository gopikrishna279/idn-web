angular.module('mod.dev')
    .controller('ModuleRolesCtrl', ['$scope', 'ModuleService', 'AlertService', '$state', 'Module_RoleAuthService', '$mdDialog', function($scope, ModuleService, AlertService, $state, Module_RoleAuthService, $mdDialog) {

        $scope.moduleClients = [];
        $scope.devTenants = {};
        $scope.module = {};
        $scope.selectedIndex = 0;

        $scope.manageRoleObj_uRoleName = "";
        var moduleKey;


        $scope.getModule = function(){
            ModuleService.getModuleInfo($scope.module.uuid).then(function(success){
                $scope.module = ModuleService.module;
            }, function(error){
                AlertService.alert("Oops! Please refresh the page.")
            })
        };

        $scope.confirmDelete = function(ev, role) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Alert!')
                .textContent('Are you sure you want to delete this role?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Yes!')
                .cancel('No.');

            $mdDialog.show(confirm).then(function() {
                role.moduleId = $scope.module.uuid;
                console.log(role);
                //delete role
                Module_RoleAuthService.deleteRoleForModule(role).then(function(success){
                    $scope.getModule();
                }, function(error){
                    AlertService.alert("Oops! Error deleting role. Please try again.");
                });
            }, function() {
                //do nothing
            });
        };

        $scope.manageRole = function(role){

            if(role){
                Module_RoleAuthService.role_edit = _.clone(role);
                $scope.manageRoleObj_uRoleName = role.uRoleName;
            } else {
                $scope.manageRoleObj = {};
                $scope.manageRoleObj_uRoleName = "";
            };

            $scope.selectedIndex = 1;

        };

        $scope.backToList = function(){
            $scope.selectedIndex = 0;
        };


        if (!ModuleService.module && !ModuleService.tokens) {
            $scope.$parent.manageNext = $state.current.name;
            $state.go('dev.module');
        } else {
            $scope.module = ModuleService.module;
        };
    }]);
