angular.module('mod.dev')
    .controller('ManageModuleCtrl', ['$scope', '$stateParams', '$state', 'ModuleService', '$q', function($scope, $stateParams, $state, ModuleService, $q) {
        $scope.moduleId = $stateParams.id;

        $scope.showpage = 'info';

        $scope.trueValue = "Active";
        $scope.falseValue = "Draft";
        $scope.tags = [];
        $scope.moduleImages = [];
        $scope.moduleObj = {};
        $scope.showProgress = true;
        $scope.manageNext = null;


        $scope.publishModule = function(moduleObj) {
            ModuleService.publishModule($scope.moduleId, moduleObj).then(function(success) {
                console.log(success);
            }, function(error) {
                console.log(error);
            });
        };

        $scope.activateModule = function(moduleObj) {
            ModuleService.activateModule($scope.moduleId, moduleObj).then(function(success) {
                console.log(success);
            }, function(error) {
                console.log(error);
            });
        };

        var getModuleConfig = function() {
            ModuleService.getModuleConfig($scope.moduleId).then(function(response) {
                console.log(response);
            }, function(response) {

            });
        };


        var init = function() {
            if ($scope.moduleId) {
                var module = ModuleService.getModuleInfo($scope.moduleId),
                    config = ModuleService.getModuleConfig($scope.moduleId),
                    // bugs = ModuleService.getAllBugs($scope.moduleId),
                    // guide = ModuleService.getGuide($scope.moduleId),
                    types = ModuleService.getAttributeTypes(),
                    apiClients = ModuleService.getApiClients($scope.moduleId);
                    devTenants = ModuleService.getdevTenants();

                $q.all([module, config, types, apiClients, devTenants])
                    .then(function(success) {
                        $scope.showProgress = false;
                        $scope.moduleName = ModuleService.module.name;
                        if ($scope.manageNext) {
                            $state.go($scope.manageNext);
                        } else {
                            $state.go('dev.module.info', { id: $stateParams.id });
                        };

                    }, function(error) {
                        console.log(error);
                        $scope.showProgress = false;
                        $state.go('dev.modules');
                    });
            }else{
                $state.go('dev.modules');
            }
        };

        init();
        console.log("here");
    }]);
