angular.module('mod.dev')
    .controller('ModuleFeCtrl', ['$scope', 'StarterCodeService', 'ModuleService', 'AlertService', '$state', '$window', '$mdDialog', function($scope, StarterCodeService, ModuleService, AlertService, $state, $window, $mdDialog) {

        $scope.moduleClients = [];
        $scope.devTenants = {};
        var moduleKey;
        $scope.getStarterCode = function(lang) {
            if ($scope.moduleClients.length > 0) {
                var obj = {};
                obj.moduleId = ModuleService.module.uuid;
                obj.lang = lang;
                obj.tenantId = $scope.devTenants.tenantId;
                StarterCodeService.get_starter_api_code(obj).then(function(success) {

                    if (success.downloadUrl) {
                        $window.location.assign("http://" + success.downloadUrl);
                    } else {
                        AlertService.alert("Something Went Wrong in downloading" + lang + "starter code", "md-warn");
                    }
                }, function(error) {
                    AlertService.alert("Something Went Wrong", "md-warn");
                });
            } else {
                AlertService.alert("Please create atleast one client to download the starter code");
            }
        };

        var getTokens = function() {
            $scope.isCreated = false;
            ModuleService.getUiTokens($scope.devTenants.tenantId).then(function(response) {
                if (response && response.length) {
                    //console.log(response[0]);
                    $scope.moduleClients = response;
                    $scope.isCreated = true;
                }
            }, function(error) {
                console.log("error retreiving clients");
            });
        };

        $scope.buildTokens = function(ev, task) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, ModuleService, AlertService, tenantId, task) {
                        $scope.tokensObj = {};
                        $scope.isFirst = false;

                        //Checking if the user is creating the tokens for first time
                        if (task) {
                            $scope.isFirst = true;
                        }

                        $scope.tokensObj = {
                            //"clientName":+ "-module-client",
                            "tenantId": tenantId,
                            "authorizedGrantTypes": ["client_credentials"],
                            "scopes": ["scope:oauth.token.verify"]
                        };
                        $scope.create = function() {
                            ModuleService.createUiClients(tenantId, $scope.tokensObj).then(function(response) {
                                $scope.tokensObj = response;

                                $mdDialog.hide(response);
                            }, function(response) {
                                console.log(response);
                            })
                        };

                        $scope.closeDialog = function() {
                            $mdDialog.cancel();
                        }
                    },
                    templateUrl: 'mod_nbos/mod_dev/views/modules/TokenCreate.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    locals: {
                        tenantId: $scope.devTenants.tenantId,
                        task: task
                    }
                })
                .then(function(token) {
                    if (token) {
                        token.devTenantId = $scope.devTenants.tenantId;
                        token.moduleKey = moduleKey;
                        $scope.moduleClients.push(token);
                        $scope.isCreated = true;
                        if (task) {
                            $scope.getStarterCode(task);
                        }
                    };
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        }


        if (!ModuleService.module && !ModuleService.tokens) {
            $scope.$parent.manageNext = $state.current.name;
            $state.go('dev.module');
        } else {
            moduleKey = ModuleService.module.moduleKey;
            for (var i = 0, len = ModuleService.devTenants.length; i < len; i++) {
                if (ModuleService.module.name == ModuleService.devTenants[i].name) {
                    $scope.devTenants = ModuleService.devTenants[i];
                    break;
                };
            };
            getTokens();
        }
    }]);
