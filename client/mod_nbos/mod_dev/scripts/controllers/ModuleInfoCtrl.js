angular.module('mod.dev')
    .controller('ModuleInfoCtrl', ['$scope', '$state', 'ModuleService', 'AlertService', '$stateParams', '$mdDialog', function($scope, $state, ModuleService, AlertService, $stateParams, $mdDialog) {

        $scope.tags = [];

        $scope.readonly = false;

        $scope.updateStatus = function(status) {
            var obj = {
                "uuid": $scope.moduleId,
                "name": $scope.moduleObj.name,
                "moduleStatus": status
            }
            switch (status) {
                case 'Active':
                    if ($scope.moduleObj.apiDefinitionUrl == '') {
                        AlertService.alert("Please fill the api url", "md-warn");
                    } else {
                        $scope.activateModule(obj);
                    }
                    break;
                case 'PendingApproval':
                    $scope.publishModule(obj);
                    break;
            }

        };

        $scope.deleteModule = function(ev) {
            var confirm = $mdDialog.confirm()
                .title('Would You like to delete')
                .textContent('')
                .ariaLabel('')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('No');

            $mdDialog.show(confirm).then(function() {
                ModuleService.deleteModule($stateParams.id).then(function(response) {
                    AlertService.alert("Module Deleted Successfully", 'md-primary');
                    $state.go('dev.modules');
                }, function(error) {

                });
            }, function() {

            });
        };

        var init = function() {
            if (!ModuleService.module) {
                $scope.$parent.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {

                $scope.moduleObj = ModuleService.module;
                if (ModuleService.module.logoImage)
                    $scope.moduleLogo = ModuleService.module.logoImage.mediaFileDetailsList[1].mediapath;
                if (ModuleService.module.moduleImages && ModuleService.module.moduleImages.length) {
                    for (var i = 0, len = ModuleService.module.moduleImages.length; i < len; i++) {
                        $scope.moduleImages.push(ModuleService.module.moduleImages[i].mediaFileDetailsList[1].mediapath);
                    };
                }
                if (ModuleService.module.tags && ModuleService.module.tags != "") {
                    $scope.tags = ModuleService.module.tags.split(",");
                } else {
                    $scope.tags = [];
                };
            }
        };
        init();
    }]);
