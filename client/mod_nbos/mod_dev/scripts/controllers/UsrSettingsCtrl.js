angular.module('mod.dev')
    .controller('UsrSettingsCtrl', ['$scope', '$state', 'SessionService', 'UserService', function($scope, $state, SessionService, UserService) {

        $scope.userObj = {};
        var memberId;
        var getUserInfo = function() {
            if (memberId) {
                UserService.getUserObject(memberId).then(function(response) {
                    $scope.userObj = response;
                }, function(response) {

                })
            }
        };

        $scope.updateUser = function() {
            UserService.updateUser(memberId, $scope.userObj).then(function(response) {
                console.log(response);
            }, function(response) {

            });
        }
        var init = function() {
            if (SessionService.getSession()) {
                memberId = SessionService.getSession().uuid;
                getUserInfo();
            } else {
                $state.go('login');
            }
        }
        init();
    }]);
