angular.module('mod.dev')
    .controller('ModuleConfigCtrl', ['$scope', '$state', 'ModuleService', 'AlertService', '$mdDialog', '_', function($scope, $state, ModuleService, AlertService, $mdDialog, _) {



        var getConfig = function() {
            ModuleService.getModuleConfig($scope.moduleId).then(function(response) {
                configHelper(response);
            }, function(error) {
                console.log(error);
            })
        };
        $scope.configArr = [];


        // Config Array build Helper 
        var configHelper = function(arr) {
            $scope.configArr = [];
            for (var i = 0, len = arr.length; i < len; i++) {
                if (arr[i].possibleValues && arr[i].possibleValues.length) {
                    arr[i].isSelect = true;
                } else {
                    arr[i].isSelect = false;
                };
                $scope.configArr.push(arr[i]);
            }
        }

        //Delete a config
        $scope.deleteConfig = function(config) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete Config?')
                .textContent('')
                .ariaLabel('')
                .targetEvent(document.body)
                .ok('Please do it!')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                if (config.uAttributeName) {
                    ModuleService.deleteModuleConfig(config.uAttributeName).then(function(response) {
                        var position;
                        _.find($scope.configArr, function(list, index) {
                            if (list.uAttributeName == config.uAttributeName) {
                                position = index;
                            }
                        });
                        $scope.configArr.splice(position, 1);
                    }, function(error) {
                        console.log("error deleting config");
                    })
                } else {

                }
            }, function() {

            });
        };

        $scope.addConfig = function(config) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, ModuleService, AlertService, config) {
                        $scope.configTypes = ModuleService.configAttrs.configTypes;
                        $scope.configObj = {};
                        $scope.isNew = true;
                        if (config) {
                            $scope.configObj = config;
                            $scope.isNew = false;
                        }

                        $scope.addPossible = function() {
                            $scope.configObj.possibleValues.push({});
                        };

                        $scope.updateMulti = function(config) {
                            if (config.isSelect) {
                                config.type = "String";
                                config.possibleValues = [{}];
                            } else {
                                delete config['possibleValues'];
                            }
                        };

                        $scope.create = function() {
                            var finalArr = [];
                            var isValid = true;
                            if ($scope.configObj.isSelect && $scope.configObj.possibleValues && !$scope.configObj.possibleValues[0].name) {
                                AlertService.alert("Please choose atleast one value");
                                isValid = false;

                            } else {
                                if ($scope.configObj.isSelect) {
                                    finalArr.push({
                                        "name": $scope.configObj.name,
                                        "description": $scope.configObj.description,
                                        "type": $scope.configObj.type,
                                        "multiValued": $scope.configObj.multiValued,
                                        "required": $scope.configObj.required,
                                        "possibleValues": $scope.configObj.possibleValues
                                    });
                                } else {
                                    finalArr.push({
                                        "name": $scope.configObj.name,
                                        "description": $scope.configObj.description,
                                        "type": $scope.configObj.type,
                                        "multiValued": $scope.configObj.multiValued,
                                        "required": $scope.configObj.required,
                                        "possibleValues": null
                                    });
                                };
                            };

                            if (isValid) {
                                var configObj = {
                                    "moduleUuid": ModuleService.module.uuid,
                                    "configuration": finalArr
                                };
                                ModuleService.updateModuleConfig(configObj).then(function(response) {
                                    if ($scope.isNew) {
                                        AlertService.alert("Config added successfully", "md-primary");
                                    }else{
                                        AlertService.alert("Config updated successfully", "md-primary");
                                    }
                                    $mdDialog.hide();
                                }, function(response) {
                                    console.log("error");
                                });
                            }
                        };

                        $scope.closeDialog = function() {
                            $mdDialog.cancel();
                        }
                    },
                    templateUrl: 'mod_nbos/mod_dev/views/modules/dev.configCreate.html',
                    locals: {
                        "config": config
                    },
                    parent: angular.element(document.body),
                    targetEvent: document.body
                })
                .then(function() {
                    getConfig();
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        $scope.backToInfo = function() {
            $state.go('dev.module.info', {
                'id': $scope.moduleId
            });
        };

        $scope.modifyConfig = function(config, task) {
            if (task == 'delete') {
                $scope.deleteConfig(config);
            } else if (task == 'edit') {
                $scope.addConfig(config);
            }
        }

        $scope.postConfig = function() {
            var finalArr = [];
            var isValid = true;
            for (var i = 0, len = $scope.configArr.length; i < len; i++) {
                if ($scope.configArr[i].isSelect && $scope.configArr[i].possibleValues && !$scope.configArr[i].possibleValues[0].name) {
                    AlertService.alert("Please choose atleast one value");
                    isValid = fasle;
                    break;
                } else {
                    if ($scope.configArr[i].isSelect) {
                        finalArr.push({
                            "name": $scope.configArr[i].name,
                            "description": $scope.configArr[i].description,
                            "type": $scope.configArr[i].type,
                            "multiValued": $scope.configArr[i].multiValued,
                            "required": $scope.configArr[i].required,
                            "possibleValues": $scope.configArr[i].possibleValues
                        });
                    } else {
                        finalArr.push({
                            "name": $scope.configArr[i].name,
                            "description": $scope.configArr[i].description,
                            "type": $scope.configArr[i].type,
                            "multiValued": $scope.configArr[i].multiValued,
                            "required": $scope.configArr[i].required,
                            "possibleValues": null
                        });
                    };
                };

            };
            if (isValid) {
                var configObj = {
                    "moduleUuid": $scope.moduleId,
                    "configuration": finalArr
                };
                ModuleService.updateModuleConfig(configObj).then(function(response) {
                    AlertService.alert("Config added successfully", "md-primary");
                }, function(response) {
                    console.log("error");
                });
            }

        };

        var init = function() {
            if (!ModuleService.config) {
                $scope.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {
                $scope.configTypes = ModuleService.configAttrs.configTypes;
                if (ModuleService.config.length) {
                    configHelper(ModuleService.config);
                } else {
                    $scope.configArr = [];
                };
                getConfig();
            }
        }
        init();

    }]);
