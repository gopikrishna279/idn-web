angular.module('mod.dev')
    .controller('ModuleAuthoritiesCtrl', ['$scope', 'ModuleService', 'AlertService', '$state', '$mdMedia', '$mdDialog', function($scope, ModuleService, AlertService, $state, $mdMedia, $mdDialog) {

        $scope.moduleClients = [];
        $scope.devTenants = {};

        $scope.authorities = [];
        var moduleKey;


        $scope.getModule = function(){
            ModuleService.getModuleInfo($scope.module.uuid).then(function(success){
                $scope.module = ModuleService.module;
            }, function(error){
                AlertService.alert("Oops! Please refresh the page.")
            })
        };

        $scope.newAuthority = function(ev, authority){

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                controller: AuthoritiesDialogController,
                templateUrl: 'mod_nbos/mod_dev/views/modules/dev.module.authorities.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:false,
                fullscreen: useFullScreen,
                locals : {
                    authority : _.clone(authority),
                    module : $scope.module
                }
            })
                .then(function(success) {
                    AlertService.alert("Hurray! Authority created successfully.", "md-primary");

                    $scope.getModule();

                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        function AuthoritiesDialogController($scope, authority, $mdDialog, module) {
            $scope.authority = {};
            $scope.showProgress = false;
            var editMode = false;

            if(authority) {
                $scope.authority = authority;
                editMode = true;
                var uAuthorityNameArr = authority.uAuthorityName.split('.');
                $scope.authority.authorityName_category = uAuthorityNameArr[uAuthorityNameArr.length - 2];
                $scope.authority.authorityName_subCategory = uAuthorityNameArr[uAuthorityNameArr.length - 1];
            };
            $scope.authority.authorityName_prefix = "authority:" +module.name;

            $scope.hide = function() {
                $mdDialog.cancel();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $scope.showProgress = true;
                $scope.authority.uAuthorityName = $scope.authority.authorityName_prefix + "." + $scope.authority.authorityName_category +"." + $scope.authority.authorityName_subCategory;
                $scope.authority.moduleUuid = module.uuid;
                $scope.authority.moduleUuid = module.uuid;
                console.log($scope.authority);
                if(editMode){
                    ModuleService.updateAuthorityForModule($scope.authority).then(function(success){
                        $scope.showProgress = false;
                        $mdDialog.hide(success);
                    }, function(error){
                        $scope.showProgress = false;
                        AlertService.alert("Oops! Error saving authority. Please refresh and try again.", "md-warn")
                    });
                } else {
                    ModuleService.createAuthorityForModule($scope.authority).then(function(success){
                        $scope.showProgress = false;
                        $mdDialog.hide(success);
                    }, function(error){
                        $scope.showProgress = false;
                        AlertService.alert("Oops! Error saving authority. Please refresh and try again.", "md-warn")
                    });
                };


            };
        };


        if (!ModuleService.module && !ModuleService.tokens) {
            $scope.$parent.manageNext = $state.current.name;
            $state.go('dev.module');
        } else {
            $scope.module = ModuleService.module;
        };
    }]);
