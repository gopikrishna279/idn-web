angular.module('mod.dev')
    .controller('ModulesListCtrl', ['$scope', '$state', '$mdDialog', 'ModuleService', 'AlertService', '_', function($scope, $state, $mdDialog, ModuleService, AlertService, _) {

        $scope.modules = [];
        $scope.logo = "mod_nbos/assets/images/project.png";
        $scope.cover = "mod_nbos/assets/images/cover.jpg";

        //List of all modules
        var getAllModules = function() {
            ModuleService.getModulesList().then(function(response) {
                $scope.modules = response;
            }, function(response) {

            });
        };

        //Create New Module
        $scope.createModule = function(ev) {
            $mdDialog.show({
                    controller: function($scope, $mdDialog, ModuleService, AlertService) {
                        $scope.moduleObj = {
                            "clientSecret": "nbosSecret",
                            "moduleType": "BOTH"
                        };
                        $scope.create = function() {
                            ModuleService.saveModule($scope.moduleObj).then(function(response) {
                                if (response) {
                                    $mdDialog.hide(response);
                                };
                            }, function(error) {
                                console.log("error saving module");
                                AlertService.alert("Module Name already Exist", 'md-warn');
                            });
                        };

                        $scope.closeDialog = function() {
                            $mdDialog.cancel();
                        }
                    },
                    templateUrl: 'mod_nbos/mod_dev/views/modules/ModuleCreate.html',
                    parent: angular.element(document.body),
                    targetEvent: ev
                })
                .then(function(module) {
                    if (module) {
                        $scope.modules.push(module);
                        createDevTenant(module);
                    };
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        var createDevTenant = function(module) {
            var tenant = {
                "name": module.name,
                "tenantId": module.name
            }
            ModuleService.createdevTenants(tenant).then(function(response) {
                AlertService.alert("Module Created Successfully", "md-primary");
            }, function(response) {
                console.log(response);
            })
        };

        $scope.deleteModule = function(ev, id) {
            var confirm = $mdDialog.confirm()
                .title('Would You like to delete')
                .textContent('')
                .ariaLabel('')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('No');

            $mdDialog.show(confirm).then(function() {
                ModuleService.deleteModule(id).then(function(response) {
                    var pos;
                    _.each($scope.modules, function(item, index) {
                        if (item.uuid == id) {
                            pos = index;
                        }
                    });
                    $scope.modules.splice(pos, 1);
                    AlertService.alert("Module Deleted Successfully", 'md-primary');
                }, function(error) {

                });
            }, function() {

            });
        };

        $scope.manageModule = function(id) {
            $state.go('dev.module.info', {
                "id": id
            })
        }

        var init = function() {
            ModuleService.module = null;
            ModuleService.config = null;
            getAllModules();
        };

        init();


    }]);
