angular.module('mod.dev')
    .controller('ModuleSwaggerCtrl', ['$scope', '$state', 'ModuleService', '$rootScope', 'SessionService', function($scope, $state, ModuleService, $rootScope, SessionService) {
        $scope.guideObj = {};
        $scope.showApi = false;
        $scope.user = {};

        $scope.generate = function() {
            $rootScope.admin_user_token = $scope.user.token;
            $scope.showApi = true;
        };

        $scope.useToken = function() {
            $scope.user.token = SessionService.getStoredUserToken();
        };

        $scope.showAbout = function() {
            $state.go('dev.module.about');
        };

        var init = function() {
            if (!ModuleService.module) {
                $scope.$parent.manageNext = $state.current.name;
                $state.go('dev.module');
            } else {
                if (ModuleService.module)
                    $scope.moduleObj = ModuleService.module;
            };
        };

        init();

    }]);
