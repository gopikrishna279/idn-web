angular.module('mod.dev')

.factory('ModuleService', ['$q', 'ModuleFactory', 'Upload', 'MOD_NBOS', 'SessionService', function($q, ModuleFactory, Upload, MOD_NBOS, SessionService) {

    var modService = {};
    modService.module;
    modService.config;
    modService.bugs;
    modService.guide;
    modService.tokens;
    modService.devTenants;
    modService.apiClients;

    modService.saveModule = function(moduleObj) {
        var deferred = $q.defer();

        ModuleFactory.modules().save(moduleObj, function(success) {
            deferred.resolve(success);
        }, function(error) {

            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.updateModule = function(moduleObj) {
        var deferred = $q.defer();

        ModuleFactory.modules().update(moduleObj, function(success) {
            deferred.resolve(success);
        }, function(error) {

            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.deleteModule = function(moduleId) {
        var deferred = $q.defer();

        ModuleFactory.deleteModule().delete({ "moduleId": moduleId }, function(success) {
            deferred.resolve(success);
        }, function(error) {

            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.getModulesList = function() {
        var deferred = $q.defer();

        ModuleFactory.userModules().get({ 'tenantId': 'TNT:ROOT' }, function(success) {
            deferred.resolve(success);
        }, function(error) {

            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.getModuleInfo = function(moduleId) {
        var deferred = $q.defer();
        ModuleFactory.getModule().get({ moduleId: moduleId }, function(success) {
            modService.module = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    modService.publishModule = function(moduleUuid, moduleObj) {
        var deferred = $q.defer();
        ModuleFactory.publishModule().publish({ moduleUuid: moduleUuid }, moduleObj, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    /*
        everything to do with authorities
     */
    modService.createAuthorityForModule = function(authority) {
        var deferred = $q.defer();

        ModuleFactory.moduleAuthority().save(authority, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    modService.updateAuthorityForModule = function(authority) {
        var deferred = $q.defer();

        ModuleFactory.moduleAuthority().update(authority, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    /*
        TODO : THIS SHOULD BE AN ADMIN TASK ONLY
     */
    modService.activateModule = function(moduleId, moduleObj) {
        var deferred = $q.defer();
        ModuleFactory.activateModule().activate({ moduleUuid: moduleId }, moduleObj, function(success) {

            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    };

    modService.updateModuleLogo = function(id, file) {
        var deferred = $q.defer();
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        Upload.upload({
            url: MOD_NBOS.API_URL + 'core/v0/modules/' + id + '/logo',
            headers: {
                'Authorization': bearer
            },
            file: file,
        }).success(function(data, status, headers, config) {
            console.log(data);
            deferred.resolve(data);

        }).error(function(error) {
            deferred.reject();

        });
        return deferred.promise;
    };

    modService.updateModuleImages = function(id, file) {
        var deferred = $q.defer();
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        Upload.upload({
            url: MOD_NBOS.API_URL + 'core/v0/modules/' + id + '/image',
            headers: {
                'Authorization': bearer
            },
            file: file,
        }).success(function(data, status, headers, config) {
            deferred.resolve(data);

        }).error(function(error) {
            deferred.reject();

        });
        return deferred.promise;
    };

    //Get Configuration for a Module
    modService.getModuleConfig = function(moduleId) {
        var deferred = $q.defer();
        ModuleFactory.moduleConfig().get({ 'moduleId': moduleId }, function(success) {
            deferred.resolve(success);
            modService.config = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.updateModuleConfig = function(config) {
        var deferred = $q.defer();
        ModuleFactory.moduleConfig().update(config, function(success) {
            modService.getModuleConfig(config.moduleUuid);
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.deleteModuleConfig = function(attr) {
        var deferred = $q.defer();
        ModuleFactory.configAttr().delete({ 'types': attr }, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    modService.getUiTokens = function(tenantId) {
        var deferred = $q.defer();

        ModuleFactory.tokens().get({ 'tenantId': tenantId }, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.createUiClients = function(tenantId, token) {
        var deferred = $q.defer();

        ModuleFactory.tokens().save({ 'tenantId': tenantId }, token, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.getdevTenants = function() {
        var deferred = $q.defer();

        ModuleFactory.devTenants().get(function(success) {
            deferred.resolve(success);
            modService.devTenants = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.createdevTenants = function(token) {
        var deferred = $q.defer();

        ModuleFactory.devTenants().save(token, function(success) {
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.getApiClients = function(moduleId) {
        var deferred = $q.defer();

        ModuleFactory.apiClients().get({ 'moduleId': moduleId }, function(success) {
            deferred.resolve(success);
            modService.apiClients = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.updateSecret = function(tenantId, clientId, secret) {
        var deferred = $q.defer();
        ModuleFactory.clientSecret().update({ tenantId: tenantId, clientId: clientId }, secret, function(success) {
            //factory.projectClients = success;
            deferred.resolve(success);
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    //get all the supported attribute types for a configuration
    modService.getAttributeTypes = function() {
        var deferred = $q.defer();
        ModuleFactory.configAttr().get({ 'types': 'types' }, function(success) {
            deferred.resolve(success);
            modService.configAttrs = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    }

    modService.getAllBugs = function(moduleId) {
        var deferred = $q.defer();

        ModuleFactory.bugReports().get({ 'moduleId': moduleId }, function(success) {
            deferred.resolve(success);
            modService.bugs = success;
        }, function(error) {

            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.getGuide = function(moduleId) {
        var deferred = $q.defer();

        ModuleFactory.guide().get({ 'moduleId': moduleId }, function(success) {
            deferred.resolve(success);
            modService.guide = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    modService.updateGuide = function(moduleId, guide) {
        var deferred = $q.defer();

        ModuleFactory.guide().save({ 'moduleId': moduleId }, guide, function(success) {
            deferred.resolve(success);
            modService.guide = success;
        }, function(error) {
            deferred.reject(error);
        });
        return deferred.promise;
    };

    return modService;

}])

.factory('ModuleFactory', ['$resource', 'MOD_NBOS', 'MOD_NBOS', 'SessionService', function($resource, MOD_NBOS, MOD_NBOS, SessionService) {
    var modFactory = {};

    modFactory.modules = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:mine', { mine: '@mine' }, {
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    "Authorization": bearer
                }

            },
            'save': {
                method: 'POST',
                headers: {
                    'Authorization': bearer
                }
            },
            'update': {
                method: 'PUT',
                headers: {
                    'Authorization': bearer
                }
            }
        })
    };

    modFactory.getModule = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleId/show', {}, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    modFactory.deleteModule = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleId/delete', {
            moduleId: '@moduleId'
        }, {
            'delete': {
                method: 'DELETE',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    modFactory.moduleAuthority = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleId/authorities', { moduleId: '@moduleUuid' }, {
            'save': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            },
            'update': {
                method: 'PUT',
                headers: {
                    Authorization: bearer
                }
            },
            'remove': {
                method: 'DELETE',
                headers: {
                    Authorization: bearer
                },
                isArray: true
            }
        })
    };

    modFactory.publishModule = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleUuid/publish', { moduleUuid: '@moduleUuid' }, {
            'publish': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    modFactory.activateModule = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleUuid/activate', { moduleUuid: '@moduleUuid' }, {
            'activate': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    modFactory.moduleConfig = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.API_URL + 'core/v0/config/module/:moduleId', { moduleId: '@moduleId' }, {
            'update': {
                method: 'PUT',
                headers: {
                    Authorization: bearer
                }
            },
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    modFactory.configAttr = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.API_URL + 'core/v0/config/attribute/:types', {
            types: '@types'
        }, {
            'get': {
                method: 'GET',
                isArray: false,
                headers: {
                    Authorization: bearer
                }
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    Authorization: bearer
                }
            }
        })
    }

    modFactory.tokens = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.API_URL + 'oauth/v0/tenants/:tenantId/clients', {
            tenantId: '@tenantId'
        }, {
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    Authorization: bearer
                }
            },
            'save': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    }

    modFactory.devTenants = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.API_URL + 'tenant/v0/developer/tenants', {

        }, {
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    Authorization: bearer
                }
            },
            'save': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    modFactory.apiClients = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.API_URL + 'core/v0/modules/:moduleId/clients', {
            moduleId: '@moduleId'
        }, {
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    Authorization: bearer
                }
            },
            'save': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };


    modFactory.bugReports = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.NODE_API + 'modules/:moduleId/bugs', {
            moduleId: '@moduleId'
        }, {
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    "Authorization": bearer
                }
            }

        })
    }

    modFactory.guide = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();
        return $resource(MOD_NBOS.NODE_API + 'module/:moduleId/guide', {
            moduleId: '@moduleId'
        }, {
            'get': {
                method: 'GET',
                isArray: false,
                headers: {
                    "Authorization": bearer
                }
            },
            'save': {
                method: 'POST',
                headers: {
                    "Authorization": bearer
                }
            }

        })
    };

    modFactory.clientSecret = function() {

        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'oauth/v0/tenants/:tenantId/clients/:clientId/secret', { tenantId: '@tenantId', clientId: '@clientId' }, {
            'get': {
                method: 'GET',
                headers: {
                    Authorization: bearer
                },
                isArray: true
            },
            'save': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            },
            'delete': {
                method: 'DELETE',
                headers: {
                    Authorization: bearer
                }
            },
            'update': {
                method: 'POST',
                headers: {
                    Authorization: bearer
                }
            }
        })
    };

    modFactory.userModules = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'core/v0/tenants/:tenantId/modules/mine', { tenantId: '@tenantId' }, {
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    "Authorization": bearer
                }

            }
        })
    };

    return modFactory;
}])
