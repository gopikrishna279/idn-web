angular.module('mod.dev')

    .factory('StarterCodeService', ['$q', 'StarterCodeFactory', 'SessionService', function($q, StarterCodeFactory, SessionService) {

        var factory = {};

        factory.get_starter_api_code = function(moduleObj){
            var deferred = $q.defer();

            StarterCodeFactory.starter_api_node().getCode(moduleObj, function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return factory;
    }])
    .factory('StarterCodeFactory', ['$resource', 'MOD_NBOS', 'SessionService', function($resource, MOD_NBOS, SessionService) {
        var codeFactory = {};

        codeFactory.starter_api_node = function(){
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            return $resource(MOD_NBOS.BUILD_API + "dev/:tenantId/module/:moduleId/build/:lang", {moduleId : '@moduleId', tenantId: '@tenantId'}, {
                'getCode' : {
                    method: 'GET',
                    headers: {
                        'Authorization': bearer
                    }
                }
            })
        };

        return codeFactory;
    }]);