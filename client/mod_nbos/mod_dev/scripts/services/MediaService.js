angular.module('mod.dev')
    .factory('MediaService', ['$q', 'MediaFactory', 'MOD_NBOS', 'SessionService', 'Upload', function($q, MediaFactory, MOD_NBOS, SessionService, Upload) {


        var mediaService = {};
        mediaService.getMedia = function(id, mediaFor) {
            var deferred = $q.defer();

            MediaFactory.media().get({ 'id': id, 'mediafor': mediaFor }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        mediaService.updateMedia = function(id, mediaFor, file) {
            var deferred = $q.defer();
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            Upload.upload({
                url: MOD_NBOS.MEDIA_URL + 'media?mediafor=' + mediaFor + '&id=' + id,

                headers: {
                    'Authorization': bearer
                },
                file: file,
            }).success(function(data, status, headers, config) {
                //service.creative = data.creative;
                console.log(data);
                deferred.resolve(data);

            }).error(function(error) {
                deferred.reject();

            });
            return deferred.promise;
        }

        return mediaService;
    }])
    
    .factory('MediaFactory', ['$resource', 'MOD_NBOS', 'SessionService', function($resource, MOD_NBOS, SessionService) {
        var mediaFactory = {};
        mediaFactory.media = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.MEDIA_URL + 'media', {

            }, {
                'get': {
                    method: 'GET',
                    isArray: false,
                    headers: {
                        "Authorization": bearer
                    }

                },
                'save': {
                    method: 'POST',
                    headers: {
                        'Authorization': bearer
                    }
                }
            })
        };
        return mediaFactory;
    }]);
