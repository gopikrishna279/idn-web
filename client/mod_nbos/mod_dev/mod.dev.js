/**
 *
 */

'use strict';

angular.module('mod.dev', ['ngFileUpload', 'ngSanitize', 'swaggerUi'])
    .service('swaggerModule', ['$rootScope', '$q', function($rootScope, $q) {

        this.execute = function(req) {
            var bearer = "Bearer " + $rootScope.admin_user_token;
            req.headers.Authorization = bearer;
            var deferred = $q.defer();
            deferred.resolve(true);
            return deferred.promise;
        }

    }])
    .run(function(swaggerModules, swaggerModule) {
        swaggerModules.add(swaggerModules.BEFORE_EXPLORER_LOAD, swaggerModule);
    })

.config(['$stateProvider', function($stateProvider) {
    $stateProvider
        .state('dev', {
            url: '/dev',
            parent: 'layout',
            template: "<div ui-view></div>",
            data: {
                type: 'nav'
            }
        })
        //FOR NAVIGATION
        .state('dev.header', {
            url: '',
            template: "<div></div>",
            data: {
                templateUrl: 'mod_nbos/mod_dev/views/menu/dev.header.html',
                position: 1,
                moduleName: "Developer",
                type: 'header',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Profile",
                displayIcon: ""
            }
        })
        .state('dev.dashboard', {
            url: '/dash',
            templateUrl: "mod_nbos/mod_dev/views/dashboard/dev.dashboard.html",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Dashboard",
                displayIcon: ""
            }
        })
        .state('dev.modules', {
            url: '/modules',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.modulesList.html",
            controller: "ModulesListCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Modules",
                displayIcon: ""
            }
        })
        .state('dev.module', {
            url: '/module/:id',
            templateUrl: "mod_nbos/mod_dev/views/modules/ManageModule.html",
            controller: "ManageModuleCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module View",
                displayIcon: ""
            }
        })
        .state('dev.module.info', {
            url: '/info',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.info.html",
            data: {
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module Info",
                displayIcon: ""
            }
        })
        .state('dev.module.config', {
            url: '/config',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.config.html",
            controller: "ModuleConfigCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module Config",
                displayIcon: ""
            }
        })
        .state('dev.module.bugReport', {
            url: '/bugs',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.bug.html",
            controller: "ModuleBugReportCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.module.starterCode', {
            url: '/starterCode',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.starter.html",
            controller: "ModuleStarterCodeCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.module.fe', {
            url: '/fe',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.fe.html",
            controller: "ModuleFeCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module Front End",
                displayIcon: ""
            }
        })
        .state('dev.module.be', {
            url: '/be',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.be.html",
            controller: "ModuleBeCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module Back End",
                displayIcon: ""
            }
        })
        .state('dev.module.authorities', {
            url: '/authorities',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.authorities.html",
            controller: "ModuleAuthoritiesCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module Back End",
                displayIcon: ""
            }
        })
        .state('dev.module.roles', {
            url: '/roles',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.roles.html",
            controller: "ModuleRolesCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module Back End",
                displayIcon: ""
            }
        })
        .state('dev.module.about', {
            url: '/about',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.about.html",
            controller: "AboutModuleCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.module.readMe', {
            url: '/readMe',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.readme.html",
            controller: "ModuleReadMeCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.module.tokens', {
            url: '/tokens',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.tokens.html",
            controller: "ModuleTokensCtrl",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.module.help', {
            url: '/help',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.help.html",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.module.swagger', {
            url: '/swagger',
            templateUrl: "mod_nbos/mod_dev/views/modules/dev.module.swagger.html",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.users', {
            url: '/users',
            templateUrl: "mod_nbos/mod_dev/views/users/dev.users.html",
            data: {
                type: 'home',
                menu: true,
                name: "User Management",
                module: "Developer"
            }
        })

    .state('dev.guide', {
            url: '/guide',
            templateUrl: "mod_nbos/mod_dev/views/guide/dev.guide.html",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module",
                displayIcon: ""
            }
        })
        .state('dev.forum', {
            url: '/forum',
            template: "<div>FORUM</div>",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: true,
                disableLink: true,
                authenticate: true,
                displayName: "Forum",
                displayIcon: ""
            }
        })
        .state('dev.settings', {
            url: '/settings',
            template: "<div>USER SETTINGS</div>",
            data: {
                moduleName: "Developer",
                type: 'nav',
                displayLink: true,
                disableLink: true,
                authenticate: true,
                displayName: "User Settings",
                displayIcon: ""
            }
        })

}]);


var underscore = angular.module('underscore', []);
underscore.factory('_', ['$window', function($window) {
    return $window._; // assumes underscore has already been loaded on the page
}]);
