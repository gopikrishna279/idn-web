angular.module('mod.admin', ['ngFileUpload']);
angular.module('mod.console', ['ngFileUpload']);
angular.module('mod.dev', ['ngFileUpload']);

angular.module('mod.nbos', []);

angular.module('mod.nbos')

    .constant('MOD_NBOS', {
        // API_URL: 'http://10.9.9.44:8080/api/',
        API_URL: 'https://idn.au-syd.mybluemix.net/api/',
        API_URL_DEV: 'https://idn.au-syd.mybluemix.net/api/',
        MEDIA_URL: 'https://idn.au-syd.mybluemix.net/api/media/v0/',
        NODE_API: 'http://nodeapi2.nbos.io/api/',
        NODE_API_DEV: 'http://localhost:3002/',
        BUILD_API: 'http://nodeapi1.nbos.io/api/',
        //BUILD_API: 'http://localhost:3001/api/',
        QUALIFIER: 'NBOS'
    })
    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('nbos', {
                url: '/nbos',
                template: '<div ui-view></div>',
                controller: 'NBOSCtrl',
                data: {
                    type: "Authenticated"
                }
            })
    }])

    .controller('NBOSCtrl', ['$scope', '$state', '$location', function ($scope, $state, $location) {
        var domain = $location.host().split('.');
        if (domain.indexOf('admin') > -1) {
            //redirect to admin
            $state.go("adm.dashboard");
        } else if (domain.indexOf('dev') > -1) {
            //redirect to dev
            $state.go("dev.dashboard");
        } else {
            //console
            $state.go("con.dashboard");
        }
    }]);