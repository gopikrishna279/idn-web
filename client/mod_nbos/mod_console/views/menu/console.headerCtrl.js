'use strict';

angular.module('mod.idn')
    .controller('ConHeaderCtrl', ['$scope', 'USER_CONFIG', '$location', function($scope, USER_CONFIG, $location) {
        $scope.isAdmin = false;
        var hostName = $location.host().split('console.')[1];
        var hostPort = $location.port();

        $scope.devUrl = 'http://dev.' + hostName + ":" + hostPort;
        $scope.adminUrl = 'http://admin.' + hostName + ":" + hostPort;

        var init = function() {
            if (USER_CONFIG.roles && USER_CONFIG.roles.isAdmin) {
                $scope.isAdmin = true;
            };
        };

        init();
    }]);