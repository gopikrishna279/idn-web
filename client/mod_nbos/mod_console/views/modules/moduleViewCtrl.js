angular.module('mod.console')

.controller('ModuleViewCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'ModuleService_Console', 'AlertService', '$rootScope', 'SessionService', function($scope, $state, $stateParams, ManageService, ModuleService_Console, AlertService, $rootScope, SessionService) {


    $scope.module = {};
    $scope.tags = [];
    var moduleId, tenantId;
    $scope.showApi = false;
    $scope.showConfig = false;
    $scope.isPageLoad = false;
    $scope.showSubscribed = false;
    $scope.showUnSubscribed = false;

    $scope.currentNavItem = 0;

    $scope.showImage = function(index) {
        $scope.currentNavItem = index;
    };

    $scope.user = {};

    $scope.generateSwagger = function() {
        $rootScope.admin_user_token = $scope.user.token;
        $scope.showApi = true;
    };

    $scope.useToken = function() {
        $scope.user.token = SessionService.getStoredUserToken();
    };

    //$scope.showSubscribed = $scope.$parent.$parent.showSubscribe;

    $scope.placeholder = 'mod_nbos/assets/images/placeholder.png';


    $scope.subscribe = function() {

        ModuleService_Console.subscribeModule(tenantId, moduleId).then(function(success) {
            var id = $stateParams.id;
            ManageService.getSubscribedModules(id).then(function() {
                $scope.showSubscribed = false;
                $scope.showUnSubscribed = true;
                AlertService.alert("Module Subscribed Successfully", "md-success");
            }, function(error) {

            });
        }, function(error) {
            console.error(error);
        });
    };

    $scope.unsubscribe = function() {
        var myObj = {
            tenantId: tenantId,
            moduleUuid: moduleId
        }

        ModuleService_Console.unsubscribeModule(myObj).then(function(success) {
            var id = $stateParams.id;
            AlertService.alert("Hurray! Module Unsubscribed Successfully", "md-success");
            $scope.showSubscribed = true;
            $scope.showUnSubscribed = false;
            //$state.go('con.manage.dash');
        }, function(error) {
            console.error(error);
        });
    };

    var prepareDisplay = function() {
        if ($scope.module.tags) {
            $scope.tags = $scope.module.tags.split(',');
        } else {
            $scope.tags = [];
        }
        $scope.isPageLoad = true;
    };

    $scope.saveConfigForModule = function(module) {
        module.tenantId = $stateParams.id;
        ManageService.saveConfig(module).then(function(success) {
            $scope.moduleConfig = module;
            if ($scope.module.configureEndPoint != "") {
                var obj = {
                    "config": $scope.moduleConfig.configuration,
                    "tenantId": $stateParams.id
                }
                ManageService.notifyModule($scope.module.configureEndPoint, obj).then(function(response) {
                    AlertService.alert("Configuration successfully saved for " + module.name, "md-primary");
                }, function(error) {
                    console.log("error notifying module");
                })
            } else {
                AlertService.alert("Configuration successfully saved for " + module.name, "md-primary");
            }
        }, function(error) {
            AlertService.alert("Error in saving configuration for " + module.name, "md-warn");
        });
    };

    var showOnlyModuleConf = function(modulesArr) {

        for (var i = 0; i < modulesArr.length; i++) {
            if (modulesArr[i].uuid == moduleId) {
                $scope.moduleConfig = modulesArr[i];
            }
        };
    };

    var init = function() {
        moduleId = $stateParams.moduleId;
        tenantId = $stateParams.id;
        if (moduleId) {
            ModuleService_Console.getModule(tenantId, moduleId).then(function(success) {
                $scope.module = success;
                if ($scope.module.subscription && $scope.module.subscription.status == 'Active') {
                    $scope.showSubscribed = false;
                    $scope.showUnSubscribed = true;
                } else {
                    $scope.showSubscribed = true;
                    $scope.showUnSubscribed = false;
                };
                prepareDisplay();

            }, function(error) {
                console.error(error);
            });

        } else {
            $state.go('con.manage.market');
        }

        if ($state.current.name == 'con.manage.module') {
            $scope.showConfig = true;
            if (!ManageService.projectConfig) {
                $scope.$parent.$parent.manageNext = $state.current.name;
                $state.go('con.manage');
            } else {

                $scope.project = ManageService.project;
                $scope.projectAbout = ManageService.projectAbout;
                showOnlyModuleConf(ManageService.projectConfig);
            };
        } else {
            $scope.showConfig = false;
        }
    };

    init();

}]);
