angular.module('mod.console')

    .controller('ModuleMarketplaceCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'ModuleService_Console', function($scope, $state, $stateParams, ManageService, ModuleService_Console){


        $scope.modules = [];

        $scope.placeholder = 'mod_nbos/assets/images/cover.jpg';

        $scope.showModule = function(module){
            $state.go('con.manage.view', {moduleId : module.uuid})
        };

        var init = function(){
            if(!ModuleService_Console.allModules){
                ModuleService_Console.getAllModules().then(function(success){
                    $scope.modules = success;
                }, function(error){
                    console.error(error);
                });
            }
        };

        init();

    }]);