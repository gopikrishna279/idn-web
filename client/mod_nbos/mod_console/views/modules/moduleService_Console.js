'use strict';

angular.module('mod.console')

    .factory('ModuleService_Console',  ['ModuleFactory_Console', '$q', function(ModuleFactory_Console, $q){
        var factory = {};

        factory.allModules;
        factory.module;

        factory.getAllModules = function(tenantId){
            var deferred = $q.defer();
            ModuleFactory_Console.allModules().get({tenantId:tenantId}, function(success){
                factory.allLayouts = success;
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        factory.getModule = function(tenantId, moduleId){
            var deferred = $q.defer();
            ModuleFactory_Console.getModule().get({tenantId: tenantId, moduleId: moduleId}, function(success){
                factory.module = success;
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        factory.subscribeModule = function(tenantId, moduleId){

            var deferred = $q.defer();
            ModuleFactory_Console.subscribeModule().subscribe({tenantId:tenantId, moduleUuid: moduleId}, function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        factory.unsubscribeModule = function(obj){
            var deferred = $q.defer();

            ModuleFactory_Console.unsubscribeModule(obj).then(function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return factory;

    }])

    .factory('ModuleFactory_Console', ['MOD_NBOS', 'SessionService', '$resource', '$http', function(MOD_NBOS, SessionService, $resource, $http){
        var factory = {};


        factory.allModules = function(){

            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'core/v0/modules/list', {}, {
                'get' : {
                    method : 'GET',
                    headers : {
                        Authorization : bearer
                    },
                    isArray : true
                }
            })
        };


        factory.getModule = function(){

            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'core/v0/tenants/:tenantId/modules/:moduleId/show', {}, {
                'get' : {
                    method : 'GET',
                    headers : {
                        Authorization : bearer
                    }
                }
            })
        };

        factory.getModule_no = function(){

            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'core/v0/tenants/:tenantId/modules/:moduleId/show', {}, {
                'get' : {
                    method : 'GET',
                    headers : {
                        Authorization : bearer
                    }
                }
            })
        };

        factory.subscribeModule = function (){
            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'core/v0/tenants/:tenantId/modules/subscribe', {tenantId : '@tenantId'}, {
                'subscribe' : {
                    method : 'POST',
                    headers : {
                        Authorization: bearer
                    }

                },
                'unsubscribe' : {
                    method: 'DELETE',
                    headers :{
                        Authorization: bearer
                    }
                }
            })
        };

        factory.unsubscribeModule = function (obj){
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            var deleteUrl = MOD_NBOS.API_URL + 'core/v0/tenants/' +obj.tenantId +'/modules/subscribe';

            //NOTE: this has to be a $http request since 'DELETE' in $resource does not accept a payload
            return $http({
                method : 'DELETE',
                url : deleteUrl,
                headers :{
                    Authorization: bearer
                },
                data : obj
            });
        };

        return factory;
    }]);