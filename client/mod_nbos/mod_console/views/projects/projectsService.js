'use strict';

angular.module('mod.console')

    .factory('ProjectService', ['ProjectFactory', '$q', function(ProjectFactory, $q){
        var factory = {};

        factory.getAllProjects = function(){
            var deferred = $q.defer();

            ProjectFactory.getAllProjects().get({}, function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };


        factory.createProject = function(project){
            var deferred = $q.defer();

            ProjectFactory.project().post(project).$promise.then(function(success){
                deferred.resolve(success)
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };


        factory.getProject = function(project){
            var deferred = $q.defer();

            ProjectFactory.project().get(project).$promise.then(function(success){
                deferred.resolve(success)
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };


        factory.getAppTypes = function(){
            var deferred = $q.defer();

            ProjectFactory.getAppTypes().get().$promise.then(function(success){
                deferred.resolve(success)
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };



        return factory;
    }])

    .factory('ProjectFactory', ['MOD_NBOS', 'SessionService', '$resource', function(MOD_NBOS, SessionService, $resource){
        var factory = {};

        factory.getAllProjects = function(){
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            return $resource(MOD_NBOS.API_URL + 'tenant/v0/tenants', {}, {
                'get': {
                    method : 'GET',
                    headers : {
                        "Authorization" : bearer
                    },
                    isArray : true
                }
            });
        };

        factory.project = function(){
            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'tenant/v0/tenants', {}, {
                'post' : {
                    method: 'POST',
                    headers : {
                        Authorization : bearer
                    }
                }
            })
        };

        factory.getProject = function(){
            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'tenant/v0/tenants', {}, {
                'get' : {
                    method : 'GET',
                    headers : {
                        Authorization : bearer
                    }
                }
            })
        };

        factory.getAppTypes = function(){
            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'core/v0/apps/list', {}, {
                'get' : {
                    method : 'GET',
                    headers : {
                        Authorization : bearer
                    },
                    isArray : true
                }
            })
        };


        return factory;
    }]);