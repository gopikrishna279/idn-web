angular.module('mod.console')

    .controller('ProjectsCtrl', ['$scope', '$state', '$mdDialog', 'ProjectService', function($scope, $state, $mdDialog, ProjectService) {

        $scope.projects = [];
        $scope.noProjects = false;

        $scope.logo = "mod_nbos/assets/images/project-logo.png";
        $scope.cover = "mod_nbos/assets/images/cover.jpg";


        $scope.showPrompt = function(ev) {

            //TODO enable WIZARD creation

            $mdDialog.show({
                controller: function($scope, $mdDialog, ProjectService, AlertService) {
                    $scope.projectObj = {
                        id: 0,
                        tenantId: '',
                        isPublic: true,
                        apiClientSecret : "API_CLIENT_SECRET"
                    }
                    $scope.create = function() {
                        ProjectService.createProject($scope.projectObj).then(function(response) {
                            $mdDialog.hide(response);
                        }, function(error) {
                            console.error(error);
                            AlertService.alert("Project Name already Exist", 'md-warn');
                        });
                    };

                    $scope.closeDialog = function() {
                        $mdDialog.cancel();
                    }
                },
                templateUrl: 'mod_nbos/mod_console/views/projects/ProjectCreate.html',
                parent: angular.element(document.body),
                targetEvent: ev
            })
                .then(function(project) {
                    if (project) {
                        init();
                    };
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        $scope.manageProject = function(projectId) {
            $state.go('con.manage', { id: projectId });
        };


        var init = function() {

            /*
             todo: uncomment this once app types are moved to production

             ProjectService.getAppTypes().then(function(success){
             console.log(success);
             }, function(error){
             console.log(error);
             });

             */



            ProjectService.getAllProjects().then(function(success) {
                console.log(success);
                $scope.projects = success;

                //Show No Projects Message
                if ($scope.projects.length == 0) {
                    $scope.noProjects = true;
                } else {
                    $scope.noProjects = false;
                };


            }, function(error) {
                console.error(error);

            });
        };

        init();
    }]);
