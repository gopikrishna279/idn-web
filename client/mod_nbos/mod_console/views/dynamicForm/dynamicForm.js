'use strict';

angular.module('mod.console')
    .directive('dynamicForm', function() {
        return {
            restrict: 'E',
            scope: {
                module: '=projectModule',
                savemodule: '&onSubmit'
            },
            templateUrl: 'mod_nbos/mod_console/views/dynamicForm/dynamicForm-template.html',
            link: function(scope, element, attr) {
                scope.noConfig = false;

                // var clone = function(obj) {
                //     var copy;
                //
                //     // Handle the 3 simple types, and null or undefined
                //     if (null == obj || "object" != typeof obj) return obj;
                //
                //     // Handle Date
                //     if (obj instanceof Date) {
                //         copy = new Date();
                //         copy.setTime(obj.getTime());
                //         return copy;
                //     }
                //
                //     // Handle Array
                //     if (obj instanceof Array) {
                //         copy = [];
                //         for (var i = 0, len = obj.length; i < len; i++) {
                //             copy[i] = clone(obj[i]);
                //         }
                //         return copy;
                //     }
                //
                //     // Handle Object
                //     if (obj instanceof Object) {
                //         copy = {};
                //         for (var attr in obj) {
                //             if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
                //         }
                //         return copy;
                //     }
                //
                //     throw new Error("Unable to copy obj! Its type isn't supported.");
                // };

                // scope.$watch('module.uuid', function(){
                //
                //     scope.moduleCopy = copy(scope.module);
                //
                //     if(scope.module && scope.module.configuration && scope.module.configuration.length < 1){
                //         scope.noConfig = true;
                //     } else {
                //         scope.noConfig = false;
                //
                //         if(scope.module && scope.module.configuration){
                //             for(var i=0; i< scope.module.configuration.length ; i++){
                //                 if(scope.module.configuration[i].values && scope.module.configuration[i].values.length > 0){
                //                     if(scope.module.configuration[i].possibleValues  && scope.module.configuration[i].possibleValues.length > 0){
                //                         var values = [];
                //                         var obj = {};
                //
                //                         scope.module.configuration[i].values.forEach(function(element, index, array){
                //                             var found = false;
                //                             for(var j=0; j < scope.module.configuration[i].possibleValues.length; j++){
                //                                 if(element == scope.module.configuration[i].possibleValues[j].value){
                //                                     found = true;
                //                                     obj = {};
                //                                     obj.name = scope.module.configuration[i].possibleValues[j].name;
                //                                     obj.value = scope.module.configuration[i].possibleValues[j].value;
                //                                     values.push(obj);
                //                                 };
                //                             };
                //                             if(!found){
                //                                 //the value present is not in any of the possible values, so remove it.
                //
                //                             }
                //                         });
                //                         scope.module.configuration[i].values = values;
                //                     };
                //                 }
                //
                //             };
                //          console.log("END MODULE IF");
                //         }; // END MODULE IF
                //     }
                // });
            },
            controller: ['$scope', 'AlertService', function($scope, AlertService) {

                $scope.$watch('module.uuid', function() {
                    $scope.moduleCopy = clone($scope.module);
                    console.log($scope.moduleCopy)

                    if ($scope.moduleCopy && $scope.moduleCopy.configuration && $scope.moduleCopy.configuration.length < 1) {
                        $scope.noConfig = true;
                    } else {
                        $scope.noConfig = false;

                        if ($scope.moduleCopy && $scope.moduleCopy.configuration) {
                            for (var i = 0; i < $scope.moduleCopy.configuration.length; i++) {
                                if ($scope.moduleCopy.configuration[i].values && $scope.moduleCopy.configuration[i].values.length > 0) {
                                    if ($scope.moduleCopy.configuration[i].possibleValues && $scope.moduleCopy.configuration[i].possibleValues.length > 0) {
                                        var values = [];
                                        var obj = {};

                                        $scope.module.configuration[i].values.forEach(function(element, index, array) {
                                            var found = false;
                                            for (var j = 0; j < $scope.moduleCopy.configuration[i].possibleValues.length; j++) {
                                                if (element == $scope.moduleCopy.configuration[i].possibleValues[j].value) {
                                                    found = true;
                                                    obj = {};
                                                    obj.name = $scope.moduleCopy.configuration[i].possibleValues[j].name;
                                                    obj.value = $scope.moduleCopy.configuration[i].possibleValues[j].value;
                                                    values.push(obj);
                                                };
                                            };
                                            if (!found) {
                                                //the value present is not in any of the possible values, so remove it.

                                            }
                                        });
                                        $scope.moduleCopy.configuration[i].values = values;
                                    };
                                }

                            };
                            console.log("END MODULE IF");
                        }; // END MODULE IF
                    }
                });

                $scope.transformChip = function(chip) {
                    // If it is an object, it's already a known chip
                    if (angular.isObject(chip)) {
                        return chip;
                    }
                    // Otherwise, create a new one
                    return { name: chip, type: 'new' }
                };


                $scope.selectedItemChange = function(item, index) {
                    //console.log("in onMultipleItemChange");
                };

                $scope.onSubmit = function() {
                    var emptyValue = false;
                    var missingConfig = {};
                    var moduleClone = clone($scope.moduleCopy);

                    for (var i = 0; i < moduleClone.configuration.length; i++) {
                        if (moduleClone.configuration[i].values == undefined || moduleClone.configuration[i].values == null) {
                            missingConfig = moduleClone.configuration[i];
                            emptyValue = true;
                            break;
                        } else if (moduleClone.configuration[i].possibleValues && moduleClone.configuration[i].possibleValues.length > 0) {

                            if (moduleClone.configuration[i].multiValued) {
                                var values = [];
                                moduleClone.configuration[i].values.forEach(function(element) {
                                    values.push(element.value);
                                });
                                moduleClone.configuration[i].values = values;

                            } else {
                                moduleClone.configuration[i].values = moduleClone.configuration[i].values.value;
                            };
                        };
                    };

                    if (emptyValue) {
                        AlertService.alert("Missing configuration for " + missingConfig.name);
                    } else {
                        console.log("Send to save");
                        console.log(moduleClone);
                        $scope.savemodule({ module: moduleClone });
                    }

                };

                var clone = function(obj) {
                    var copy;

                    // Handle the 3 simple types, and null or undefined
                    if (null == obj || "object" != typeof obj) return obj;

                    // Handle Date
                    if (obj instanceof Date) {
                        copy = new Date();
                        copy.setTime(obj.getTime());
                        return copy;
                    }

                    // Handle Array
                    if (obj instanceof Array) {
                        copy = [];
                        for (var i = 0, len = obj.length; i < len; i++) {
                            copy[i] = clone(obj[i]);
                        }
                        return copy;
                    }

                    // Handle Object
                    if (obj instanceof Object) {
                        copy = {};
                        for (var attr in obj) {
                            if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
                        }
                        return copy;
                    }

                    throw new Error("Unable to copy obj! Its type isn't supported.");
                };

                $scope.resetFields = function() {

                };

                $scope.showModule = function() {
                    console.log($scope.module.configuration.length);
                };
            }]
        }
    });
