angular.module('mod.console')

    .controller('ManageModuleViewCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'LayoutService', function($scope, $state, $stateParams, ManageService, LayoutService){

        $scope.project = {};
        $scope.subscribedModules = {};
        $scope.isMarketPlace = true;

        var init = function(){
            if(!ManageService.project){
                console.log("no project");
                $scope.$parent.$parent.manageNext = $state.current.name;
                $scope.$parent.$parent.manageParams = {};
                $scope.$parent.$parent.manageParams.id  = $stateParams.id;
                $scope.$parent.$parent.manageParams.moduleId  = $stateParams.moduleId;
                $state.go('con.manage');
            } else {
                $scope.project = ManageService.project;
                $scope.projectAbout = ManageService.projectAbout;
            }
        };



        init();

    }]);