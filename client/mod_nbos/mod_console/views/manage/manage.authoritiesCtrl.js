angular.module('mod.console')

    .controller('ManageAuthoritiesCtrl', ['$scope', 'ManageService', 'AlertService', '$state', '$mdMedia', '$mdDialog', function($scope, ManageService, AlertService, $state, $mdMedia, $mdDialog){

        $scope.project = {};
        $scope.projectAbout = {};

        $scope.startDownload = function(){

        };

        $scope.authorities = [];

        $scope.newAuthority = function(ev){
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'mod_nbos/mod_console/views/manage/manage.authorities.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: false,
                fullscreen: useFullScreen,
                locals : {
                    role : ""
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    //do nothing
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        function DialogController($scope, role, $mdDialog) {
            $scope.role = role;

            $scope.hide = function() {
                $mdDialog.cancelx();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }


        var init = function(){
            if(!ManageService.projectAbout){
                $scope.$parent.$parent.manageNext = $state.current.name;
                console.log($scope.manageNext);
                $state.go('con.manage');
            } else {

                $scope.projectAbout = ManageService.projectAbout;
                $scope.project = ManageService.project;
                $scope.projectAbout.tenantId = $scope.project.tenantId;
            }
        };

        init();

    }]);