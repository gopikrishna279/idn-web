angular.module('mod.console')
    .controller('ManageClientTokensCtrl', ['$scope', '$stateParams', 'ManageService', '$state', function($scope, $stateParams, ManageService, $state) {

        var tenantId = $stateParams.id;
        var clientId = $stateParams.clientId;
        $scope.tokens = [];

        var getClientTokens = function() {
            ManageService.getClientTokens(tenantId, clientId).then(function(response) {
            	$scope.tokens = response;
            }, function(error) {
                console.log("error");
            })
        }
        var init = function() {
            if (!ManageService.projectClients) {
                $scope.$parent.$parent.manageNext = $state.current.name;
                $scope.projectClients = ManageService.projectClients;
                $state.go('con.manage');
            } else {
                if (clientId) {
                    $scope.project = ManageService.project;
                    $scope.projectClients = ManageService.projectClients;
                    getClientTokens();
                }else{
                	$state.go('con.manage.clients', {
                		"id": tenantId
                	})
                }
            };
        };
        init();
    }]);
