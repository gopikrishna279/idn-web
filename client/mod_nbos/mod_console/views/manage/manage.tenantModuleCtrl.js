angular.module('mod.console')
    .controller('ManageTenantModuleCtrl', ['$scope', 'ManageService', 'Module_RoleAuthService', 'AlertService', '$state', '$stateParams', '$mdMedia', '$mdDialog', 'StarterCodeService', '$window', function($scope, ManageService, Module_RoleAuthService, AlertService, $state, $stateParams, $mdMedia, $mdDialog, StarterCodeService, $window) {

        $scope.project = {};
        $scope.projectAbout = {};
        $scope.tenantId = '';
        $scope.moduleId = '';
        $scope.module;
        $scope.moduleActive = false;
        $scope.selectedIndex = 0;
        $scope.manageRoleObj = {};
        $scope.manageRoleObj_uRoleName = "";

        $scope.updateSecret = function(ev) {
            var confirm = $mdDialog.prompt()
                .title('Set Client Secret')
                .textContent('Please enter a text of your choosing.')
                .placeholder('API Secret')
                .ariaLabel('API Secret')
                .clickOutsideToClose(true)
                .targetEvent(ev)
                .ok('Save')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function(result) {
                var secretObj = {
                    tenantId: $stateParams.id,
                    clientId: $scope.module.apiClients[0].clientId,
                    clientSecret: result
                };
                console.log(secretObj);
                Module_RoleAuthService.updateModuleSecret(secretObj).then(function(success) {
                    AlertService.alert("Hurray! Secret updated successfully.", "md-accent");
                }, function(error) {

                });
            }, function() {
                //do nothing
            });
        };

        $scope.activateModule = function(val) {
            var obj = {};
            obj.moduleUuid = Module_RoleAuthService.module.uuid;
            obj.tenantId = $stateParams.id;

            if ($scope.moduleActive || val == "activate") {
                //activate module
                console.log("proceeding to activate module");
                Module_RoleAuthService.activateTenantModule(obj).then(function(success) {
                    $scope.moduleActive = true;
                    ManageService.getSubscribedModules($stateParams.id);
                }, function(error) {
                    AlertService.alert("Oops! Something went wrong. Please refresh the page and try again.")
                })
            } else {
                //deactivate module
                console.log("proceeding to DEactivate module");
                Module_RoleAuthService.deactivateTenantModule(obj).then(function(success) {
                    $scope.moduleActive = false;
                    ManageService.getSubscribedModules($stateParams.id);
                }, function(error) {
                    AlertService.alert("Oops! Something went wrong. Please refresh the page and try again.")
                })
            };
        };

        $scope.getStarterCode = function(ev, lang) {
            var obj = {};
            obj.moduleId = $scope.moduleId;
            obj.lang = lang;
            obj.tenantId = $scope.tenantId;
            StarterCodeService.get_starter_api_code(obj).then(function(success) {

                if (success.downloadUrl) {
                    $window.location.assign("http://" + success.downloadUrl);
                } else {
                    AlertService.alert("Something Went Wrong in downloading" + lang + "starter code", "md-warn");
                }
            }, function(error) {
                AlertService.alert("Something Went Wrong", "md-warn");
            });
        };


        // function updateModuleWithAuthority(success) {
        //     var found = false;
        //
        //     for(var i = 0; i < $scope.module.authorities.length; i++){
        //         if($scope.module.authorities[i].uAuthorityName == success.uAuthorityName){
        //             $scope.module.authorities[i] = success;
        //             found = true;
        //             console.log($scope.module.authorities);
        //         }
        //     };
        //
        //     _.each($scope.module.authorities, function (authority) {
        //         if (authority.uAuthorityName == success.uAuthorityName) {
        //             authority = success;
        //             found = true;
        //         };
        //     });
        //
        //     if (!found) {
        //         $scope.module.authorities.push(success);
        //
        //         //Add this authority to the admin role of the module automatically
        //     };
        // }

        var warnAboutActive = function() {
            AlertService.alert("Module is active. Please deactivate module in 'Info' to make changes.", "md-warn");
        };

        $scope.backToProjects = function(){
            $state.go('con.manage.dash', {
                id : $scope.project.tenantId
            })
        }

        $scope.newAuthority = function(ev, authority) {

            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'mod_nbos/mod_console/views/manage/manage.authorities.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen,
                    locals: {
                        authority: _.clone(authority),
                        module: $scope.module
                    }
                })
                .then(function(success) {
                    AlertService.alert("Hurray! Authority created successfully.", "md-primary");

                    $scope.getModuleForModuleId();

                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        function DialogController($scope, authority, $mdDialog, module) {
            $scope.authority = {};
            $scope.showProgress = false;
            var editMode = false;

            if (authority) {
                $scope.authority = authority;
                editMode = true;
                var uAuthorityNameArr = authority.uAuthorityName.split('.');
                $scope.authority.authorityName_category = uAuthorityNameArr[uAuthorityNameArr.length - 2];
                $scope.authority.authorityName_subCategory = uAuthorityNameArr[uAuthorityNameArr.length - 1];
            };
            $scope.authority.authorityName_prefix = "authority:" + module.name;

            $scope.hide = function() {
                $mdDialog.cancel();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $scope.showProgress = true;
                $scope.authority.uAuthorityName = $scope.authority.authorityName_prefix + "." + $scope.authority.authorityName_category + "." + $scope.authority.authorityName_subCategory;
                $scope.authority.moduleUuid = module.uuid;
                $scope.authority.moduleUuid = module.uuid;
                console.log($scope.authority);
                if (editMode) {
                    Module_RoleAuthService.updateAuthorityForModule($scope.authority).then(function(success) {
                        $scope.showProgress = false;
                        $mdDialog.hide(success);
                    }, function(error) {
                        $scope.showProgress = false;
                        AlertService.alert("Oops! Error saving authority. Please refresh and try again.", "md-warn")
                    });
                } else {
                    Module_RoleAuthService.createAuthorityForModule($scope.authority).then(function(success) {
                        $scope.showProgress = false;
                        $mdDialog.hide(success);
                    }, function(error) {
                        $scope.showProgress = false;
                        AlertService.alert("Oops! Error saving authority. Please refresh and try again.", "md-warn")
                    });
                };


            };
        };


        $scope.confirmAuthorityDelete = function(ev) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Delete Authority?')
                .textContent('FUNCTIONALITY CURRENTLY UNAVAILABLE?')
                // .textContent('Are you sure you want to delete this authority?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .clickOutsideToClose(true)
                .ok('Yes!')
                .cancel('No.');
            $mdDialog.show(confirm).then(function() {
                $scope.status = 'You decided to get rid of your debt.';
            }, function() {
                $scope.status = 'You decided to keep your debt.';
            });
        };

        $scope.manageRole = function(role) {

            if (role) {
                Module_RoleAuthService.role_edit = _.clone(role);
                $scope.manageRoleObj_uRoleName = role.uRoleName;
            } else {
                $scope.manageRoleObj = {};
                $scope.manageRoleObj_uRoleName = "";
            };

            $scope.selectedIndex = 4;

        };

        $scope.backToList = function() {
            $scope.selectedIndex = 3;
        };

        function RoleController($scope) {

        };

        $scope.manageRoleDialogue = function(ev, role) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
            $mdDialog.show({
                    controller: RoleController,
                    templateUrl: 'mod_nbos/mod_console/views/manage/manage.authorities.template.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    fullscreen: useFullScreen,
                    locals: {
                        role: role,
                        module: $scope.module
                    }
                })
                .then(function(success) {
                    //here update $scope.module

                    $scope.getModuleForModuleId();

                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });

        };


        $scope.getModuleForModuleId = function() {
            //get module
            Module_RoleAuthService.getModule($scope.tenantId, $scope.moduleId).then(function(success) {
                console.log("GOT MODULE");
                $scope.module = success;
            }, function(error) {
                AlertService.alert("Oops! Error getting module. Please go back and try again.");
            });

            //check if module is subscribed in $scope.subscribedModules
            _.each(ManageService.subscribedModules, function(module) {
                if (module.uuid == $scope.moduleId) {
                    console.log("THE MODULE IS ACTIVE");
                    $scope.moduleActive = Module_RoleAuthService.moduleActive = true;
                };
            });
        };

        var init = function() {
            if (!ManageService.projectAbout) {
                $scope.$parent.$parent.manageNext = $state.current.name;
                $scope.$parent.$parent.manageParams = {};
                $scope.$parent.$parent.manageParams.id = $stateParams.id;
                $scope.$parent.$parent.manageParams.moduleId = $stateParams.moduleId;
                $state.go('con.manage');
            } else if (!$stateParams.moduleId) {
                $state.go('con.manage');
            } else {
                $scope.projectAbout = ManageService.projectAbout;
                $scope.project = ManageService.project;
                $scope.projectAbout.tenantId = $scope.project.tenantId;
                $scope.tenantId = $stateParams.id;
                $scope.moduleId = $stateParams.moduleId;
                $scope.getModuleForModuleId();
            }
        };

        init();

    }]);
