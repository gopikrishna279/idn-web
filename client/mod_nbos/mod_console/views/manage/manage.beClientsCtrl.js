'use strict';

/**
 * NOTE: The Client with the name "API Client" is created when the tenant is created.
 */


angular.module('mod.console')

.controller('ManageBeClientsCtrl', ['$scope', '$state', '$stateParams', 'ManageService', '$mdDialog', 'AlertService', function($scope, $state, $stateParams, ManageService, $mdDialog, AlertService) {


    var tenantId = $stateParams.id;
    $scope.beClient = {};

    $scope.updateSecret = function(ev) {

        var confirm = $mdDialog.prompt()
            .title('Please enter a new password.')
            .textContent('Use this password in your API\'s')
            .placeholder('password')
            .ariaLabel('password')
            .targetEvent(ev)
            .ok('Change')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function(result) {
            if (result != "") {
                var obj = {
                    "clientSecret": result
                }
                ManageService.updateSecret(tenantId, $scope.beClients.clientId, obj).then(function(response) {
                    AlertService.alert("The new secret was set successfully.", 'md-primary');
                }, function(error) {
                    AlertService.alert("Error Updating Secret. Please try again", 'md-warn');
                })
            }
        }, function() {
            //do nothing
        });
    };

    var getBeClients = function() {
        $scope.feClients = [];
        for (var i = 0; i < $scope.projectClients.length; i++) {
            if ($scope.projectClients[i].clientName == "API Client") {
                $scope.beClients = $scope.projectClients[i];
                break;
            }
        };
    };

    var init = function() {
        if (!ManageService.projectClients) {
            $scope.$parent.$parent.manageNext = $state.current.name;
            $scope.projectClients = ManageService.projectClients;
            $state.go('con.manage');
        } else {

            $scope.project = ManageService.project;
            $scope.projectAbout = ManageService.projectAbout;
            $scope.projectClients = ManageService.projectClients;
            getBeClients();
        }
    };

    init();





}]);
