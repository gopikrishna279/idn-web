angular.module('mod.console')

    .controller('ManageRolesCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', 'APP_CONFIG', '$mdMedia', '$mdDialog', function($scope, $state, $stateParams, ManageService, AlertService, APP_CONFIG, $mdMedia, $mdDialog){

        $scope.roles = [];

        var tenantId = $stateParams.id;
        console.log(tenantId);

        var coreRoles = ["role:app.tenant.admin", "role:app.tenant.guest", "role:app.tenant.user"];

        var coreAuthorities = [
            "authority:core.module.admin",
            "authority:identity.user.admin",
            "authority:oauth.client.admin",
            "authority:about.page.admin",
            "authority:oauth.token.admin"
        ];


        $scope.editRole = function(ev, role){
            ManageService.role_edit = role;
            $state.go("con.manage.rolesCreate", {roleId : role.uRoleName});
        };

        $scope.deleteRole = function(ev, role){
            if(coreRoles.indexOf(role.uRoleName) < 0){
                var confirm = $mdDialog.confirm()
                    .title('Delete Role?')
                    .textContent('All Users with this role will be assign the default user role.')
                    .ariaLabel('Delete Role')
                    .targetEvent(ev)
                    .ok('Yes!')
                    .cancel('No.');
                $mdDialog.show(confirm).then(function() {

                }, function() {
                    //do nothing
                });
            } else {
                AlertService.alert("This is a core role and cannot be deleted.", "md-warn")
            }
        };

        $scope.showPrompt = function(ev) {
            // Appending dialog to document.body to cover sidenav in docs app

            $state.go("con.manage.rolesCreate", {roleId : ""});

            /*
            var confirm = $mdDialog.prompt()
                .title('Create a new Role')
                .textContent('Please enter a name for your Role.')
                .placeholder('Role Name')
                .ariaLabel('Role name')
                .targetEvent(ev)
                .ok('Create')
                .cancel('cancel');
            $mdDialog.show(confirm).then(function(result) {

                var uRoleName = result.replace(/ /g,'');
                uRoleName = uRoleName.toLowerCase();

                var role = {
                    tenantId : tenantId,
                    description : "asdf",
                    displayName : result,
                    uRoleName : uRoleName,
                    authorities : []
                };
                ManageService.createRole(role).then(function(success){
                    console.log(success);
                }, function(error){
                    console.log("ERROR CREATING ROLE")
                });
                console.log(result);
            }, function() {
                $scope.status = 'You didn\'t name your dog.';
            });
            */
        };

        $scope.newRole = function(ev){
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                controller: DialogController,
                templateUrl: 'client/mod_nbos/mod_console/views/manage/manage.roles.create.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen,
                locals : {
                    role : ""
                }
            })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        function DialogController($scope, role, $mdDialog) {
            $scope.role = role;

            $scope.hide = function() {
                $mdDialog.cancelx();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }



        var init = function(){
            if(!ManageService.projectAbout){
                $scope.$parent.$parent.manageNext = $state.current.name;
                $state.go('con.manage');
            } else {
                $scope.project = ManageService.project;
                $scope.projectAbout = ManageService.projectAbout;
                //NOTE: Call for roles everytime. TO cater to the absense of get role by id for create and edit roles screen.
                ManageService.getRoles(tenantId).then(function(success){
                    console.log(success);
                    console.log(ManageService.subscribedModules);
                    $scope.roles = success;
                }, function(error){
                    AlertService.alert("Oops! Something went wrong.", "md-warn");
                });

            }
        };

        init();

    }]);