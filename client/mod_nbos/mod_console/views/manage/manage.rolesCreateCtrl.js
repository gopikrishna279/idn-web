angular.module('mod.console')

    .controller('ManageRolesCreateCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', 'APP_CONFIG', '$mdMedia', '$mdDialog', function($scope, $state, $stateParams, ManageService, AlertService, APP_CONFIG, $mdMedia, $mdDialog){

        $scope.roleObj = {};
        $scope.roleObj.roleName = '';
        $scope.isEdit = false;
        $scope.isDirty = false;

        var tenantId = $stateParams.id;
        console.log(tenantId);

        $scope.coreAuthorities = [
            "authority:core.module.admin",
            "authority:identity.user.admin",
            "authority:oauth.client.admin",
            "authority:about.page.admin",
            "authority:oauth.token.admin"
        ];

        $scope.project = {
            description: 'Nuclear Missile Defense System',
            rate: 500
        };

        $scope.selectedAuthorities = [];

        // $scope.$watch('roleObj.roleName', function() {
        //     $scope.roleObj.roleName = $scope.roleObj.roleName.replace(/\s+/g,'');
        // });

        $scope.createRole = function(){

            $scope.roleObj.tenantId = tenantId;
            console.log($scope.roleObj);
            ManageService.createRole($scope.roleObj).then(function(success){
                console.log(success);
                ManageService.getRoles(tenantId);
                $scope.isEdit = true;
            }, function(error){
                AlertService.alert("There was an error creating role.", "md-warn");
            });

        };

        $scope.editRole = function(){

            $scope.roleObj.tenantId = tenantId;
            console.log($scope.roleObj);
            ManageService.updateRole($scope.roleObj).then(function(success){
                console.log(success);
                ManageService.getRoles(tenantId);
                $scope.isEdit = true;
            }, function(error){
                AlertService.alert("There was an error editing role.", "md-warn");
            });

        };

        var createAuthoritiesList = function(){
            console.log($scope.roleObj);
            $scope.allModules = _.clone(ManageService.subscribedModules);
            //TODO - should we load authorities of ALL modules?


            _.each($scope.allModules, function(module){
                _.each(module.authorities, function(authority){

                    var found = false;
                    _.each($scope.roleObj.authorities, function(roleAuthority){
                        if(roleAuthority.uAuthorityName == authority.uAuthorityName){
                            found = true;
                        };
                    });
                    if(found){
                        authority.selected = true;
                    } else {
                        authority.selected = false;
                    };

                });
            });

        };

        $scope.addRemoveAuthority = function(ev, authority){

            var authorityObj = {
                tenantId : tenantId,
                roleId : $scope.roleObj.uRoleName,
                authorities : [
                    {
                        uAuthorityName : authority.uAuthorityName
                    }
                ]
            };

            console.log(authorityObj);

            if(authority.selected){

                ManageService.addAuthorityForRole(authorityObj).then(function(success){
                    console.log(success);
                }, function(error){
                    AlertService.alert("Error!", "md-warn")
                });
            } else {
                ManageService.deleteAuthorityForRole(authorityObj).then(function(success){
                    console.log(success);
                }, function(error){
                    AlertService.alert("Error!", "md-warn")
                });
            };


        };

        var init = function(){

            if(ManageService.roles){
                $scope.project = ManageService.project;
                $scope.projectAbout = ManageService.projectAbout;

                if($stateParams.roleId){
                    $scope.isEdit = true;
                    $scope.roleObj = _.clone(ManageService.role_edit);
                    var uRoleNameArr = $scope.roleObj.uRoleName.split('.');
                    $scope.roleObj.roleName = uRoleNameArr[uRoleNameArr.length - 1]; //The last array value

                    ManageService.role_edit = null;
                } else {
                    $scope.isEdit = false;
                };

                createAuthoritiesList();
            } else {
                $state.go("con.manage.roles");
            };
        };

        init();

    }]);