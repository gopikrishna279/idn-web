angular.module('mod.console')

    .controller('ManageLayoutCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'TenantLayoutService', 'AlertService', '$mdDialog', '$mdMedia', 'APP_CONFIG', function($scope, $state, $stateParams, ManageService, TenantLayoutService, AlertService, $mdDialog, $mdMedia, APP_CONFIG){

        $scope.project = {};
        $scope.projectLayout = {};
        $scope.isEdit = false;

        $scope.selectedIndex = 0;
        $scope.selectedLayout = {};

        var layout_empty = {};
        var layout_default = {};

        $scope.data = {
            includeNbosLayout : null
        };

        $scope.change = function(){
            $scope.data.includeNbosLayout = !$scope.data.includeNbosLayout;
        };

        $scope.onChange = function() {
            console.log("DATA CHANGED" +$scope.includeNbosLayout);
            //change config for "app"
            //change layout to no-layout

            for (var i= 0 ; i<$scope.appConfig.configuration.length; i++){
                if($scope.appConfig.configuration[i].name == "layout.modular"){
                    $scope.appConfig.configuration[i].values = $scope.includeNbosLayout;
                    break;
                };
            };

            saveConfigForModule($scope.appConfig);

            if($scope.includeNbosLayout){
                $scope.saveLayout(layout_default);
            } else {
                $scope.saveLayout(layout_empty);
            };

        };


        $scope.showEdit = function(){
            $scope.selectedIndex = 1;
        };

        $scope.showPreview = function(ev, layout) {
            var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
            $mdDialog.show({
                controller: function($scope, layout, $mdDialog){

                    console.log(layout);
                    $scope.layout = layout;

                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                    $scope.answer = function() {
                        $mdDialog.hide($scope.layout.id);
                    };

                },
                templateUrl: 'mod_nbos/mod_console/views/manage/manage.layout.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: useFullScreen,
                locals : {
                    layout : layout
                }
            })
                .then(function(layoutId) {

                    $scope.saveLayout(layout);

                }, function() {
                    //do nothing
                });

            $scope.$watch(function() {
                return $mdMedia('xs') || $mdMedia('sm');
            }, function(wantsFullScreen) {
                $scope.customFullscreen = (wantsFullScreen === true);
            });
        };

        $scope.selectLayout = function(item){
            $scope.isEdit = true;
            $scope.projectLayout = item;
        };

        $scope.cancel = function(){
            $scope.isEdit = false;
        };


        $scope.showConfirm = function(ev, layout) {
            // Appending dialog to document.body to cover sidenav in docs app
            var confirm = $mdDialog.confirm()
                .title('Would you like to use this layout?')
                .ariaLabel('Lucky day')
                .targetEvent(ev)
                .ok('Yes!')
                .cancel('No.');
            $mdDialog.show(confirm).then(function() {
                $scope.saveLayout(layout);
            }, function() {
                //do nothing
            });
        };


        $scope.saveLayout = function(layout){
            ManageService.setLayout($stateParams.id, layout.id).then(function(success){
                $scope.isEdit = false;
                AlertService.alert('Layout Saved Successfully.', 'md-success');
                ManageService.projectLayout = success;
                $scope.projectLayout = success;
                console.log(success);
            }, function(error){
                console.error(error);
            });
        };


        var saveConfigForModule = function(module) {

            module.tenantId = $stateParams.id;
            ManageService.saveConfig(module).then(function(success) {
                $scope.moduleConfig = module;

            }, function(error) {
                AlertService.alert("Error saving configuration. Please refresh the page and try again.", "md-warn");
            });

        };


        var init = function(){
            if (!ManageService.projectClients) {
                $scope.$parent.$parent.manageNext = $state.current.name;
                $scope.projectClients = ManageService.projectClients;
                $state.go('con.manage');
            } else {

                $scope.project = ManageService.project;
                $scope.projectAbout = ManageService.projectAbout;
                $scope.projectLayout = ManageService.projectLayout;

                TenantLayoutService.getAllLayouts().then(function(success){
                    $scope.layouts = [];

                    for(var i = 0; i < success.length; i++){
                        if(success[i].name == "layout-empty"){
                            layout_empty = success[i];
                        } else if(success[i].name == "layout-default"){
                            layout_default = success[i];
                            $scope.layouts.push(success[i]);
                        } else {
                            $scope.layouts.push(success[i]);
                        };
                    };

                }, function(error){
                    AlertService.alert("Error loading Layouts. Please refresh the page.");
                });

                _.find(ManageService.projectConfig, function(module) {
                    if (module.uuid == 'MOD:app') {
                        $scope.appConfig = module;

                        for (var i= 0 ; i<$scope.appConfig.configuration.length; i++){
                            if($scope.appConfig.configuration[i].name == "layout.modular"){
                                 if($scope.appConfig.configuration[i].values == "true"){
                                     $scope.data.includeNbosLayout = true;
                                 } else {
                                     $scope.data.includeNbosLayout = false
                                 }
                                console.log($scope.data.includeNbosLayout);
                                break;
                            };
                        };

                        console.log(APP_CONFIG);
                    }
                });

            };
        };

        init();



    // Dummy data
        $scope.imagePath = 'mod_nbos/assets/images/layout.png';

    }]);