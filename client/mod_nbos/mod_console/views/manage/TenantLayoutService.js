'use strict';

angular.module('mod.nbos')

    .service('TenantLayoutService',  ['TenantLayoutFactory', '$q', function(TenantLayoutFactory, $q){
        var factory = {};

        factory.allLayouts;

        factory.getAllLayouts = function(tenantId){
            var deferred = $q.defer();
            TenantLayoutFactory.allLayouts().get({tenantId:tenantId}, function(success){
                factory.allLayouts = success;
                console.log(success);
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return factory;

    }])

    .factory('TenantLayoutFactory', ['MOD_NBOS', 'SessionService', '$resource', function(MOD_NBOS, SessionService, $resource){
        var factory = {};


        factory.allLayouts = function(){

            var bearer = "Bearer " + SessionService.getStoredUserToken();

            return $resource(MOD_NBOS.API_URL + 'core/v0/layouts', {}, {
                'get' : {
                    method : 'GET',
                    headers : {
                        Authorization : bearer
                    },
                    isArray : true
                },
                'post' : {
                    method : 'POST',
                    headers : {
                        Authorization : bearer
                    }
                }
            })
        };

        return factory;
    }])