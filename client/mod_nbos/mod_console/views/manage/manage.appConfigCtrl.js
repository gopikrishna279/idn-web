angular.module('mod.console')

.controller('ManageAppConfigCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', function($scope, $state, $stateParams, ManageService, AlertService) {


    // $stateParams.id
    $scope.projectConfig = [];
    $scope.moduleConfig = {};
    $scope.showPosition = true;
    $scope.models = {
        selected: null
    }
    $scope.isNoDrag = true;


    $scope.goToConfig = function(index) {
        if (index === -1) {
            $scope.showPosition = true;
        } else {
            $scope.showPosition = false;
            $scope.moduleConfig = $scope.projectConfig[index];
        }
    };

    $scope.showDrag = function() {
        $scope.isNoDrag = false;
    };

    $scope.savePositions = function() {
        $scope.isNoDrag = true;
    };

    $scope.selectedItemChange = function() {

    };

    $scope.saveConfigForModule = function(module) {

        module.tenantId = $stateParams.id;


        ManageService.saveConfig(module).then(function(success) {
            $scope.moduleConfig = module;
            if ($scope.module.configureEndPoint != "") {
                var obj = {
                    "config": $scope.moduleConfig.configuration,
                    "tenantId": $stateParams.id
                }
                ManageService.notifyModule($scope.module.configureEndPoint, obj).then(function(response) {
                    AlertService.alert("Configuration successfully saved for " + module.name, "md-primary");
                }, function(error) {
                    console.log("error notifying module");
                })
            } else {
                AlertService.alert("Configuration successfully saved for " + module.name, "md-primary");
            }
        }, function(error) {
            AlertService.alert("Error in saving configuration for " + module.name, "md-warn");
        });



    };

    var showOnlyModuleConf = function(modulesArr) {
        var coreArr = ["app", "core", "oauth", "identity"];

        for (var i = 0; i < modulesArr.length; i++) {
            if (coreArr.indexOf(modulesArr[i].name) < 0) {
                $scope.projectConfig.push(modulesArr[i]);
            }
        };
    };

    var init = function() {
        if (!ManageService.projectConfig) {
            $scope.$parent.$parent.manageNext = $state.current.name;
            $state.go('con.manage');
        } else {

            $scope.project = ManageService.project;
            $scope.projectAbout = ManageService.projectAbout;
            showOnlyModuleConf(ManageService.projectConfig);
        };
    };

    init();


}]);
