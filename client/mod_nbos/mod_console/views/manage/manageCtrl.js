angular.module('mod.console')

.controller('ManageCtrl', ['$scope', '$state', '$stateParams', 'ManageService', '$q', 'AlertService', '$rootScope', function($scope, $state, $stateParams, ManageService, $q, AlertService, $rootScope) {

    // console.log($stateParams);
    // $scope.manageService = new ManageService();

    $scope.showProgress = true;
    $scope.manageNext = null;
    $scope.manageParams = null;
    $rootScope.determinateValue = "";
    $rootScope.isDownload = false;

    // $scope.$watch('manageParams', function(){
    //     console.log("MANAGEPARAMS CHANGED");
    //     console.log($scope.manageParams);
    // }, true);

    var calculateStatus = function() {
        //$scope.vales = 
        var myObj = {};
        $scope.calculateObj = {
            "name": "",
            "description": "",
            "bannerImage": "",
            "logoImage": "",
            "userName": "",
            "phoneNumber": "",
            "companyUrl": "",
            "companyEmail": "",
            "clients": "",
            "layout": ""
        }
        var myArr = [];
        if (ManageService.projectAbout && ManageService.projectAbout.name) {
            $scope.calculateObj = {
                "name": ManageService.projectAbout.name,
                "description": ManageService.projectAbout.description,
                "bannerImage": ManageService.projectAbout.bannerImage,
                "logoImage": ManageService.projectAbout.logoImage,
                "userName": ManageService.projectAbout.contact.category,
                "phoneNumber": ManageService.projectAbout.contact.phoneNumber,
                "companyUrl": ManageService.projectAbout.contact.companyUrl,
                "companyEmail": ManageService.projectAbout.contact.companyEmail
            }
        }
        // if ($scope.subscribedModules.length) {
        //     $scope.calculateObj.modules = $scope.subscribedModules.length
        // } else {
        //     $scope.calculateObj.modules = "";
        // };
        if (ManageService.projectClients.length) {
            $scope.calculateObj.clients = ManageService.projectClients.length;
        } else {
            $scope.calculateObj.clients = "";
        }
        $scope.calculateObj.layout = ManageService.projectLayout;
        for (var key in $scope.calculateObj) {
            if ($scope.calculateObj[key]) {
                myArr.push($scope.calculateObj[key]);
            }
        };


        $rootScope.determinateValue = Math.round((myArr.length / 10) * 100);
        if ($rootScope.determinateValue == 100) {
            $rootScope.isDownload = true;
        }
    };



    var init = function() {
        var id = $stateParams.id;

        if (id) {

            var project = ManageService.getProject(id),
                about = ManageService.getAbout(id),
                layout = ManageService.getLayout(id),
                clients = ManageService.getClients(id),
                modules = ManageService.getSubscribedModules(id),
                tenantModules = ManageService.getTenantModules(id),
                config = ManageService.getAllConfig(id),
                tokens = ManageService.getAllTokens(id),
                theme = ManageService.getTheme(id);


            $q.all([project, about, layout, clients, modules, tenantModules, config, tokens, theme])
                .then(function(success) {
                    $scope.showProgress = false;
                    calculateStatus();
                    if ($scope.manageNext) {
                        $state.go($scope.manageNext, $scope.manageParams);
                        $scope.manageNext = null;
                        $scope.manageParams = null;
                    } else {
                        $state.go('con.manage.dash', { id: $stateParams.id });
                    };
                }, function(error) {
                    console.error(error);
                    AlertService.alert("Error retrieving complete project info.", "md-warn");
                    $scope.showProgress = false;
                    $state.go('con.projects');
                });

        } else {
            $state.go('con.projects');
        }



    };

    init();

}]);
