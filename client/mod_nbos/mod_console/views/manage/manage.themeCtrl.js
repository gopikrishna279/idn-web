angular.module('mod.console')

.controller('ManageThemeCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', '$mdSidenav', '$timeout', function($scope, $state, $stateParams, ManageService, AlertService, $mdSidenav, $timeout) {



    $scope.tinyColorFunction = function() {
        var color = tinycolor("#ff0000");
        console.log(color);

    };

    $scope.project = {};
    $scope.projectAbout = {};
    $scope.isEdit = false;
    $scope.generatePalette = false;
    $scope.themeObj = {};

    $scope.mainTheme = {
        mainTheme_isDark: false,
        primaryColor: "",
        primaryColorText_isDark: false,
        accentColor: "",
        accentColorText_isDark: false,
        warnColor: "",
        warnColorText_isDark: false
    };


    $scope.alternateTheme = {
        alternateTheme_isDark: false,
        primaryColor: "",
        primaryColorText_isDark: false,
        accentColor: "",
        accentColorText_isDark: false,
        warnColor: "",
        warnColorText_isDark: false
    };

    $scope.background = {};

    $scope.selectedIndex = 0;

    $scope.showEdit = function() {
        $scope.selectedIndex = 1;
    };

    $scope.savePalette = function() {
        var themeObj = {}
        if ($scope.defaultTheme) {
            themeObj.mainTheme = $scope.defaultTheme;
        }
        if ($scope.secondaryTheme) {
            theme.alternateTheme = $scope.secondaryTheme;
        }
        console.log(themeObj);
       /* ManageService.createTheme($scope.project.tenantId, themeObj).then(function(response) {
            console.log(response);
        }, function(response) {
            console.log("error creating theme");
        });*/
    };

    $scope.preview = function() {
        if ($scope.mainTheme) {
            $scope.defaultTheme = {};
            if ($scope.mainTheme.primaryColor && $scope.mainTheme.accentColor && $scope.mainTheme.warnColor) {
                $scope.defaultTheme = {
                    "primaryPallete": {
                        '50': tinycolor($scope.mainTheme.primaryColor).lighten(38).toString(),
                        '100': tinycolor($scope.mainTheme.primaryColor).lighten(31).toString(),
                        '200': tinycolor($scope.mainTheme.primaryColor).lighten(20).toString(),
                        '300': tinycolor($scope.mainTheme.primaryColor).lighten(12).toString(),
                        '400': tinycolor($scope.mainTheme.primaryColor).lighten(6).toString(),
                        '500': tinycolor($scope.mainTheme.primaryColor).lighten(0).toString(),
                        '600': tinycolor($scope.mainTheme.primaryColor).lighten(6).toString(),
                        '700': tinycolor($scope.mainTheme.primaryColor).lighten(14).toString(),
                        '800': tinycolor($scope.mainTheme.primaryColor).lighten(18).toString(),
                        '900': tinycolor($scope.mainTheme.primaryColor).lighten(22).toString(),
                        'A100': tinycolor($scope.mainTheme.primaryColor).lighten(12).toString(),
                        'A200': tinycolor($scope.mainTheme.primaryColor).lighten(0).toString(),
                        'A400': tinycolor($scope.mainTheme.primaryColor).lighten(14).toString(),
                        'A700': tinycolor($scope.mainTheme.primaryColor).lighten(22).toString(),
                        "contrastDefaultColor": $scope.mainTheme.primaryColorText_isDark
                    },
                    "accentPalette": {
                        '50': tinycolor($scope.mainTheme.accentColor).lighten(38).toString(),
                        '100': tinycolor($scope.mainTheme.accentColor).lighten(31).toString(),
                        '200': tinycolor($scope.mainTheme.accentColor).lighten(20).toString(),
                        '300': tinycolor($scope.mainTheme.accentColor).lighten(12).toString(),
                        '400': tinycolor($scope.mainTheme.accentColor).lighten(6).toString(),
                        '500': tinycolor($scope.mainTheme.accentColor).lighten(0).toString(),
                        '600': tinycolor($scope.mainTheme.accentColor).lighten(6).toString(),
                        '700': tinycolor($scope.mainTheme.accentColor).lighten(14).toString(),
                        '800': tinycolor($scope.mainTheme.accentColor).lighten(18).toString(),
                        '900': tinycolor($scope.mainTheme.accentColor).lighten(22).toString(),
                        'A100': tinycolor($scope.mainTheme.accentColor).lighten(12).toString(),
                        'A200': tinycolor($scope.mainTheme.accentColor).lighten(0).toString(),
                        'A400': tinycolor($scope.mainTheme.accentColor).lighten(14).toString(),
                        'A700': tinycolor($scope.mainTheme.accentColor).lighten(22).toString(),
                        "contrastDefaultColor": $scope.mainTheme.accentColorText_isDark
                    },
                    "warnPalette": {
                        '50': tinycolor($scope.mainTheme.warnColor).lighten(38).toString(),
                        '100': tinycolor($scope.mainTheme.warnColor).lighten(31).toString(),
                        '200': tinycolor($scope.mainTheme.warnColor).lighten(20).toString(),
                        '300': tinycolor($scope.mainTheme.warnColor).lighten(12).toString(),
                        '400': tinycolor($scope.mainTheme.warnColor).lighten(6).toString(),
                        '500': tinycolor($scope.mainTheme.warnColor).lighten(0).toString(),
                        '600': tinycolor($scope.mainTheme.warnColor).lighten(6).toString(),
                        '700': tinycolor($scope.mainTheme.warnColor).lighten(14).toString(),
                        '800': tinycolor($scope.mainTheme.warnColor).lighten(18).toString(),
                        '900': tinycolor($scope.mainTheme.warnColor).lighten(22).toString(),
                        'A100': tinycolor($scope.mainTheme.warnColor).lighten(12).toString(),
                        'A200': tinycolor($scope.mainTheme.warnColor).lighten(0).toString(),
                        'A400': tinycolor($scope.mainTheme.warnColor).lighten(14).toString(),
                        'A700': tinycolor($scope.mainTheme.warnColor).lighten(22).toString(),
                        "contrastDefaultColor": $scope.mainTheme.warnColorText_isDark
                    },

                }
            }
        }
        if ($scope.alternateTheme) {
            $scope.secondaryTheme = {};
            if ($scope.alternateTheme.primaryColor && $scope.alternateTheme.accentColor && $scope.alternateTheme.warnColor) {
                $scope.secondaryTheme = {
                    "primaryPallete": {
                        '50': tinycolor($scope.alternateTheme.primaryColor).lighten(38).toString(),
                        '100': tinycolor($scope.alternateTheme.primaryColor).lighten(31).toString(),
                        '200': tinycolor($scope.alternateTheme.primaryColor).lighten(20).toString(),
                        '300': tinycolor($scope.alternateTheme.primaryColor).lighten(12).toString(),
                        '400': tinycolor($scope.alternateTheme.primaryColor).lighten(6).toString(),
                        '500': tinycolor($scope.alternateTheme.primaryColor).lighten(0).toString(),
                        '600': tinycolor($scope.alternateTheme.primaryColor).lighten(6).toString(),
                        '700': tinycolor($scope.alternateTheme.primaryColor).lighten(14).toString(),
                        '800': tinycolor($scope.alternateTheme.primaryColor).lighten(18).toString(),
                        '900': tinycolor($scope.alternateTheme.primaryColor).lighten(22).toString(),
                        'A100': tinycolor($scope.alternateTheme.primaryColor).lighten(12).toString(),
                        'A200': tinycolor($scope.alternateTheme.primaryColor).lighten(0).toString(),
                        'A400': tinycolor($scope.alternateTheme.primaryColor).lighten(14).toString(),
                        'A700': tinycolor($scope.alternateTheme.primaryColor).lighten(22).toString(),
                        "contrastDefaultColor": $scope.alternateTheme.primaryColorText_isDark
                    },
                    "accentPalette": {
                        '50': tinycolor($scope.alternateTheme.accentColor).lighten(38).toString(),
                        '100': tinycolor($scope.alternateTheme.accentColor).lighten(31).toString(),
                        '200': tinycolor($scope.alternateTheme.accentColor).lighten(20).toString(),
                        '300': tinycolor($scope.alternateTheme.accentColor).lighten(12).toString(),
                        '400': tinycolor($scope.alternateTheme.accentColor).lighten(6).toString(),
                        '500': tinycolor($scope.alternateTheme.accentColor).lighten(0).toString(),
                        '600': tinycolor($scope.alternateTheme.accentColor).lighten(6).toString(),
                        '700': tinycolor($scope.alternateTheme.accentColor).lighten(14).toString(),
                        '800': tinycolor($scope.alternateTheme.accentColor).lighten(18).toString(),
                        '900': tinycolor($scope.alternateTheme.accentColor).lighten(22).toString(),
                        'A100': tinycolor($scope.alternateTheme.accentColor).lighten(12).toString(),
                        'A200': tinycolor($scope.alternateTheme.accentColor).lighten(0).toString(),
                        'A400': tinycolor($scope.alternateTheme.accentColor).lighten(14).toString(),
                        'A700': tinycolor($scope.alternateTheme.accentColor).lighten(22).toString(),
                        "contrastDefaultColor": $scope.alternateTheme.accentColorText_isDark
                    },
                    "warnPalette": {
                        '50': tinycolor($scope.alternateTheme.warnColor).lighten(38).toString(),
                        '100': tinycolor($scope.alternateTheme.warnColor).lighten(31).toString(),
                        '200': tinycolor($scope.alternateTheme.warnColor).lighten(20).toString(),
                        '300': tinycolor($scope.alternateTheme.warnColor).lighten(12).toString(),
                        '400': tinycolor($scope.alternateTheme.warnColor).lighten(6).toString(),
                        '500': tinycolor($scope.alternateTheme.warnColor).lighten(0).toString(),
                        '600': tinycolor($scope.alternateTheme.warnColor).lighten(6).toString(),
                        '700': tinycolor($scope.alternateTheme.warnColor).lighten(14).toString(),
                        '800': tinycolor($scope.alternateTheme.warnColor).lighten(18).toString(),
                        '900': tinycolor($scope.alternateTheme.warnColor).lighten(22).toString(),
                        'A100': tinycolor($scope.alternateTheme.warnColor).lighten(12).toString(),
                        'A200': tinycolor($scope.alternateTheme.warnColor).lighten(0).toString(),
                        'A400': tinycolor($scope.alternateTheme.warnColor).lighten(14).toString(),
                        'A700': tinycolor($scope.alternateTheme.warnColor).lighten(22).toString(),
                        "contrastDefaultColor": $scope.alternateTheme.warnColorText_isDark
                    },

                }
            }
        }

        $scope.generatePalette = true;


    };




    var init = function() {
        if (!ManageService.projectLayout) {
            $scope.$parent.$parent.manageNext = $state.current.name;
            $scope.projectLayout = ManageService.projectLayout;
            $state.go('con.manage');
        } else {

            $scope.project = ManageService.project;
            $scope.projectAbout = ManageService.projectAbout;
            $scope.themeObj = ManageService.theme;

        }
    };

    init();



    $scope.toggleLeft = buildToggler('testNav');

    function debounce(func, wait, context) {
        var timer;
        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function() {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }
    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildToggler(navID) {
        return function() {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav(navID)
                .toggle()
                .then(function() {

                });
        }
    }

}]);
