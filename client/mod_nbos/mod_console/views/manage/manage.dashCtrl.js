angular.module('mod.console')

.controller('ManageDashCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', '$window', '$mdMedia', '$mdDialog', 'cfpLoadingBar', '$rootScope', function($scope, $state, $stateParams, ManageService, AlertService, $window, $mdMedia, $mdDialog, cfpLoadingBar, $rootScope) {

    var browseModules = function() {
        console.log("CLICKED");
    };
    $scope.logo = "mod_nbos/assets/images/project.png";

    var tenantId = $stateParams.id;

    var setModulesList = function(list) {
        $scope.subscribedModules = [];

        var obj;
        // var adminModules = ["app", "core", "identity", "oauth"];
        var adminModules = [];

        for (var k = 0; k < list.length; k++) {
            /*
             todo : silliest way to recognize native modules. This functionality needs to go into API.
             */
            if (list[k].uuid.length > 0 && adminModules.indexOf(list[k].name) < 0) {
                obj = {};
                obj.subSectionName = list[k].name;
                obj.disabled = false;
                obj.sref = "con.dashboard";
                obj.id = list[k].uuid;
                if (list[k].logoImages && list[k].logoImages.mediaFileDetailsList) {
                    obj.isIcon = false;
                    obj.imageInfo = list[k].logoImages.mediaFileDetailsList[0].mediapath;
                } else {
                    obj.isIcon = true;
                    obj.iconInfo = {
                        iconName: "dvr",
                        iconColor: "#000"
                    };
                }
                $scope.subscribedModules.push(obj);

            };
        };

        //console.log($scope.subscribedModules);
    };

    $scope.backToProjects = function() {
        $state.go('con.dashboard');
    };

    $scope.getProjectCompletionStatus = function() {
        //Read About and identify whats is missing
    };

    var helpContent = [{
        name: 'IDN - Identity Module',
        description: '<h2>What is IDN?</h2>' +
            '<p>' +
            'IDN or Identity Module forms the core of your application. This module built and maintained by NBOS is a multi-tenant design. Out of the box, IDN provides you with:' +
            '<ul>' +
            '<li>Login/Signup Features</li>' +
            '<li>Social Login Options</li>' +
            '<li>User Profile</li>' +
            '<li>Session Management</li>' +
            '<li>Inbuilt Notifications</li>' +
            '</ul>' +
            '</p>'
    }, {
        name: "Back-End Modules",
        description: '<h2>What is a module?</h2>' +
            '<p>' +
            'A module is a chuck of code that accomplishes a specific functionality in your app. A module can be your entire app. You can add such modules from our marketplace too.' +
            '</p>' +
            '<p>' +
            '<b>For those who understand "complicated": </b>When building your own backend, you will need to validate the user request token from IDN. For IDN to recognize you, we would want to register your backend as a module (or multiple modules, if you have a bigger scope) and use this module ID to communicate with the backend.' +
            '</p>'
    }];


    $scope.showHelp = function(ev, index) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
                controller: function($scope, info) {
                    $scope.renderHtml = function() {
                        return $sce.trustAsHtml(info.description);
                    };
                    $scope.info = info;
                    $scope.description = info.description;
                    $scope.hide = function() {
                        $mdDialog.hide();
                    };
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };
                    $scope.answer = function(answer) {
                        $mdDialog.hide(answer);
                    };
                },
                templateUrl: 'mod_nbos/mod_console/views/manage/manage.help.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen,
                locals: {
                    info: helpContent[index]
                }
            })
            .then(function(answer) {
                $scope.status = 'You said the information was "' + answer + '".';
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        $scope.$watch(function() {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    $scope.addNewModule = function(ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
                controller: function($scope) {

                    $scope.moduleName = '';
                    $scope.error = "";

                    $scope.hide = function() {
                        $mdDialog.cancel();
                    };
                    $scope.cancel = function() {
                        $mdDialog.cancel();
                    };

                    $scope.answer = function() {
                        $scope.error = "";
                        var moduleObj = {
                            "clientSecret": "nbosSecret",
                            "name": $stateParams.id.split(':')[1] + '@' + $scope.moduleName,
                            "tenantId": $stateParams.id
                        };

                        ManageService.createModule(moduleObj).then(function(response) {
                            if (response) {
                                $mdDialog.hide(true);
                            };
                        }, function(error) {
                            $scope.error = "Module Name already exists";
                        });

                    };
                },
                templateUrl: 'mod_nbos/mod_console/views/manage/manage.createModule.template.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: useFullScreen
            })
            .then(function(answer) {
                if (answer) {
                    ManageService.getTenantModules(tenantId).then(function() {
                        $scope.tenantModules = ManageService.tenantModules;

                    }, function(error) {
                        AlertService.alert("Error refreshing modules. Please refresh the page.", "md-warn");
                    });
                };
            }, function() {
                //do nothing on cancel
            });
        $scope.$watch(function() {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function(wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };


    var init = function() {
        if (!ManageService.project) {
            console.log("no project");
            $scope.manageNext = $state.current.name;
            console.log($scope.manageNext);
            $state.go('con.manage');
        } else {
            $scope.project = ManageService.project;
            $scope.projectAbout = ManageService.projectAbout;
            setModulesList(ManageService.subscribedModules);
            $scope.tenantModules = ManageService.tenantModules;
            console.log($rootScope.determinateValue);
        }
    };

    init();


}]);
