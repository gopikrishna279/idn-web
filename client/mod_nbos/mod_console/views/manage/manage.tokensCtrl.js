angular.module('mod.console')

.controller('ManageTokensCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', function($scope, $state, $stateParams, ManageService, AlertService) {


    // $stateParams.id
    $scope.tokens = [];


    var init = function() {
        if (!ManageService.projectLayout && !ManageService.tenantTokens) {
            $scope.$parent.$parent.manageNext = $state.current.name;
            $scope.projectLayout = ManageService.projectLayout;
            $state.go('con.manage');
        } else {
            $scope.project = ManageService.project;
            $scope.projectAbout = ManageService.projectAbout;
            $scope.tokens = ManageService.tenantTokens;
        }
    };

    init();





}]);
