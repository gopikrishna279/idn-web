angular.module('mod.console')

.controller('ManageFeCodeCtrl', ['$scope', 'ManageService', '$state', 'AlertService', '$window', '$rootScope', '$mdDialog', function($scope, ManageService, $state, AlertService, $window, $rootScope, $mdDialog) {

    $scope.project = {};
    $scope.projectAbout = {};
    console.log($scope.determinateValue);


    $scope.getApp = function(ev,lang) {
        if ($rootScope.isDownload) {
            ManageService.getApp($scope.project.tenantId, lang).then(function(success) {
                if (success.downloadUrl) {
                    $window.location.assign("http://" + success.downloadUrl);
                } else {
                    AlertService.alert("Something Went Wrong in downloading starter code.", "md-warn");
                }
            }, function(error) {
                var message = "";
                if (error && error.data && error.data != "") {

                    message = error.statusText + ". " + error.data.message;
                } else {
                    message = "There was an unknown error processing your request. Please try again later.";
                }
                AlertService.alert(message, "md-warn");
            });
        } else {
            $mdDialog.show(
                $mdDialog.alert()
                .parent(document.body)
                .clickOutsideToClose(true)
                .title('')
                .textContent('Please complete the required fields to dowload the code')
                .ariaLabel('Alert Dialog')
                .ok('Got it!')
                .targetEvent(ev)
            );
        };
    };

    $scope.startDownload = function() {

    };


    var init = function() {
        if (!ManageService.projectAbout) {
            $scope.$parent.$parent.manageNext = $state.current.name;
            console.log($scope.manageNext);
            $state.go('con.manage');
        } else {

            $scope.projectAbout = ManageService.projectAbout;
            $scope.project = ManageService.project;
            $scope.projectAbout.tenantId = $scope.project.tenantId;
        }
    };

    init();

}]);
