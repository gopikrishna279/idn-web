angular.module('mod.console')

.controller('ManageBeCodeCtrl', ['$scope', 'ManageService', 'AlertService', '$state', 'StarterCodeService', '$window', function($scope, ManageService, AlertService, $state, StarterCodeService, $window) {

    $scope.project = {};
    $scope.projectAbout = {};

    $scope.getStarterCode = function(ev, lang) {
        var obj = {};
        obj.moduleId = ModuleService.module.uuid;
        obj.lang = lang;
        obj.tenantId = $scope.devTenants.tenantId;
        StarterCodeService.get_starter_api_code(obj).then(function(success) {

            if (success.downloadUrl) {
                $window.location.assign("http://" + success.downloadUrl);
            } else {
                AlertService.alert("Something Went Wrong in downloading" + lang + "starter code", "md-warn");
            }
        }, function(error) {
            AlertService.alert("Something Went Wrong", "md-warn");
        });
    };


    var init = function() {
        if (!ManageService.projectAbout) {
            $scope.$parent.$parent.manageNext = $state.current.name;
            console.log($scope.manageNext);
            $state.go('con.manage');
        } else {

            $scope.projectAbout = ManageService.projectAbout;
            $scope.project = ManageService.project;
            $scope.projectAbout.tenantId = $scope.project.tenantId;
        }
    };

    init();

}]);
