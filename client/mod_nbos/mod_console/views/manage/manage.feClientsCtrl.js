'use strict';

/**
 * NOTE: The Client with the name "API Client" is created when the tenant is created.
 */


angular.module('mod.console')

    .controller('ManageFeClientsCtrl', ['$scope', '$state', '$stateParams', 'ManageService', '$mdDialog', 'AlertService', function($scope, $state, $stateParams, ManageService, $mdDialog, AlertService) {


        var tenantId = $stateParams.id;
        $scope.feClients = [];

        $scope.createClient = function(ev) {
            $mdDialog.show({
                controller: function($scope, $mdDialog, ManageService, tenantId) {
                    $scope.showError = false;
                    $scope.clientObj = {
                        "authorities": [],
                        "authorizedGrantTypes": ["client_credentials","refresh_token"],
                        "resourceIds": [],
                        "scopes": [],
                        "redirectUris": [],
                        "tenantId": tenantId
                    };
                    $scope.create = function() {
                        if($scope.clientObj.clientName.toLowerCase() == "api client"){
                            $scope.showError = true;
                        } else {
                            ManageService.saveClient(tenantId, $scope.clientObj).then(function(response) {
                                if (response) {
                                    $mdDialog.hide(response);
                                };
                            }, function(error) {
                                console.log("error saving layout");
                            });
                        };
                    };

                    $scope.closeDialog = function() {
                        $mdDialog.cancel();
                    }
                },
                locals: {
                    "tenantId": $scope.project.tenantId
                },
                templateUrl: 'mod_nbos/mod_console/views/manage/ClientCreate.html',
                parent: angular.element(document.body),
                targetEvent: ev
            })
                .then(function(client) {
                    if (client) {
                        ManageService.getClients(tenantId).then(function(success){
                            $scope.projectClients = ManageService.projectClients;
                            getFeClients($scope.projectClients);
                        }, function(error){
                            AlertService.alert("Oops! Something went wrong. Please refresh the page.", "md-warn");
                        });

                    }
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };

        $scope.deleteClient = function(client, ev) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete the client?')
                .textContent('')
                .ariaLabel('confirm delete')
                .targetEvent(ev)
                .ok('Ok')
                .cancel('Cancel');

            $mdDialog.show(confirm).then(function() {
                if(client.clientName.toLowerCase == "api client"){
                    AlertService.alert("Client delete restricted for this client", "md-warn");
                } else {

                    ManageService.deleteClient(tenantId, client.clientId).then(function(success){
                        AlertService.alert("Client Deleted Successfully", "md-primary");
                        ManageService.getClients(id).then(function(success){
                            $scope.projectClients = ManageService.projectClients;
                        }, function(error){
                            AlertService.alert("Oops! Something went wrong. Just reload the page.", "md-warn");
                        });

                    }, function(error){
                        AlertService.alert("Error Deleting Client", "md-warn");
                    });
                }

            }, function() {

            });
        };

        var getFeClients = function(){
            $scope.feClients = [];
            for(var i = 0; i < $scope.projectClients.length; i ++){
                if($scope.projectClients[i].clientName != "API Client"){
                    $scope.feClients.push($scope.projectClients[i]);
                }
            };
        };

        var init = function() {
            if (!ManageService.projectClients) {
                $scope.$parent.$parent.manageNext = $state.current.name;
                $scope.projectClients = ManageService.projectClients;
                $state.go('con.manage');
            } else {

                $scope.project = ManageService.project;
                $scope.projectAbout = ManageService.projectAbout;
                $scope.projectClients = ManageService.projectClients;
                getFeClients();
            }
        };

        init();





    }]);
