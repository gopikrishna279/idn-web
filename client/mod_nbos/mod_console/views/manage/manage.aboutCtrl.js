angular.module('mod.console')

    .controller('ManageAboutCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', function($scope, $state, $stateParams, ManageService, AlertService){

        $scope.project = {};
        $scope.projectAbout = {};
        $scope.isEdit = false;

        $scope.selectedIndex = 0;

        $scope.showEdit = function(val){
            // AlertService.alert("Please make changes and ")
            $scope.isEdit = val;
        };

        $scope.saveAboutApp = function(){
            ManageService.saveAbout($scope.projectAbout).then(function(success){
                AlertService.alert("Application information updated successfully!", "md-primary");
                $scope.isEdit = false;
            }, function(error){
                AlertService.alert("Oops! Something went wrong. Please try again.", "md-warn");
                console.error("ERROR saving project ABOUT");
                console.log(error);
            });
        };

        $scope.saveImage = function(imageFor){
            var file;
            switch(imageFor){
                case "logo":
                    file = $scope.logoImage;
                    break;
                case "banner":
                    file = $scope.bannerImage;
                    break;
            };

            if(file){
                ManageService.saveAboutImage($scope.project.tenantId, imageFor, file).then(function(success){
                    AlertService.alert("Hurray! Image saved successfully", "md-primary");
                    switch(imageFor){
                        case "logo":
                            ManageService.projectAbout.logoImage = success;
                            $scope.projectAbout.logoImage = success;
                            break;
                        case "banner":
                            ManageService.projectAbout.bannerImage = success;
                            $scope.projectAbout.bannerImage = success;
                            break;
                    };
                    
                }, function(error){
                    AlertService.alert("Oops! Something went wrong. Please try again with another image.", "md-warn");
                });
            }

        };

        var init = function(){
            if(!ManageService.projectAbout){
                $scope.$parent.$parent.manageNext = $state.current.name;
                console.log($scope.manageNext);
                $state.go('con.manage');
            } else {

                $scope.projectAbout = ManageService.projectAbout;
                $scope.project = ManageService.project;
                $scope.projectAbout.tenantId = $scope.project.tenantId;
            }
        };

        init();
        
    }]);