angular.module('mod.console')

.controller('ManageConfigureIdnCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'AlertService', '_', function($scope, $state, $stateParams, ManageService, AlertService, _) {

    $scope.project = {};
    $scope.projectAbout = {};
    $scope.configArr = [];
    $scope.appConfig = {};


    var getIdnConf = function(projectConfig) {

        // Filter the identity module from the list of modules
        _.find(projectConfig, function(project) {
            if (project.uuid == 'MOD:identity') {
                $scope.modConfig = project;
            };
        });

        //Build the idn config Object for UI, Seperate the social accounts and their respective values
        $scope.configArr = _.filter($scope.modConfig.configuration, function(social) {
            if (!(/.*\..*\..*/g.test(social.name))) {

                return social.name.split('social.')[1];
            } else {

            }

        });

        _.each($scope.configArr, function(social) {
            _.each($scope.modConfig.configuration, function(obj) {
                var socialName = social.name.split('social.')[1] + '.';
                if (obj.name.indexOf(socialName) > -1) {
                    if (!social.values) {
                        social.values = "false";
                    };
                    if (!social.innerObj) {
                        social.innerObj = [];
                    };
                    social.innerObj.push(obj);
                };
            });
        });
    };

    // Clear the Inner obj if the social login is turned off
    $scope.updateSocial = function(social) {
        if (social.values == 'false') {
            _.each(social.innerObj, function(item) {
                item.values = "";
            })
        };
    };

    //Update the config based on the user values

    $scope.updateConfig = function() {
        var finalConfig = [];
        _.each($scope.configArr, function(config) {
            if (config.innerObj) {
                _.each(config.innerObj, function(obj) {
                    finalConfig.push(obj);
                });
                finalConfig.push(_.omit(config, 'innerObj'));

            }
        });

        $scope.modConfig.tenantId = $stateParams.id;
        $scope.modConfig.configuration = finalConfig;
        ManageService.saveConfig($scope.modConfig).then(function(success) {
            AlertService.alert("Configuration successfully saved for " + $scope.modConfig.name, "md-primary");
        }, function(error) {
            AlertService.alert("Error in saving configuration for " + $scope.modConfig.name, "md-warn");
        });
    };


    //Saving for APP configuration
    $scope.saveConfigForModule = function(module) {

        module.tenantId = $stateParams.id;
        ManageService.saveConfig(module).then(function(success) {
            $scope.moduleConfig = module;
            AlertService.alert("Configuration successfully saved for " + module.name, "md-primary");
        }, function(error) {
            AlertService.alert("Error in saving configuration for " + module.name, "md-warn");
        });



    };


    var init = function() {
        if (!ManageService.projectAbout) {
            $scope.$parent.$parent.manageNext = $state.current.name;
            console.log($scope.manageNext);
            $state.go('con.manage');
        } else {
            //get
            $scope.projectAbout = ManageService.projectAbout;
            $scope.project = ManageService.project;
            $scope.modules = ManageService.modules;
            getIdnConf(ManageService.projectConfig);
            _.find(ManageService.projectConfig, function(module) {
                if (module.uuid == 'MOD:app') {
                    $scope.appConfig = module;
                }
            });

        };
    };

    init();

}]);
