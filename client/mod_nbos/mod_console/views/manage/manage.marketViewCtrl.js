angular.module('mod.console')

.controller('ManageMarketViewCtrl', ['$scope', '$state', '$stateParams', 'ManageService', 'LayoutService', '$q', function($scope, $state, $stateParams, ManageService, LayoutService, $q) {

    $scope.project = {};
    $scope.subscribedModules = {};
    $scope.isMarketPlace = true;

    $scope.projectDash = function() {
        var modules = ManageService.getSubscribedModules($scope.project.tenantId),
            tenantModules = ManageService.getTenantModules($scope.project.tenantId),
            config = ManageService.getAllConfig($scope.project.tenantId);

        $q.all([modules, tenantModules, config])
            .then(function(success) {
                $state.go('con.manage.dash', { id: $scope.project.tenantId });
            }, function(error) {
                console.error(error);
                $state.go('con.projects');
            });

    };


    var init = function() {
        if (!ManageService.project) {
            console.log("no project");
            $scope.$parent.$parent.manageNext = $state.current.name;
            $scope.$parent.$parent.manageParams = {};
            $scope.$parent.$parent.manageParams.id = $stateParams.id;
            $scope.$parent.$parent.manageParams.moduleId = $stateParams.moduleId;
            $state.go('con.manage');
        } else {
            $scope.project = ManageService.project;
            $scope.projectAbout = ManageService.projectAbout;
        }
    };



    init();

}]);
