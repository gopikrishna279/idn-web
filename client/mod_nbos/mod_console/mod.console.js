/**
 *
 */

'use strict';

angular.module('mod.console', ['ngSanitize', 'swaggerUi', 'dndLists'])
    .service('swaggerModule', ['$rootScope', '$q', function($rootScope, $q) {

        this.execute = function(req) {
            var bearer = "Bearer " + $rootScope.admin_user_token;
            req.headers.Authorization = bearer;
            var deferred = $q.defer();
            deferred.resolve(true);
            return deferred.promise;
        }

    }])
    .run(function(swaggerModules, swaggerModule) {
        swaggerModules.add(swaggerModules.BEFORE_EXPLORER_LOAD, swaggerModule);
    })

.config(['$stateProvider', function($stateProvider) {

    $stateProvider
        .state('con', {
            url: '/con',
            parent: "layout",
            template: "<div ui-view flex></div>",
            data: {
                moduleName: "IDN",
                type: 'nav',
                authenticate: false,
                displayLink: false,
                disableLink: false,
                displayName: "Identity",
                displayIcon: ""
            }
        })
        //FOR NAVIGATION
        .state('con.header', {
            url: '',
            template: "<div></div>",
            data: {
                templateUrl: 'mod_nbos/mod_console/views/menu/console.header.html',
                position: 1,
                moduleName: "Console",
                type: 'header',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Profile",
                displayIcon: "",
                authorities: ""
            }
        })
        .state('con.dashboard', {
            url: '/dash',
            templateUrl: "mod_nbos/mod_console/views/dashboard/console.dashboard.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Dashboard",
                displayIcon: ""
            }
        })
        .state('con.projects', {
            url: '/projects',
            templateUrl: "mod_nbos/mod_console/views/projects/projects.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: true,
                disableLink: false,
                authenticate: true,
                displayName: "Projects",
                displayIcon: ""
            }
        })
        .state('con.create', {
            url: '/create',
            templateUrl: "mod_nbos/mod_console/views/projects/create.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Create project",
                displayIcon: ""
            }
        })
        .state('con.modulesMarketplace', {
            url: '/theme',
            templateUrl: "mod_nbos/mod_console/views/module/moduleMarketplace.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Layout",
                displayIcon: ""
            }
        })
        .state('con.manage', {
            url: '/manage/:id',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Primary",
                displayIcon: ""
            }
        })
        .state('con.manage.dash', {
            url: '/dashboard',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.dash.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Dashboard",
                displayIcon: ""
            }
        })
        .state('con.manage.about', {
            url: '/about',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.about.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage About",
                displayIcon: ""
            }
        })
        .state('con.manage.config', {
            url: '/config',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.appConfig.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Configuration",
                displayIcon: ""
            }
        })
        .state('con.manage.feCode', {
            url: '/feCode',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.feCode.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Front End Code",
                displayIcon: ""
            }
        })
        .state('con.manage.feClients', {
            url: '/feClients',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.feClients.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Front End Clients",
                displayIcon: ""
            }
        })
        .state('con.manage.clientTokens', {
            url: '/client/:clientId/tokens',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.clientTokens.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Clients",
                displayIcon: ""
            }
        })
        .state('con.manage.tenantModule', {
            url: '/tenantModule/:moduleId',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.tenantModule.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Tenant Module View",
                displayIcon: ""
            }
        })
        .state('con.manage.beCode', {
            url: '/beCode',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.beCode.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Back End Code",
                displayIcon: ""
            }
        })
        .state('con.manage.beClients', {
            url: '/beClients',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.beClients.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Back End Clients",
                displayIcon: ""
            }
        })
        .state('con.manage.clients', {
            url: '/clients',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.clients.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Clients",
                displayIcon: ""
            }
        })
        .state('con.manage.authorities', {
            url: '/authorities',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.authorities.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Authorities",
                displayIcon: ""
            }
        })
        .state('con.manage.configureIdn', {
            url: '/confIdn',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.configureIdn.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Conf Idn",
                displayIcon: ""
            }
        })
        .state('con.manage.roles', {
            url: '/roles',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.roles.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Conf Idn",
                displayIcon: ""
            }
        })
        .state('con.manage.rolesCreate', {
            url: '/roles/create/:roleId',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.createRole.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Conf Idn",
                displayIcon: ""
            }
        })
        .state('con.manage.tokens', {
            url: '/tokens',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.tokens.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Tokens",
                displayIcon: ""
            }
        })
        .state('con.manage.layout', {
            url: '/layout',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.layout.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Layout",
                displayIcon: ""
            }
        })
        .state('con.manage.theme', {
            url: '/theme',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.theme.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Theme",
                displayIcon: ""
            }
        })
        .state('con.manage.market', {
            url: '/market',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.market.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Market",
                displayIcon: ""
            }
        })
        .state('con.manage.view', {
            url: '/market/:moduleId/view',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.marketView.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Module View",
                displayIcon: ""
            }
        })
        .state('con.manage.module', {
            url: '/module/:moduleId',
            templateUrl: "mod_nbos/mod_console/views/manage/manage.moduleView.html",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: false,
                disableLink: false,
                authenticate: true,
                displayName: "Manage Module",
                displayIcon: ""
            }
        })

    .state('con.manage.users', {
        url: '/users',
        templateUrl: "mod_nbos/mod_console/views/manage/manage.users.html",
        data: {
            moduleName: "Console",
            type: 'nav',
            displayLink: false,
            disableLink: false,
            authenticate: true,
            displayName: "Manage Module",
            displayIcon: ""
        }
    })

    .state('con.billing', {
            url: '/billing',
            template: "<div>Welcome to Console View</div>",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: true,
                disableLink: true,
                authenticate: true,
                displayName: "Billing Info",
                displayIcon: ""
            }
        })
        .state('con.support', {
            url: '/support',
            template: "<div>Welcome to Console View</div>",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: true,
                disableLink: true,
                authenticate: true,
                displayName: "Support",
                displayIcon: ""
            }
        })
        .state('con.forum', {
            url: '/forum',
            template: "<div>Welcome to Console View</div>",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: true,
                disableLink: true,
                authenticate: true,
                displayName: "Forum",
                displayIcon: ""
            }
        })
        .state('con.guide', {
            url: '/guide',
            template: "<div>Welcome to Console View</div>",
            data: {
                moduleName: "Console",
                type: 'nav',
                displayLink: true,
                disableLink: true,
                authenticate: true,
                displayName: "Guide",
                displayIcon: ""
            }
        })
}])
