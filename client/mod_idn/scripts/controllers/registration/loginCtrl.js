/**
 * Created by nbos on 3/14/16.
 */
angular.module('mod.idn')
    .controller('LoginCtrl', ['$scope', 'LoginService', 'AlertService', 'SessionService', '$state', 'UserService', 'FBService', 'GoogleService', '$mdMedia', 'LayoutService', function($scope, LoginService, AlertService, SessionService, $state, UserService, FBService, GoogleService, $mdMedia, LayoutService) {

        $scope.showProgress = false;
        $scope.disabledLogin = false;
        $scope.user = {
            username: '',
            password: ''
        };

        $scope.validateuser2 = function() {
            $state.go('dashboard');
        };

        var setUserData = function(responseObj) {
            UserService.setUser("member", responseObj.member);
            UserService.setUser("session", responseObj.token);
            UserService.setAdminRole();

            SessionService.setSession(responseObj.token, responseObj.member.uuid);

            LayoutService.buildLayout().then(function (laySuccess) {
                if (SessionService.checkSession()) {
                    $state.go('idn.dashboard');
                };
            }, function (layError) {
                console.log("Error Buinding Layout");
            });

        };

        $scope.validateuser = function(loginForm) {

            $scope.disabledLogin = true;
            // $scope.showProgress = true;
            LoginService.login($scope.user)
                .then(function(success) {
                    try {
                        setUserData(success);
                    } catch (err) {
                        // alert("ERROR");
                        var message = err.statusText + ": ";
                        for (var i = 0; i < err.data.errs.length; i++) {
                            message += error.data.errors[i].message + ". ";
                        }
                        AlertService.alert(message, 'md-warn');
                    } finally {
                        $scope.disabledLogin = false;
                        $scope.user = {
                            username: '',
                            password: ''
                        };
                    };
                }, function(error) {
                    $scope.disabledLogin = false;
                    var message = error.statusText + ": ";
                    for (var i = 0; i < error.data.errors.length; i++) {
                        message += error.data.errors[i].message + ". ";
                    }
                    AlertService.alert(message, 'md-warn');
                });

        };

        $scope.socialLogin = function(socialType) {
            switch (socialType) {
                case 'fb':
                    FBService.fbLogin().then(function(authResponse) {
                        if (authResponse) {
                            setUserData(authResponse);
                        }
                    }, function(error) {
                        var message = error.statusText + ": ";
                        for (var i = 0; i < error.data.errors.length; i++) {
                            message += error.data.errors[i].message + ". ";
                        }
                        AlertService.alert(message, 'md-warn');
                    });
                    break;
                case 'google':
                    GoogleService.googleLogin().then(function(authResponse) {
                        if (authResponse) {
                            setUserData(authResponse);
                        }
                    }, function(error) {
                        var message = error.statusText + ": ";
                        for (var i = 0; i < error.data.errors.length; i++) {
                            message += error.data.errors[i].message + ". ";
                        }
                        AlertService.alert(message, 'md-warn');
                    });
                    break;
            }
        };

        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;

        $scope.$watch(function() {
            return $mdMedia('xs');
        }, function(wantsFullScreen) {
            if($mdMedia('xs')){
                console.log("setting to true");
                $scope.minWidthClass = true;    
            } else {
                console.log("setting to false");
                $scope.minWidthClass = false;
            }
            
        });

    }]);
