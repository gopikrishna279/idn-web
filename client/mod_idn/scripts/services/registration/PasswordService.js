angular.module('mod.idn')
    //ALL SERVICE CALLS
    .factory('PasswordService', ['PasswordFactory', '$q', function(PasswordFactory, $q) {
        var factory = {};
        factory.changePasswd = function(user) {
            var deferred = $q.defer();
            PasswordFactory.updatePassword().authenticate(user).$promise.then(function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        return factory;
    }])


//ALL FACTORY DEFINITIONS START HERE

.factory('PasswordFactory', ['$resource', 'MOD_IDN', 'AppService', function($resource, MOD_IDN, AppService) {
    var factory = {};

    factory.updatePassword = function() {

        var bearer = "Bearer " + AppService.token.access_token;
        return $resource(MOD_IDN.API_URL + 'auth/changePassword', {}, {
            'authenticate': {
                method: 'POST',
                headers: {
                    "Authorization": bearer
                }
            }
        });
    };

    return factory;

}]);
