'use strict';

/**
 * @ngdoc overview
 * @name CodingLabs
 * @description
 * # CodingLabsApp
 *
 * Main module of the application.
 */
angular.module('mod.idn', []);


angular.module('mod.idn')

    .value('USER_CONFIG', {})

    .constant('MOD_IDN', {
        API_URL: 'https://idn.au-syd.mybluemix.net/api/identity/v0/',
        // API_URL: 'http://10.9.9.44:8080/api/identity/v0/',
        // API_URL_DEV: 'http://api.qa1.nbos.in/api/identity/v0/',
        QUALIFIER :'idn'
    })

    .config(['$stateProvider', '$urlRouterProvider', '$mdThemingProvider', function($stateProvider, $urlRouterProvider, $mdThemingProvide){

        // $urlRouterProvider.otherwise('/idn/dashboard');

        $stateProvider
            .state('idn', {
                url: '/idn',
                template: "<div ui-view flex></div>",
                data: {
                    type: 'home',
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            //FOR NAVIGATION
            .state('idn.header', {
                url: '/idnHeader',
                templateUrl: "<div></div>",
                data: {
                    templateUrl:'mod_idn/views/menu/idn.header.html',
                    position: 999,
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : true,
                    authenticate : false,
                    disableLink : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.login', {
                url: '/login',
                templateUrl: "mod_idn/views/registration/login.html",
                data: {
                    moduleName: "IDN",
                    type: 'login',
                    displayLink : false,
                    disableLink : false,
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.forgot', {
                url: '/forgot',
                templateUrl: "mod_idn/views/forgot/forgot.html",
                data: {
                    moduleName: "IDN",
                    type: 'login',
                    displayLink : false,
                    disableLink : false,
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.reset', {
                url: '/reset/:token',
                templateUrl: "mod_idn/views/reset/reset.html",
                data: {
                    moduleName: "IDN",
                    type: 'login',
                    displayLink : false,
                    disableLink : false,
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.register', {
                url: '/register',
                templateUrl: "mod_idn/views/registration/register.html",
                controller:'RegisterCtrl',
                data: {
                    moduleName: "IDN",
                    type: 'login',
                    displayLink : false,
                    disableLink : false,
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.success', {
                url: '/register/success',
                templateUrl: "mod_idn/views/registration/register.success.html",
                data: {
                    moduleName: "IDN",
                    type: 'login',
                    displayLink : false,
                    disableLink : false,
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.unauthorised', {
                url: '/unauthorised',
                templateUrl: "mod_idn/views/unauthorised.html",
                data: {
                    moduleName: "IDN",
                    type: 'login',
                    displayLink : false,
                    disableLink : false,
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.dashboard', {
                url: '/dashboard',
                templateUrl: "mod_idn/views/dashboard/idn.dashboard.html",
                data: {
                    moduleName: "IDN",
                    type: 'login',
                    displayLink : false,
                    disableLink : false,
                    authenticate : false,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn2', {
                url: '/idn2',
                template: "<div ui-view></div>",
                parent:'layout',
                data: {
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : false,
                    disableLink : false,
                    authenticate : true,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn2.profile', {
                url: '/profile',
                templateUrl: "mod_idn/views/profile/idn.profile.html",
                data: {
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : false,
                    disableLink : false,
                    authenticate : true,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn2.settings', {
                url: '/settings',
                templateUrl: "mod_idn/views/settings/idn.settings.html",
                data: {
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : false,
                    disableLink : false,
                    authenticate : true,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn2.settings.changePassword', {
                url: '/changePassword',
                templateUrl: "mod_idn/views/settings/idn.changePassword.html",
                data: {
                    type: 'home',
                    authenticate : true
                }
            })
            .state('idn2.bug', {
                url: '/issues',
                templateUrl: "mod_idn/views/bug/idn.bugReporting.html",
                data: {
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : false,
                    disableLink : false,
                    authenticate : true,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn2.bugView', {
                url: '/issues/:bugId',
                templateUrl: "mod_idn/views/bug/idn.bugView.html",
                data: {
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : false,
                    disableLink : false,
                    authenticate : true,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn2.bugCreate', {
                url: '/issue/create',
                templateUrl: "mod_idn/views/bug/idn.bugCreate.html",
                controller: "BugCreateCtrl",
                data: {
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : false,
                    disableLink : false,
                    authenticate : true,
                    displayName : "Identity",
                    displayIcon: ""
                }
            })
            .state('idn.403', {
                url: '/403',
                templateUrl: "mod_idn/views/403.html",
                data: {
                    moduleName: "IDN",
                    type: 'header',
                    displayLink : false,
                    disableLink : false,
                    authenticate : true,
                    displayName : "Identity",
                    displayIcon: ""
                }
            });
    }])
    .run(function(SessionService, UserService, $state, $rootScope, APP_CONFIG, USER_CONFIG, StateService) {

        $rootScope.$on('$stateChangeStart', function(event, next, params) {

            console.log(next);

            if(SessionService.checkSession()){
                console.log("SESSION EXISTS");

                /*
                    If Session Exists, then get all app related data, user related date etc. from IDN dashboard. Don't bother with /home
                 */

                USER_CONFIG.test = true;
                console.log(USER_CONFIG);
                if(APP_CONFIG.token){

                    console.log("APPCONFIG EXISTS");

                    if(USER_CONFIG.member){
                        console.log("USER CONFIG EXISTS");

                        //continue where the wind blows

                    } else {
                        console.log("USER CONFIG DOES NOT EXIST.");

                        //Send to dashboard to get user config
                        if(next.name != 'idn.dashboard'){
                            console.log("SENDING TO DASH");
                            event.preventDefault();

                            //This if condition is to prevent the StateService from saving a login type page.

                            if(next.data && next.data.type){
                                if(next.data.type != 'login' && next.data.type != 'home'){
                                    StateService.setState(event, next, params);
                                };
                            } else {
                                StateService.setState(event, next, params);
                            };

                            $state.go('idn.dashboard');
                        };
                    }
                } else {
                    console.log("APPCONFIG DOES NOT EXIST.");
                    // Send to 'home' to get appConfig

                    if(next.data.type != 'home'){
                        console.log("SENDING TO HOME.");

                        event.preventDefault();

                        if(next.data && next.data.type){
                            if(next.data.type != 'login' && next.data.type != 'home'){
                                StateService.setState(event, next, params);
                            };
                        } else {
                            StateService.setState(event, next, params);
                        };

                        $state.go('home');
                    }

                };

            } else {
                console.log("SESSION DOES NOT EXIST");
                /*
                    If Session Does not Exist. Go to home, get app data and redirect to login.
                 */
                if(APP_CONFIG.token){


                    if(next.name!= 'idn.dashboard'){

                        if(next.data && next.data.authenticate){

                            //Session does not exit and its a page that requires authentication. So go to login.
                            if(next.data.type && next.data.type == 'login' && next.data.type =='home'  ){
                                // continue to whichever login type page you are going to
                            } else {
                                //TODO - save state for post login. Do we want to implement that feature?
                                event.preventDefault();
                                $state.go('idn.login');
                            };

                        };
                    };

                } else {
                    // Redirect to load config first.
                    if(next.data.type != 'home'){
                        event.preventDefault();
                        if(next.data && next.data.type != 'login'){
                            StateService.setState(event, next, params);
                        } else {
                            //do nothing???
                            //Test if public pages are working as expected
                        };
                        $state.go('home');
                    };

                };

            };


        });
    });
