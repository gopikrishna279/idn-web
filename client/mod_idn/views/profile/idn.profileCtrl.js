'use strict';

angular.module('mod.idn')
    .controller('IdnProfileCtrl', ['$scope', '$mdDialog', 'SessionService', 'UserService', 'AlertService', 'USER_CONFIG', function($scope, $mdDialog, SessionService, UserService, AlertService, USER_CONFIG) {

        $scope.currentPage = 'page1';
        $scope.currentNavItem = 'page1';
        var userId = SessionService.getSession().uuid;
        $scope.user = {};
        console.log(USER_CONFIG)

        var getUser = function() {
            $scope.user = JSON.parse(JSON.stringify(USER_CONFIG.member));
            UserService.getNodeProfile().then(function(response) {
                if (USER_CONFIG.nodeMember && Object.keys(USER_CONFIG.nodeMember).indexOf("uuid") > -1) {
                    $scope.user.personalInfo = USER_CONFIG.nodeMember.personalInfo;
                    if (USER_CONFIG.nodeMember.birthday) {
                        $scope.user.birthday = new Date(USER_CONFIG.nodeMember.birthday);
                    }
                    if (USER_CONFIG.nodeMember.city) {
                        $scope.user.city = USER_CONFIG.nodeMember.city;
                    }
                    if (USER_CONFIG.nodeMember.gender) {
                        $scope.user.gender = USER_CONFIG.nodeMember.gender;
                    }
                    if (USER_CONFIG.nodeMember.education) {
                        $scope.user.education = USER_CONFIG.nodeMember.education;
                    }
                    if (USER_CONFIG.nodeMember.professional) {
                        $scope.user.professional = USER_CONFIG.nodeMember.professional;
                    };
                } else {
                    $scope.user.personalInfo = {};
                }
                buildObject();
            }, function(error) {

            });
        };

        var buildObject = function() {
            if (!$scope.user.education) {
                $scope.user.education = [{

                }];
            };
            if (!$scope.user.professional) {
                $scope.user.professional = [{

                }];
            }
            if ($scope.user.personalInfo) {
                if (!$scope.user.personalInfo.interest) {
                    $scope.user.personalInfo.interest = [];
                }
                if (!$scope.user.personalInfo.books) {
                    $scope.user.personalInfo.books = [];
                };
                if (!$scope.user.personalInfo.movies) {
                    $scope.user.personalInfo.movies = [];
                };
                if (!$scope.user.personalInfo.music) {
                    $scope.user.personalInfo.music = [];
                };
            }
            getProfileImage();
        }


        var getProfileImage = function() {
            UserService.getUserImg(userId).then(function(response) {
                if (response && response.mediaFileDetailsList) {
                    $scope.user.image = response.mediaFileDetailsList[1].mediapath;
                }
            }, function(error) {
                console.log("error");
            })
        }

        $scope.updateUser = function() {
            $scope.user.uuid = userId;
            var nodeObj = {
                "uuid": userId,
                "city": $scope.user.city,
                "birthday": $scope.user.birthday,
                "personalInfo": $scope.user.personalInfo,
                "education": $scope.user.education,
                "professional": $scope.user.professional,
                "gender": $scope.user.gender
            };
            var userObj = {
                "id": USER_CONFIG.member.id,
                "uuid": userId,
                "email": $scope.user.email,
                "firstName": $scope.user.firstName,
                "lastName": $scope.user.lastName,
                "phone": $scope.user.phone,
                "description": $scope.user.description,
                "socialAccounts": [],
                "emailConnects": []
            };

            UserService.updateUser(USER_CONFIG.member.id, userObj).then(function(success) {
                UserService.updateNodeProfile(nodeObj).then(function(response) {
                    AlertService.alert("Profile Updated Successfully");
                }, function(error) {
                    console.log("error");
                });
            }, function(error) {

            })

        };

        $scope.goto = function(page) {
            $scope.currentPage = page;
        };

        //Add row based on type Education or Professional
        $scope.addRow = function(type) {
            $scope.user[type].push({

            });
        };

        $scope.updateImage = function() {
            UserService.updateUserImg(userId, $scope.user.image).then(function(response) {
                if (response && response.mediaFileDetailsList) {
                    console.log(response);
                }
            })
        }

        $scope.deleteRow = function(ev, obj, type) {
            var confirm = $mdDialog.confirm()
                .title('Would you like to delete?')
                .textContent('')
                .ariaLabel('')
                .targetEvent(ev)
                .ok('Please do it!')
                .cancel('Cancel');
            $mdDialog.show(confirm).then(function() {
                var position = $scope.user[type].indexOf(obj);
                $scope.user[type].splice(position, 1);
            }, function() {

            });
        };

        var init = function() {
            getUser();
        }
        init();
    }]);
