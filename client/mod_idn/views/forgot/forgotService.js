angular.module('mod.idn')
//ALL SERVICE CALLS
    .factory('ForgotService', ['ForgotFactory', '$q', 'SessionService', function(ForgotFactory, $q, SessionService){

        var factory = {};

        factory.sendEmail = function(email){
            var deferred = $q.defer();

            ForgotFactory.forgotPassword().forgot({email : email}, function(success){
                deferred.resolve(success);
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;
        };

        return factory;
    }])


    //ALL FACTORY DEFINITIONS START HERE

    .factory('ForgotFactory', ['$resource', 'MOD_IDN', 'AppService', 'SessionService', function($resource, MOD_IDN, AppService, SessionService){
        var factory = {};

        factory.forgotPassword = function(){

            var bearer = "Bearer " + AppService.token.access_token;
            return $resource(MOD_IDN.API_URL + 'auth/forgotPassword', {},{
                'forgot' :{
                    method : 'POST',
                    headers :{
                        "Authorization" : bearer
                    }
                }
            });
        };

        return factory;

    }]);