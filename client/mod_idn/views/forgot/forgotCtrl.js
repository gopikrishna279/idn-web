/**
 * Created by nbos on 3/14/16.
 */
angular.module('mod.idn')
    .controller('ForgotCtrl', ['$scope', 'ForgotService', 'AlertService', 'SessionService', '$state', 'UserService', 'FBService', 'GoogleService', '$mdMedia',function($scope, ForgotService, AlertService, SessionService, $state, UserService, FBService, GoogleService, $mdMedia) {

        $scope.user_email='';

        $scope.message = '';
        
        $scope.forgotPassword = function(){
            console.log("in")
            ForgotService.sendEmail($scope.user_email).then(function(success){
                console.log(success);
                $scope.message = "Please check your email for reset instructions."
            }, function(error){
                $scope.message = "Oops! The email address specified does not exist in our records."
                console.log(error);
            });
        };


        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;

        $scope.$watch(function() {
            return $mdMedia('xs');
        }, function(wantsFullScreen) {
            if($mdMedia('xs')){
                console.log("setting to true");
                $scope.minWidthClass = true;    
            } else {
                console.log("setting to false");
                $scope.minWidthClass = false;
            }
            
        });






    }]);
