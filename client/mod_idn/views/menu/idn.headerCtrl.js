'use strict';

angular.module('mod.idn')
    .controller('IdnHeaderCtrl', ['$scope', 'LoginService', 'AlertService', '$state', 'SessionService', 'UserService', function($scope, LoginService, AlertService, $state, SessionService, UserService) {
    	var userId = SessionService.getSession().uuid;
        $scope.user = {};

        var getProfileImage = function() {
            UserService.getUserImg(userId).then(function(response) {
                if (response && response.mediaFileDetailsList) {
                    $scope.user.image = response.mediaFileDetailsList[1].mediapath;
                }
            }, function(error) {
                console.log("error");
            })
        }
        $scope.userLogout = function() {
            LoginService.logout().then(function(success) {
                $state.go('idn.login');
            }, function(error) {
                AlertService.alert("Error Logging Out. You can checkout any time you like, but you can never leave. ");
            });
        };
        getProfileImage();
    }])
