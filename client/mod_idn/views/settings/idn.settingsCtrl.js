'use strict';

angular.module('mod.idn')
    .controller('IdnSettingsCtrl', ['$scope', 'PasswordService', 'AlertService', function($scope, PasswordService, AlertService) {

        $scope.user = {};
        
        //Update password service call
        $scope.changePassword = function() {
            PasswordService.changePasswd($scope.user).then(function(response) {
                if (response) {
                    AlertService.alert('Password updated successfully', 'md-primary');
                }
            }, function(error) {
                AlertService.alert('Error updating password', 'md-warn');
            })
        }
    }]);
