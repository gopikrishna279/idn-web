'use strict';

angular.module('mod.idn')
    .controller('BugViewCtrl', ['$scope', '$stateParams', 'BugReportingService', 'UserService', 'AlertService', function($scope, $stateParams, BugReportingService, UserService, AlertService) {

        var bugId = $stateParams.bugId;
        $scope.isAdmin = false;



        var getBugDetails = function() {
            BugReportingService.getThread(bugId).then(function(response) {
                $scope.bugView = {
                    "bugInfo": response.bugInfo[0],
                    "comments": response.comments
                };
            }, function(response) {

            });
        };

        $scope.closeIssue = function() {
            $scope.bugView.bugInfo.isOpen = false;
            BugReportingService.updateIssue(bugId, $scope.bugView.bugInfo).then(function(response) {
                AlertService.alert("Issue closed successfully", "md-primary");
            }, function(error) {
                console.log("error closing issue");
            })
        }

        $scope.postComment = function() {
            if ($scope.commentObj.comment) {
                BugReportingService.postComment(bugId, $scope.commentObj).then(function(response) {
                    $scope.commentObj.comment = "";
                    getBugDetails();
                }, function(response) {
                    console.log("error");
                })
            } else {
                AlertService.alert("please type something to post a comment", 'md-warn');
            }
        };

        var init = function() {
            $scope.froalaOptions = {
                height: 200,
                toolbarButtons: ["bold", "italic", "underline", "|", "align", "formatOL", "formatUL"]
            };
            if ($scope.APP_SUBDOMAIN == 'admin') {
                $scope.isAdmin = true;
            }
            UserService.getUserObject().then(function(response) {
                $scope.commentObj = {
                    "id": bugId,
                    "reportedBy": response.firstName + " " + response.lastName,
                    "reportedId": response.uuid,
                    "reportedEmail": response.email
                };
            }, function(response) {

            })
            getBugDetails();
        };

        init();
    }]);
