angular.module('mod.idn')
    .factory('BugReportingService', ['BugReportingFactory', '$q', function(BugReportingFactory, $q) {
        var bugServ = {};

        bugServ.getAllIssues = function() {
            var deferred = $q.defer();

            BugReportingFactory.issues().list(function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        bugServ.postIssue = function(issue) {
            var deferred = $q.defer();

            BugReportingFactory.issues().post(issue, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        bugServ.getThread = function(id) {
            var deferred = $q.defer();

            BugReportingFactory.issues().get({ "id": id }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        bugServ.postComment = function(id, comment) {
            var deferred = $q.defer();

            BugReportingFactory.issues().post({ "id": id }, comment, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        bugServ.updateIssue = function(id, issue){
        	var deferred = $q.defer();

            BugReportingFactory.issues().update({ "id": id }, issue, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise; 
        }

        return bugServ;
    }])
    .factory('BugReportingFactory', ['$resource', 'MOD_NBOS', 'SessionService', function($resource, MOD_NBOS, SessionService) {
        var bugFact = {};

        bugFact.issues = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            return $resource(MOD_NBOS.NODE_API + "reporting/issues/:id", { id: '@id' }, {
                'list': {
                    method: 'GET',
                    headers: {
                        'Authorization': bearer
                    },
                    isArray: true
                },
                'post': {
                    method: 'POST',
                    headers: {
                        'Authorization': bearer
                    }
                },
                'get': {
                    method: 'GET',
                    headers: {
                        'Authorization': bearer
                    }
                },
                'update': {
                	method: 'PUT',
                	headers: {
                        'Authorization': bearer
                    }
                }
            })
        };

        return bugFact;
    }])
