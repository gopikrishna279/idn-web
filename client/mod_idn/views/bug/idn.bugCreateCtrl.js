angular.module('mod.idn')
    .controller('BugCreateCtrl', ['$scope', '$state', 'UserService', 'BugReportingService', function($scope, $state, UserService, BugReportingService) {

        $scope.postIssue = function() {
            BugReportingService.postIssue($scope.bugObj).then(function(response) {
                if (response) {
                    $state.go('idn2.bug');
                };
            }, function(error) {
                console.log("error saving issue");
            });
        };

        var init = function() {
            $scope.froalaOptions = {
                height: 200,
                toolbarButtons: ["bold", "italic", "underline", "|", "align", "formatOL", "formatUL"]
            };
            UserService.getUserObject().then(function(response) {
                $scope.bugObj = {
                    "reportedBy": response.firstName,
                    "reportedId": response.uuid,
                    "reportedEmail": response.email
                };
            }, function(response) {

            })
        };
        init();
    }]);
