'use strict';

angular.module('mod.idn')
    .factory('UserService', ['UserFactory', '$q', 'USER_CONFIG', 'SessionService', 'Upload', 'MOD_NBOS', function(UserFactory, $q, USER_CONFIG, SessionService, Upload, MOD_NBOS) {


        var factory = {};

        factory.member;

        // factory.token;

        //TODO: retrieve 'user access token' for all calls for modules from UserService not SessionService

        factory.setUser = function(key, val) {

            factory[key] = val;
            USER_CONFIG[key] = val;

        };

        factory.setAdminRole = function() {
            // TODO - this is a dummy role. Remove it after roles is completed
            if (USER_CONFIG.member.email == 'vijayrathore@nbostech.com' || USER_CONFIG.member.email == 'anudeep@nbos.in' || USER_CONFIG.member.email == 'user@nbostech.com') {
                var roles = {};
                roles.isAdmin = true;
                USER_CONFIG.roles = roles;
            };

        };

        factory.checkUserObj = function() {
            return factory.member ? true : false;
        };

        factory.updateUserImg = function(id, file) {
            var deferred = $q.defer();
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            Upload.upload({
                url: MOD_NBOS.API_URL + 'media/v0/media?id=' + id + '&mediafor=profile',
                headers: {
                    'Authorization': bearer
                },
                file: file,
            }).success(function(data, status, headers, config) {
                console.log(data);
                deferred.resolve(data);

            }).error(function(error) {
                deferred.reject();

            });
            return deferred.promise;
        };

        factory.getUserImg = function(id) {
            var deferred = $q.defer();

            UserFactory.userImage().get({ 'id': id, 'mediafor': 'profile' }, function(success) {
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        }

        factory.searchUser = function(obj) {
            var deferred = $q.defer();

            UserFactory.tenantUser().get(obj, function(success) {
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        }

        factory.getTenantUsers = function(tenantId) {
            var deferred = $q.defer();

            UserFactory.tenantUser().get({ tenantId: tenantId }, function(success) {
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        }

        factory.deleteUser = function() {
            console.log("IN DELETE USER");
            factory.member = null;
            factory.token = null;
        };

        factory.getUserObject = function() {
            var deferred = $q.defer();
            var session = SessionService.getSession();

            UserFactory.getUserObject().get({ userId: session.uuid }, function(success) {
                factory.member = {};
                USER_CONFIG.member = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        USER_CONFIG.member[key] = success[key];
                        factory.member[key] = success[key];
                    };
                };

                USER_CONFIG.session = SessionService.getSession();

                console.log(USER_CONFIG);
                deferred.resolve(success);
            }, function(error) {

                deferred.reject(error);
            });

            return deferred.promise;

        };

        factory.updateUser = function(id, profile) {
            var deferred = $q.defer();
            var session = SessionService.getSession();

            UserFactory.getUserObject().update({ userId: id }, profile, function(success) {
                factory.member = {};
                USER_CONFIG.member = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        USER_CONFIG.member[key] = success[key];
                        factory.member[key] = success[key];
                    };
                };

                USER_CONFIG.session = SessionService.getSession();

                console.log(USER_CONFIG);
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

        /*
         * Written in Node API 
         */
        factory.getNodeProfile = function() {
            var deferred = $q.defer();
            var session = SessionService.getSession();
            UserFactory.userProfile().get({ userId: session.uuid }, function(success) {
                factory.nodeMember = {};
                USER_CONFIG.nodeMember = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        USER_CONFIG.nodeMember[key] = success[key];
                        factory.nodeMember[key] = success[key];
                    };
                };
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;

        };

        factory.updateNodeProfile = function(profile) {
            var deferred = $q.defer();

            UserFactory.userProfile().update(profile, function(success) {
                factory.nodeMember = {};
                USER_CONFIG.nodeMember = {};

                for (var key in success) {
                    if (key[0] != "$") {
                        USER_CONFIG.nodeMember[key] = success[key];
                        factory.nodeMember[key] = success[key];
                    };
                };
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;

        };

        return factory;
    }])

.factory('UserFactory', ['$resource', 'MOD_IDN', 'MOD_NBOS', 'SessionService', function($resource, MOD_IDN, MOD_NBOS, SessionService) {
    var factory = {};

    factory.getUserObject = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_IDN.API_URL + 'users/:userId', {}, {
            'get': {
                method: 'GET',
                headers: {
                    "Authorization": bearer
                }
            },
            'update': {
                method: 'PUT',
                headers: {
                    "Authorization": bearer
                }
            }
        })
    };


    factory.tenantUser = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_IDN.API_URL + 'tenants/:tenantId/members', {
            tenantId: '@tenantId'
        }, {
            'get': {
                method: 'GET',
                isArray: true,
                headers: {
                    "Authorization": bearer
                }
            }
        })
    };

    factory.userImage = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.API_URL + 'media/v0/media', {}, {
            'get': {
                method: 'GET',
                headers: {
                    "Authorization": bearer
                }
            }
        })
    };

    /*
     * User Profile NODE API
     */
    factory.userProfile = function() {
        var bearer = "Bearer " + SessionService.getStoredUserToken();

        return $resource(MOD_NBOS.NODE_API + 'member/:userId', {
            userId: '@userId'
        }, {
            'get': {
                method: 'GET',
                headers: {
                    "Authorization": bearer
                }
            },
            'update': {
                method: 'POST',
                headers: {
                    "Authorization": bearer
                }
            }
        })
    };



    return factory;
}]);
