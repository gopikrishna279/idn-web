/**
 *
 * @ ACTION 1:
 * Get all user related information here
 * Get all APP level configurations here
 * Redirect to app configured main page post login
 */

'use strict';

angular.module('mod.idn')

    .controller('IdnDashboardCtrl', ['$scope', 'SessionService', 'UserService', 'RolesService', '$state', '$q', 'APP_CONFIG', 'USER_CONFIG', 'LayoutService', 'StateService', function ($scope, SessionService, UserService, RolesService, $state, $q, APP_CONFIG, USER_CONFIG, LayoutService, StateService) {

        $scope.showError = false;
        $scope.showInvalidToken = false;
        $scope.error = '';


        var navigateUserToModule = function () {

            var state = StateService.state;

            if (state) {
                $state.go(state.next.name, state.params);
            } else {
                $state.go(APP_CONFIG.MOD_IDN.login_redirect);
            };


            /*
             TODO: If accessing admin page check if role exists. Only then redirect else destroy session and redirect to 403 page.
             */
        };

        $scope.deleteSession = function () {
            console.log("IN DELETE SESSION");
            SessionService.deleteSession();
        };

        /*
         ********************************************************
         This is a special case code for NBOS App only
         ********************************************************
         */
        var navigateUserToModule_nbos = function () {

            var state = StateService.state;

            switch (APP_CONFIG.SUBDOMAIN) {

                case 'dev':
                    if (state) {
                        $state.go(state.next.name, state.params);
                    } else {
                        $state.go('dev.dashboard');
                    };
                    break;
                case 'admin':

                    if (USER_CONFIG.roles.isAdmin) {
                        if (state) {
                            $state.go(state.next.name, state.params);
                        } else {
                            $state.go('admin.dashboard');
                        }
                    } else {
                        $state.go('idn.403');
                    };

                    break;

                default:
                    if (state) {
                        $state.go(state.next.name, state.params);
                    } else {
                        $state.go('con.dashboard');
                    };
                    break;

            };
        };
        /*
         // SPECIAL CASE ENDS HERE
         */


        $scope.reLogin = function () {
            SessionService.deleteSession();
            UserService.deleteUser();
            $state.go('idn.login');
        };

        var init = function () {

            console.log("IN DASHBOARD INIT");
            if (SessionService.checkSession()) {

                if (USER_CONFIG.member) {
                    
                    navigateUserToModule_nbos();

                } else {
                    var session = SessionService.getSession();
                    var user = UserService.getUserObject(session.uuid),
                        roles = RolesService.getUserRoles(session.uuid);

                    $q.all([user, roles])
                        .then(function (success) {
                            UserService.setAdminRole();

                            console.log("USER OBJECT RECIEVED");
                            console.log(USER_CONFIG);

                            // if(APP_CONFIG.app["layout.modular"]){
                            //
                            // } else {
                            //     console.log("LAYOUT IS NOT MODULAR");
                            // };

                            LayoutService.buildLayout().then(function (laySuccess) {
                                navigateUserToModule_nbos();
                            }, function (layError) {
                                console.log("Error Buinding Layout");
                            });

                        }, function (error) {
                            if (error.status && error.status === 401) {
                                $scope.reLogin();
                            } else {
                                $scope.showError = true;
                            }
                            ;

                        })
                }


            } else {

                if (APP_CONFIG.token) {

                    if (StateService.state) {

                        //no session, public page, build layout/header as guest user for display
                        $state.go(StateService.state.next.name, StateService.state.params);

                    } else {
                        $state.go('idn.login');
                    };

                } else {
                    $state.go('home');
                };

            };
        };

        init();

    }]);
