'use strict';


angular
    .module('app.layout', []);


angular
    .module('app.layout')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

        $stateProvider
            .state('layout', {
                url: '',
                abstract : true,
                views : {
                    '':{
                        templateUrl: 'app_layout/layout.html',
                        controller : 'LayoutCtrl'
                    },
                    'header@layout':{
                        templateUrl: 'app_layout/header.html',
                        controller : 'HeaderCtrl'

                    },
                    'nav@layout':{
                        templateUrl: 'app_layout/nav.html'
                    },
                    'footer@layout':{
                        templateUrl: 'app_layout/footer.html'
                    }
                }
            });

    }]);
