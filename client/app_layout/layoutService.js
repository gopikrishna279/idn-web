'use strict';

angular.module('app.layout')

.factory('LayoutService', ['$state', '$q', 'NavigationService', 'RolesService', 'USER_CONFIG', function($state, $q, NavigationService, RolesService, USER_CONFIG){

    var factory = {};

    factory.nav;
    factory.headerNav;

    factory.createHeader = function(){

    };

    factory.buildLayoutForRole = function(){
        var deferred = $q.defer();
        console.log(NavigationService.nav);

        deferred.resolve();

        return deferred.promise;
    };

    factory.buildLayout = function(){
        console.log("BUILD LAYOUT CALLED");
        var deferred = $q.defer();

        var header = [],
            links = [],
            nav = [],
            nav_new = [],
            found = false;

        links = NavigationService.nav;

        header = NavigationService.headerNav;

        //Building Header Links
        header.sort(function(a,b){
            if(a.position > b.position){
                return 1;
            };
        });


        //Building Navigation
        for(var i =0; i<links.length; i++){
            found = false;
            for(var j=0; j<nav.length; j++){
                if(links[i].data && links[i].data.displayLink && links[i].data.moduleName && nav[j].name === links[i].data.moduleName){
                    var page = {};
                    page.name = links[i].data.displayName;
                    page.disabled = links[i].data.disableLink;
                    page.type = "link";
                    page.icon = "";
                    page.state = links[i].name;

                    nav[j].pages.push(page);
                    found = true;
                }
            };
            if(!found && links[i].data && links[i].data.displayLink && links[i].data.moduleName){
                var obj = {};
                obj.name = links[i].data.moduleName;
                obj.type = "toggle";
                obj.pages =[];
                var page = {};
                page.name = links[i].data.displayName;
                page.disabled = links[i].data.disableLink;
                page.type = "link";
                page.icon = "";
                page.state = links[i].name;

                obj.pages.push(page);

                nav.push(obj);
            };

        };

        //

        var new_nav = {
            sections : nav
        };


        console.log("LAYOUT INFO");
        console.log(new_nav);
        console.log(header);
        console.log("LAYOUT INFO ENDS HERE");

        factory.nav = new_nav;
        factory.headerNav = header;

        deferred.resolve();

        return deferred.promise;
    };

    factory.createMenu = function(){

        var deferred = $q.defer();

        try{
            //construct menu

            var links = $state.get(),
                nav = [],
                old_nav = [],
                header = [],
                found = false;
            
            for(var i =0; i<links.length; i++) {

                if(links[i].data && links[i].data.type){
                    if(links[i].data.type === 'nav'){
                        nav.push(links[i].data);
                    } else if(links[i].data.type === 'header'){
                        header.push(links[i].data);
                    }
                }
            };

            header.sort(function(a,b){
                if(a.position > b.position){
                    return 1;
                };
            });

            for(var i =0; i<links.length; i++){
                found = false;
                for(var j=0; j<old_nav.length; j++){
                    if(links[i].data && links[i].data.menu && links[i].data.module && old_nav[j].name === links[i].data.module){
                        var page = {};
                        page.name = links[i].data.name;
                        page.disabled = links[i].data.disabled;
                        page.type = "link";
                        page.icon = "";
                        page.state = links[i].name;

                        old_nav[j].pages.push(page);
                        found = true;
                    }
                };
                if(!found && links[i].data && links[i].data.menu && links[i].data.module){
                    var obj = {};
                    obj.name = links[i].data.module;
                    obj.type = "toggle";
                    obj.pages =[];
                    var page = {};
                    page.name = links[i].data.name;
                    page.disabled = links[i].data.disabled;
                    page.type = "link";
                    page.icon = "";
                    page.state = links[i].name;

                    obj.pages.push(page);

                    old_nav.push(obj);
                };
            };

            //

            var new_nav = {
                sections : old_nav,
                toggleSelectSection: function (section) {
                    new_nav.openedSection = (new_nav.openedSection === section ? null : section);
                },
                isSectionSelected: function (section) {
                    return new_nav.openedSection === section;
                },

                selectPage: function (section, page) {
                    page && page.url && $location.path(page.url);
                    new_nav.currentSection = section;
                    new_nav.currentPage = page;
                }
            };

            factory.nav = new_nav;
            factory.headerNav = header;

            deferred.resolve();
        } catch(error){
            deferred.reject(error);
        };

        return deferred.promise;

    };

    return factory;

}]);