'use strict';

/**
 * @ngdoc overview
 * @name CodingLabs
 * @description
 * # NBOS LAYOUT MODULE
 *
 * Main module of the application.
 */
angular
    .module('app.layout', []);

angular
    .module('app.layout')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

        $stateProvider
            .state('layout', {
                url: '',
                abstract : true,
                views : {
                    '':{
                        templateUrl: 'demo/client/app_config/layout/layout.html',
                        controller : 'LayoutCtrl'
                    },
                    'header@layout':{
                        templateUrl: 'demo/client/app_config/layout/header.html'
                    },
                    'nav@layout':{
                        templateUrl: 'demo/client/app_config/layout/nav.html'
                    },
                    'footer@layout':{
                        templateUrl: 'demo/client/app_config/layout/footer.html'
                    }
                }
            });

    }]);
