angular.module('mod.m34')
    .controller('TodoController', ['$scope', 'TodoService', '_', '$mdDialog', 'SessionService', function($scope, TodoService, _, $mdDialog, SessionService) {
        $scope.todos = [];
        $scope.dueDate = new Date();
        var memberId = SessionService.getSession().uuid;
       // console.log(SessionService.getSession().uuid);
        var getAllTodos = function() {
            TodoService.getMyActiveTodos(memberId).then(function(response) {

                angular.forEach(response, function(value, key) {
                    value.dueDate = new Date(value.dueDate).setHours(0, 0, 0, 0);
                })
                $scope.todos = _.groupBy(response, "dueDate");

            }, function(response) {

            });
        }

        $scope.addTodo = function() {
            var param = {
                "name": $scope.newTodo,
                "memberId": memberId
            };
            if ($scope.dueDate) {
                param.dueDate = $scope.dueDate;
            }

            TodoService.saveTodo(memberId, param).then(function(response) {
                getAllTodos();
                $scope.newTodo = "";
            }, function(response) {

            });
        };

        $scope.editTodo = function(todo) {
            
        };

        $scope.updateCompleted = function(todo, isCompleted) {
            todo.isCompleted = isCompleted;
            TodoService.updateTodo(todo._id, todo).then(function(response) {
                
            }, function(response) {

            });
        };

        $scope.removeTodo = function(todo) {

            TodoService.delete({ 'id': todo._id }).$promise.then(function(response) {
                getAllTodos();
            }, function(response) {

            });


        };

        $scope.doneEditing = function(todo) {
            todo.isEdit = false;
        };

        getAllTodos();

    }]);
