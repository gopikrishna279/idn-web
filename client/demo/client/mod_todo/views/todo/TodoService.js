/**
 *  Module
 *
 * Description
 */
angular.module('mod.m34').
factory('TodoService', function(TodoFactory, $q) {
        var todoService = {};
        todoService.getMyActiveTodos = function(memberId) {
            var deferred = $q.defer();

            TodoFactory.myTodos().get({ 'id': memberId, 'method': 'all', 'mine': 'mine' }, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        todoService.saveTodo = function(memberId, todo) {
            var deferred = $q.defer();

            TodoFactory.myTodos().save({ 'id': memberId }, todo, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        todoService.updateTodo = function(todoId, todo) {
            var deferred = $q.defer();

            TodoFactory.myTodos().update({ 'id': todoId }, todo, function(success) {
                deferred.resolve(success);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
        return todoService;
    })
    .factory('TodoFactory', ['$resource', 'MOD_todo', 'SessionService', function($resource, MOD_todo, SessionService) {
        var todoFact = {};

        todoFact.myTodos = function() {
            var bearer = "Bearer " + SessionService.getStoredUserToken();
            //var bearer = "Bearer cc94a610-f3d8-4218-8c3d-34b51ed1ce02";
            return $resource(MOD_todo.API_URL + 'todos/:id/:method/:mine', {
                id: '@id',
                method: '@method',
                mine: '@mine'
            }, {
                'get': {
                    method: 'GET',
                    isArray: true,
                    headers: {
                        "Authorization": bearer
                    }

                },
                'save': {
                    method: 'POST',
                    headers: {
                        'Authorization': bearer
                    }
                },
                'update': {
                    method: 'PUT',
                    headers: {
                        'Authorization': bearer
                    }
                }
            })
        };

        return todoFact;
    }])
