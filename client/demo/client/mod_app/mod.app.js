'use strict';

angular.module('mod.app', []);


angular.module('mod.app')

    .constant('MOD_APP', {
        API_URL: '',
        API_URL_DEV: ''
    })

    .config(['$stateProvider', function ($stateProvider) {
        $stateProvider
            .state('app', {
                url: '/app',
                parent:'layout',
                template: '<div ui-view></div>',
                data: {
                    type: "home"
                }
            })
            //FOR NAVIGATION
           .state('app.header', {
               url: '',
               template: "<div></div>",
               data: {
                   templateUrl:'demo/client/mod_app/views/menu/app.header.html',
                   position: 1,
                   enabled : true,
                   type: 'header',
                   name: "App",
                   module: "App"
               }
           })
           //FOR ROUTES
           .state('app.dashboard', {
               url: '/dash',
               templateUrl: "demo/client/mod_app/views/dashboard/app.dashboard.html",
               data: {
                   type: 'home',
                   menu: true,
                   disabled : false,
                   name: "Dashboard",
                   module: "App"
               }
           })

    }])