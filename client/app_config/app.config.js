'use strict';

angular.module('app.config', []);


angular.module('app.config')

.constant('APP_CONSTANTS', {
    API_URL: 'https://idn.au-syd.mybluemix.net/',
    // API_URL: 'http://10.9.9.44:8080/',
    // API_URL_DEV: 'http://api.qa1.nbos.in/',
    TENANT_ID: 'TNT:appConsole',
    GRANT_TYPE: 'client_credentials',
    SCOPE: '',
    APP_SESSION_KEY: 'TNT:appConsole'
})

.value('APP_CONFIG', {})

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: "app_config/views/primaryPage.html",
            controller: 'PrimaryPageCtrl',
            data: {
                module: "App",
                type: 'home',
                display : false,
                disabled : false,
                authenticate : false
            }
        })
        .state('error', {
            url: '/error',
            templateUrl: "app_config/views/error.html",
            controller: 'ErrorCtrl',
            data: {
                module: "App",
                type: 'home',
                display : false,
                disabled : false,
                authenticate : false
            }
        })
}]);
var underscore = angular.module('underscore', []);
underscore.factory('_', ['$window', function($window) {
    return $window._; // assumes underscore has already been loaded on the page
}]);
