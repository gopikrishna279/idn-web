'use strict';


angular.module('app.config')
    .factory('ConfigService', ['ConfigFactory', '$q', 'APP_CONSTANTS', 'APP_CONFIG', function(ConfigFactory, $q, APP_CONSTANTS, APP_CONFIG){
        var factory = {};

        factory.config;


        factory.getAppConfig = function(){
            var deferred = $q.defer();

            ConfigFactory.getConfig().get({tenantId : APP_CONSTANTS.TENANT_ID}).$promise.then(function(success){
                factory.config = success;
                for(var key in success){
                    if(key[0] != "$"){
                        APP_CONFIG[key] = success[key];
                    };
                };
                deferred.resolve();
            }, function(error){
                deferred.reject(error);
            });

            return deferred.promise;

        };

        return factory;

    }])

    .factory('ConfigFactory', ['APP_CONSTANTS', 'APP_CONFIG', '$resource', function(APP_CONSTANTS, APP_CONFIG, $resource){
        var factory = {};



        factory.getConfig = function(){
            // return $http.get('data/app_config.json')
            var bearer = "Bearer " + APP_CONFIG.token.access_token;

            return $resource(APP_CONSTANTS.API_URL + 'api/core/v0/config/values/tenant/:tenantId', {tenantId : '@tenantId'},{
                'get' :{
                    method : 'GET',
                    headers:{
                        "Authorization" : bearer
                    }
                }
            });
        };

        return factory;
    }]);