'use strict';

/**
 * @ngdoc function
 * @name newappApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the Entire Application
 */

angular.module('app.config')
    .controller('AppCtrl', ['$scope', 'APP_CONFIG', function ($scope, APP_CONFIG) {


        /**
         *    NBOS NOTE: ENVIRONMENT VARIABLE IS SET HERE
         */

        $scope.message = "hello";


        APP_CONFIG.ENV = document.getElementById('ENV').innerHTML.toUpperCase();
        APP_CONFIG.SUBDOMAIN = document.getElementById('subdomain').innerHTML;

    }]);
