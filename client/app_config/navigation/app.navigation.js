'use strict';

/**
 * @ngdoc overview
 * @name CodingLabs
 * @description
 * # NBOS LAYOUT MODULE
 *
 * Main module of the application.
 */
angular
    .module('app.nav', []);