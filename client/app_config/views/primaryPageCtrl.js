'use strict';
angular.module('app.config')
.controller('PrimaryPageCtrl', ['$scope', 'AppService', 'ConfigService', 'NavigationService', 'StateService', '$state', '$q', 'ErrorService', 'APP_CONFIG', function($scope, AppService, ConfigService, NavigationService, StateService, $state, $q, ErrorService, APP_CONFIG){
    $scope.message="Hello and Welcome to your App";

    $scope.showError = false;
    $scope.error = "";
    /*
     TODO: THIS SHOULD BE A BLOCKING CALL
     ALTERNATERLY, MOVE THIS TO THE NODE SERVER
     */
        if(!APP_CONFIG.token){

            AppService.getToken().then(function(success){

                /*
                    TODO : SEVERE
                     Depending on the location of  this file in the index, the NavigationService $state.get() is not showing all the links configured.
                    <script src="app_config/views/primaryPageCtrl.js"></script>
                    This may prove to be a severe issue. Investigate further.
                 */

                var config = ConfigService.getAppConfig(),
                    navigation = NavigationService.createLinks();

                $q.all([config, navigation])
                    .then(function(success2){
                        console.log("AT HOME. APP_CONFIG RECIEVED. SENDING TO DASH");
                        $state.go('idn.dashboard');

                    }, function(error2){

                        //error getting tokens
                        console.log("error");
                    });

            }, function(error){

                ErrorService.setError(error, "Primary Page");
                $state.go('error');
            });



        } else {

            if(StateService.state){
                $state.go(StateService.state.next.name, StateService.state.params);
            } else {
                $state.go('idn.dashboard');
            }
        };

}]);