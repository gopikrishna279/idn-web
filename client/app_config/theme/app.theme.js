'use strict';

angular.module('app.theme', []);

angular.module('app.theme')
    .config(['$mdThemingProvider', 'cfpLoadingBarProvider', function($mdThemingProvider, cfpLoadingBarProvider) {

        cfpLoadingBarProvider.includeSpinner = false;

        //Angular Material Theme Configuration

        //By default, shades 500, 300 800 and A100 are used for primary and warn intentions, in order for default, hue1, hue2 and hue3.
        // While A200, A100, A400 and A700 are used for accent.

        var backgroundPalette = {
            '50': '#ECEFF1',
            '100': '#CFD8DC',
            '200': '#B0BEC5',
            '300': '#90A4AE',
            '400': '#78909C',
            '500': '#383f45',
            '600': '#546E7A',
            '700': '#455A64',
            '800': '#383f45',
            '900': '#263238',
            'A100': '#383f45',
            'A200': '#B0BEC5',
            'A400': '#31373d',
            'A700': '#263238',
        };
        $mdThemingProvider
            .definePalette('backgroundPalette',
                backgroundPalette);



        var customAccent = {
            '50': '#000000',
            '100': '#000000',
            '200': '#0a0c0d',
            '300': '#16181b',
            '400': '#212529',
            '500': '#2d3237',
            '600': '#434c53',
            '700': '#4f5961',
            '800': '#5a666f',
            '900': '#66727d',
            'A100': '#434c53',
            'A200': '#383f45',
            'A400': '#2d3237',
            'A700': '#717f8b',
            'contrastDefaultColor': 'light',
        };
        $mdThemingProvider
            .definePalette('customAccent',
                customAccent);

        var myAccent = {
            '50': '#57521d',
            '100': '#6a6424',
            '200': '#7d762a',
            '300': '#908831',
            '400': '#a39a37',
            '500': '#b6ac3e',
            '600': '#c8bf5e',
            '700': '#cfc771',
            '800': '#d5cf84',
            '900': '#dcd697',
            'A100': '#4DB6AC',
            'A200': '#009688',
            'A400': '#00796B',
            'A700': '#004D40',
            'contrastDefaultColor': 'light',
        };
        $mdThemingProvider
            .definePalette('myAccent',
                myAccent);


        $mdThemingProvider.theme('default')
            .primaryPalette('orange')
            .accentPalette('myAccent')
            .warnPalette('red');

        $mdThemingProvider.theme('alternate')
            .primaryPalette('yellow')
            .accentPalette('green')
            .warnPalette('red')
            .backgroundPalette('backgroundPalette')
            .dark();

    }]);
