
/**
 * Module dependencies.
 */

var express         = require('express'),
  routes            = require('./routes'),
  pageNotFound      = require('./pageNotFound'),
  http              = require('http'),
  path              = require('path'),
  morgan            = require('morgan'),
  favicon           = require('serve-favicon'),
  errorhandler      = require('errorhandler'),
  serveStatic       = require('serve-static');

var app = express();

// all environments
app.set('port', process.env.PORT || 9000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/client/assets/favicon/favicon.ico'));
app.use(morgan('dev'));
app.use(serveStatic(__dirname + '/client'));
app.use('/site', serveStatic(__dirname + '/website'));

// development only
if (process.env.NODE_ENV === 'development') {

  app.use(errorhandler());

};


//For the Angular App
app.get('/', routes.index);

//For Other Routes
app.get('*', pageNotFound.index);


app.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
