exports.index = function(req, res){

  console.log(req);

  var domain = req.headers.host,
      subDomain = domain.split('.');

  //change .in to .io in production
  if(domain.indexOf('.in') > -1 && domain.indexOf('.nbos.') > -1){
    //PRODUCTION MODE
    if(subDomain.length > 2){
      //make HTTP request here

    }else{
      subDomain = "www";
      res.render('index', { subDomain: subDomain });
    }

  } else {
    //DEV MODE
    if(subDomain.indexOf('admin') >= 0){
      res.render('admin', { subDomain: "admin", ENV: "dev" });
    } else  if(subDomain.indexOf('dev') >= 0){
      res.render('dev', { subDomain: "dev" , ENV: "dev" });
    } else  if(subDomain.indexOf('console') >= 0){
      res.render('console', { subDomain: "console" , ENV: "dev" });
    } else if(subDomain.indexOf('docs') >= 0){
      res.render('docs', { subDomain: "docs", ENV: "dev" });
    } else if(subDomain.indexOf('demo') >= 0){
      res.render('demo', { subDomain: "demo", ENV: "dev" });
    } else {
      res.redirect("http://" + req.headers.host + "/site");
    }

  }

};
