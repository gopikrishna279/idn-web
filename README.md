# newapp

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## PreRequisite
Project has dependency on sass. Check if sass is installed. Otherwise install sass from http://sass-lang.com/install

sudo gem install sass

## Local Installtion
npm install -g bower grunt-cli gulp-cli

npm install

bower install

## HostFile Entries
127.0.0.1               console.localhost

127.0.0.1               dev.localhost

127.0.0.1               admin.localhost


## Start Server
grunt express

Open in browser http://console.localhost:9000/


