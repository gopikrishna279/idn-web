// Karma configuration
// Generated on 2016-03-08

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    // as well as any additional frameworks (requirejs/chai/sinon/...)
    frameworks: [
      'jasmine'
    ],

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'client/bower_components/jquery/dist/jquery.js',
      'client/bower_components/angular/angular.js',
      'client/bower_components/angular-animate/angular-animate.js',
      'client/bower_components/angular-cookie/angular-cookie.js',
      'client/bower_components/froala-wysiwyg-editor/js/froala_editor.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/froala_editor.pkgd.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/align.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/char_counter.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/code_beautifier.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/code_view.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/colors.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/draggable.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/emoticons.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/entities.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/file.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/font_family.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/font_size.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/fullscreen.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/image.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/image_manager.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/inline_style.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/line_breaker.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/link.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/lists.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/paragraph_format.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/paragraph_style.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/quick_insert.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/quote.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/save.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/table.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/url.min.js',
      'client/bower_components/froala-wysiwyg-editor/js/plugins/video.min.js',
      'client/bower_components/angular-froala/src/angular-froala.js',
      'client/bower_components/angular-loading-bar/build/loading-bar.js',
      'client/bower_components/angular-local-storage/dist/angular-local-storage.js',
      'client/bower_components/angular-aria/angular-aria.js',
      'client/bower_components/angular-messages/angular-messages.js',
      'client/bower_components/angular-material/angular-material.js',
      'client/bower_components/angular-material-data-table/dist/md-data-table.js',
      'client/bower_components/angular-resource/angular-resource.js',
      'client/bower_components/angular-ui-router/release/angular-ui-router.js',
      'client/bower_components/less/dist/less.js',
      'client/bower_components/ng-file-upload/ng-file-upload.js',
      'client/bower_components/underscore/underscore.js',
      'client/bower_components/tinycolor/tinycolor.js',
      'client/bower_components/md-color-picker/dist/mdColorPicker.min.js',
      'client/bower_components/angular-sanitize/angular-sanitize.js',
      'client/bower_components/angular-swagger-ui/dist/scripts/swagger-ui.min.js',
      'client/bower_components/angular-mocks/angular-mocks.js',
      // endbower
      'app/scripts/**/*.js',
      'test/mock/**/*.js',
      'test/spec/**/*.js'
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      'PhantomJS'
    ],

    // Which plugins to enable
    plugins: [
      'karma-phantomjs-launcher',
      'karma-jasmine'
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
